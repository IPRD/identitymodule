package org.iprd.sharedutils;

import android.os.StrictMode;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * RestAPIHandler class to send rest API request to a third party rest API server. This class is right now used in our code for implementing thinkMD functionality (in mgd) which talks to the thinkMD remote server and
 * smsService which sends REST requestAPI calls to the twillio sms service.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */

public class RestAPIHandler {
    /**
     * mRemoteServer : Remote server value
     */
    private String mRemoteServer; //here we are hardcoding the remote server value for the demo based on what the thinkMD team shared. We need to later decide how to pass this value. (Maybe from some config parameter?)

    /**
     *RestAPIHandler() : Zero parameter constructor used for "medsine app"
     * here mRemoteServer = "medsinc-v3-api-staging-pr-17.herokuapp.com" is seted and StrictMode.setThreadPolicy(policy)
     */
    public RestAPIHandler() {
        mRemoteServer = "medsinc-v3-api-staging-pr-17.herokuapp.com";
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    /**
     *
     * @param remoteServer for connecting with given remote server
     */

    public RestAPIHandler(String remoteServer) {
        mRemoteServer = remoteServer;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    /**
     *
     * @return mRemoteServer i.e, remote server information
     */
    public String getRestServer() {
        return mRemoteServer;
    }

    /**
     *
     * @param remoteServer : for setting the remote server.
     */
    public void setRestServer(String remoteServer) {
        mRemoteServer = remoteServer;
    }

    /**
     *
     * @param httpSecureFlag : boolean information
     * @return for getting requested http protocol - http or https
     */
    private String getHTTPProtocol(boolean httpSecureFlag) {
        if (httpSecureFlag)
        {
            //this refers to https
            return "https://";
        }
        else
            return "http://";
    }

    /**
     * To send GET request
     * @param getURL : url string GET
     * @param rspStr : response content from rest API
     * @param isHTTPSecure : boolean data, whether http protocol is secure or not.
     * @param headerParams : requested prototype information (eg, GET/PUT/POST)
     * @return : boolean data, depends on request information and successful request. successful retuen data is true.
     */
    public boolean handleGetRequest(String getURL, StringBuffer rspStr, boolean isHTTPSecure, Map<String,String> headerParams) {
        String completeGetURL = getHTTPProtocol(isHTTPSecure)+getRestServer()+getURL;
        HttpURLConnection con;
        try {
            URL obj = new URL(completeGetURL);
            BufferedReader in;
            if (isHTTPSecure)
                con = (HttpsURLConnection) obj.openConnection();
            else
                con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            for (Map.Entry<String,String> entry : headerParams.entrySet())
            {
                con.setRequestProperty(entry.getKey(),entry.getValue());
            }
            //con.setRequestProperty("X-Authy-API-Key","RBzQGUPGF7KOLjboMGXRjM01RbVdLGuN");
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);
            Log.d("Rsp",con.getRequestMethod());
            String apiKey = con.getRequestProperty("X-Authy-API-Key");
            int rspCode = con.getResponseCode();
            Log.d("Rsp",Integer.toString(rspCode));
            if ((rspCode == HttpURLConnection.HTTP_OK) || (rspCode == HttpsURLConnection.HTTP_OK) || (rspCode == HttpsURLConnection.HTTP_NOT_FOUND))
            {
                String line = con.getResponseMessage();
                rspStr.append(line);
                return true;
            }
            else{
                Log.d("Rsp",con.getRequestMethod());
                Log.d("Rsp",con.getResponseMessage());
                return false;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        } catch (ProtocolException e) {
            e.printStackTrace();
            return false;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * To send POST request
     * @param postURL : url string POST
     * @param rspStr : response content from rest API
     * @param isHTTPSecure : boolean data, whether http protocol is secure or not.
     * @return : boolean data, depends on request information and successful request. successful return data is true.
     */
    public boolean handlePostRequest(String postURL, String postBody, StringBuffer rspStr,boolean isHTTPSecure) {
        String completePostURL = getHTTPProtocol(isHTTPSecure)+getRestServer()+postURL;
        try {
            URL obj = new URL(completePostURL);
            BufferedReader in;
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "UTF-8");
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(postBody);
            writer.flush();
            writer.close();
            os.close();
            con.connect();
            Log.d("Rsp",Integer.toString(con.getResponseCode()));
            if ((con.getResponseCode() == HttpsURLConnection.HTTP_CREATED) || (con.getResponseCode() == HttpsURLConnection.HTTP_OK)) {
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line;
                StringBuffer response = new StringBuffer();
                while((line = in.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                in.close();
                rspStr.append(response.toString());
                return true;
            }
            else {
                Log.e("REST Error","Failure msg recvd from rest server - "+con.getResponseCode());
                return false;
            }
        } catch (java.net.MalformedURLException var1) {

            Log.e("REST Error",var1.toString());
            return false;
        } catch (java.io.IOException var2) {
            Log.e("REST Error",var2.toString());
            return false;
        }
    }

}

