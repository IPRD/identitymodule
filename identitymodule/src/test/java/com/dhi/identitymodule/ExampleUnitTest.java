package com.dhi.identitymodule;





import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void FHIRJsonResponse() {

//        String svcReqJSON =  "{\"entry\": [{\"basedOn\": [],\"code\": {\"coding\": [{\"code\": \"8867-4\",\"display\": \"Heart rate\",\"system\": \"http://loinc.org\"" +
//                "}]},\"effectiveDateTime\": 123,\"id\": null,\"identifier\": [{\"system\": \"VideoVitals\",\"type\": {\"coding\": []},\"use\": \"usual\"," +
//                "\"value\": \"5ff75a42-6820-4e14-b7ae-934ceda8dfff\"}],\"resourceType\": \"ServiceRequest\",\"status\": null,\"subject\": {\"identifier\": " +
//                "[{\"system\": \"iprd\",\"type\": {\"coding\": []},\"use\": \"usual\",\"value\": \"patientid123\"}]},\"valueQuantity\": {\"code\": \"beats/minute\"," +
//                "\"system\": \"http://unitsofmeasure.org\",\"unit\": \"beats/minute\",\"value\": 0}}],\"resourceType\": \"Bundle\",\"type\": \"collection\"}";

        String responseString = "{\"entry\":[{\"fullUrl\":\"http:\\/\\/iprdgroup.com\\/FHIR\\/Resources\\/Observation2\",\"resource\":{\"basedOn\":[]," +
                "\"code\":{\"coding\":[{\"code\":\"8867-4\",\"display\":\"Heart rate\",\"system\":\"http:\\/\\/loinc.org\"}]}," +
                "\"effectiveDateTime\":\"2017-01-01T00:00:00.000Z\",\"id\":\"Observation\\/obs1\",\"identifier\":[{\"system\":\"http:\\/\\/www.jumper.com\"," +
                "\"type\":{\"coding\":[{\"code\":\"8867-4\",\"display\":\"Heart rate\",\"system\":\"http:\\/\\/loinc.org\"}]},\"use\":\"usual\"," +
                "\"value\":\"5ff75a42-6820-4e14-b7ae-934ceda8d3d6\"}],\"resourceType\":\"Observation\",\"status\":\"preliminary\",}}]," +
                "\"resourceType\":\"Bundle\",\"type\":\"collection\"}";
        try {
            String requestString="{\"entry\":[{\"fullUrl\":\"http://iprdgroup.com/FHIR/Resources\",\"resource\":{\"resourceType\":\"ServiceRequest\"," +
                    "\"id\":\"example_request_HR\",\"status\":\"active\",\"intent\":\"original-order\"," +
                    "\"code\":{\"coding\":[{\"system\":\"http://loinc.org\",\"code\":\"8867-4\",\"display\":\"Heart rate\"}]," +
                    "\"text\":\"Heart rate\"},\"subject\":{\"reference\":\"Patient/patient_example\"}}}],\"resourceType\":\"Bundle\"," +
                    "\"type\":\"collection\"}";
            JSONObject requestObj = new JSONObject(requestString);
            JSONObject responseObj = new JSONObject(responseString);

            JSONObject valueQuantityObj = new JSONObject();
            valueQuantityObj.put("code","beats/minute");
            valueQuantityObj.put("system","http://unitsofmeasure.org");
            valueQuantityObj.put("unit","beats/minute");
            valueQuantityObj.put("value",98.7);
            responseObj.getJSONArray("entry").getJSONObject(0).getJSONObject("resource").put("valueQuantity",valueQuantityObj);

            String pattern ="YYYY-MM-dd'T'HH:mm:ss.SSSXXX";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            System.out.println(".........."+date);
            responseObj.getJSONArray("entry").getJSONObject(0).getJSONObject("resource").put("effectiveDateTime",date);

            //responseObj.getJSONArray("entry").getJSONObject(0).put("valueQuantity",valueQuantityObj);
            // for copying the subject from request
            JSONObject subjectFromRequest = requestObj.getJSONArray("entry").getJSONObject(0).getJSONObject("resource").getJSONObject("subject");
            responseObj.getJSONArray("entry").getJSONObject(0).getJSONObject("resource").put("subject", subjectFromRequest);

            System.out.println(">>>>>>>>>2  "+responseObj.toString());
            //test = resJSON.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            //test = svcReqJSON;
        }

        //assertEquals(4, 2 + 2);
    }
}

//
            /*JSONObject resJSON = new JSONObject(svcReqJSON);
            String date = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss").format(new Date());//"yyyy-MM-dd'T'HH:mm:ss"
            JSONObject jEntry= new JSONObject(resJSON.getJSONArray("entry").get(0).toString());
            resJSON.getJSONArray("entry").getJSONObject(0).getJSONObject("valueQuantity").put("value","100.0");
            resJSON.getJSONArray("entry").getJSONObject(0).put("resourceType", "Observation");
            resJSON.getJSONArray("entry").getJSONObject(0).put("effectiveDateTime", date);*/