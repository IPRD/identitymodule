package com.dhi.identitymodule;


import android.app.Instrumentation;
import android.content.pm.InstrumentationInfo;
import android.util.Log;

import org.junit.Test;
import android.content.Context;
//import android.support.test.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;


public class UserCacheTest {


    @Test
    public void testUtils() {
        StringBuffer dob = new StringBuffer();
        long dobLong = IdentityUtils.getDOBFromAge(39*365+33,dob);
        Log.d("DOB: ",dob.toString());
        assertNotNull(dob);

        IdentityUtils.setAgeAccuracy(1);
        boolean ageVerification = IdentityUtils.validateAge(39*365+60,39*365+30);
        assertEquals(true,ageVerification);
    }

    @Test
    public void testCache() {
        UserCache.getCache().addDataToCache("HCW","name","Hari","H100");
        UserCache.getCache().addDataToCache("HCW","uid","25","H100");
        UserCache.getCache().addDataToCache("HCW","name","Ganesh","H102");
        UserCache.getCache().addDataToCache("HCW","uid","26","H102");
        UserCache.getCache().addDataToCache("HCW","name","Hari","H103");
        UserCache.getCache().addDataToCache("HCW","uid","27","H103");
        UserCache.getCache().addDataToCache("Patient","name","Jack","J100");
        Set<String> guidResult = new HashSet<String>();
        guidResult = UserCache.getCache().searchForUser("HCW","name","Hari");
        if (guidResult != null && guidResult.size() > 0)
        {
          //  Log.d("Search result", guidResult.get(0));
            assertEquals("H100",guidResult.iterator().next());
        }
        else
            Log.e("Search error","No user found: Hari");

        Set<String> guidResult1 = new HashSet<String>();
        guidResult1 = UserCache.getCache().searchForUser("HCW","age","25");
        if (guidResult1 != null && guidResult1.size() > 0)
        {
        //    Log.d("Search result", guidResult1.get(0));
            assertEquals("H100",guidResult1.iterator().next());
        }
        else
            Log.e("Search error","No user found: Jack");

        Set<String> guidResult2 = new HashSet<String>();
        guidResult2 = UserCache.getCache().searchForUser("HCW","name","Mono");
        if (guidResult2 != null && guidResult2.size() > 0)
        {
            //    Log.d("Search result", guidResult1.get(0));
            assertEquals("J100",guidResult2.iterator().next());
        }
        else
            Log.e("Search error","No user found: Jack");
    }

    @Test
    public void testIDRetrieve() {
        UserCache.getCache().addDataToCache("HCW","name","Hari","H_100");
        UserCache.getCache().addDataToCache("HCW","name","Om","H_200");
        UserCache.getCache().addDataToCache("Patient","name","ABC","P_100");
        UserCache.getCache().addDataToCache("Patient","name","DEF","P_200");
        UserCache.getCache().addDataToCache("NA","name","HariOm","B_100");
        List<String> allID = new ArrayList<String>();
        allID = UserCache.getCache().getAllIds(IdentityModule.HCW);
        assertEquals(2,allID.size());
        allID.clear();
        allID = UserCache.getCache().getAllIds(IdentityModule.PATIENT);
        assertEquals(2,allID.size());
        allID.clear();
        allID = UserCache.getCache().getAllIds(IdentityModule.BIOMETRIC_DATA);
        assertEquals(1,allID.size());
        allID.clear();
    }
}
