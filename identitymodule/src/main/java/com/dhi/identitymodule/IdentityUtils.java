package com.dhi.identitymodule;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
/**
 * This class contains various utility functions used within identity module.
 * Please see individual function for explanation.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class IdentityUtils {
    public static long MILLIS_IN_DAY = 24*60*60*1000; //24 hrs in a day. each hr has 60*60 seconds. multiply by 1000 for ms
    /*
    this var is for deciding how accurate the date comparison of the DOB entered by the user and actual DOB should be.
    0 = high accuracy - actual DOB and value entered should be within 30 days range
    1 = medium accuracy - range of 45 days. Default value.
    2 = low accuracy - range of 60 days
     */
    private static int mAgeAccuracy = 1;


    /**
     *
     * This function computes the DOB from age. (age to be passed in days)
     * It returns the epoch value (DOB as epoch number).
     * We can also pass a StringBuffer that will contain the DOB in string format.
     * @param age (in days)
     * @param dobStr
     * @return
     */
    public static long getDOBFromAge(int age,StringBuffer dobStr)
    {
        //this will take current age (by default we assume age is in days).
        long epochDOB = getCurrentTimeInEpoch() - age;
        Date date = new Date(epochDOB*MILLIS_IN_DAY);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        dobStr.append(format.format(date).replaceAll("/",""));
        return epochDOB;
    }

    /**
     *
     * This is similar to above function but just returns the DOB as an epoch number.
     * @param age
     * @return : long
     */
    public static long getDOBFromAge(int age)
    {
        //this will take current age (by default we assume age is in days). however, we may change it to months by setting ageUnit
        return getCurrentTimeInEpoch() - age;
    }

    /**
     *
     * this gives the current time in epoch format in number of days
     * @return : long
     */

    public static long getCurrentTimeInEpoch()
    {
        Date curr = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat();

        String currentTime = dateFormat.format(curr);
        Date date = null;
        try {
            date = dateFormat.parse(currentTime);
        } catch (ParseException e) {
            Log.e("Curr-date error",e.toString());
            return 0;
        }
        return (date.getTime())/MILLIS_IN_DAY;
    }

    public static void setAgeAccuracy(int accuracyNum)
    {
        mAgeAccuracy = accuracyNum;
    }

    public static int getAgeAccuracy()
    {
        return mAgeAccuracy;
    }

    /**
     * This function checks if the age entered is accurate by comparing with the epoochDOB.
     * EpochDOB and age both have to be in days. We can also set the accuracy level to what we want. By default, it's 1 (Medium)
     * Returns true if the age is right else false.
     * @param age
     * @param epochDOB
     * @return
     */
    public static boolean validateAge(long age,long epochDOB)
    {
        //age has to be in days
        //long ageInDays = (getCurrentTimeInEpoch() - epochDOB);
        long ageInDays = epochDOB;
        switch(mAgeAccuracy)
        {
            case 0:
                if (java.lang.Math.abs(age - ageInDays) < 30)
                    return true;
                break;
            case 1:
                if (java.lang.Math.abs(age - ageInDays) < 45)
                    return true;
                break;
            case 2:
                if (java.lang.Math.abs(age - ageInDays) < 60)
                    return true;
                break;
            default:
                return false;
        }

        return false;
    }

    /**
     * Generate the string of size and return new string object.
     * @param size : int size
     * @return : string
     */
    public static String generateStringOfSize(int size)
    {
        char[] chars = new char[size];
        Arrays.fill(chars, 'a');
        return new String(chars);
    }

    /**
     * This function gives a JSON string from a given asset id.
     * @param context
     * @param id
     * @return : json string
     */
    public static String loadJSONFromAsset(Context context, int id) {
        String json = null;
        if (context == null)
            throw new IllegalArgumentException("Context unavailable");
        Resources res = context.getResources();
        try {
            InputStream is = res.getAssets().open(res.getString(id));
            int size = is.available();
            if (size == 0)
                return null;
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
