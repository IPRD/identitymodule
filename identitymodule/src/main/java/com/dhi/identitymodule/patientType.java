package com.dhi.identitymodule;
/**
 *
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public enum patientType {
    child, newborn, youth, postnatal, antenatal
}

