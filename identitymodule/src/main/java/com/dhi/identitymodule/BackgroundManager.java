package com.dhi.identitymodule;

import android.content.Context;
import android.util.Log;

/**
 * This class runs a background thread that fires every 20s (value below)
 * right now, we poll for location in this code.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class BackgroundManager extends Thread{
    Context mContext;
    final int THREAD_INTERVAL=60000; //make this configurable from a config file.
    private static BackgroundManager m_bkMgr;

    private BackgroundManager(Context ctx) {
        mContext = ctx;
    }

    public static BackgroundManager getBackgroundThread(Context ctx)
    {
        if (m_bkMgr != null)
            return m_bkMgr;
        else
        {
            m_bkMgr = new BackgroundManager(ctx);
            m_bkMgr.start();
            return m_bkMgr;
        }
    }

    public void run() {
        Log.i("LogMsg","In thread");
        LocationManager locManager = new LocationManager(mContext);
        boolean checkLoc = true;
        boolean doReplication = false;
        while (checkLoc || doReplication)
        {
            if (checkLoc)
            {
                if (!locManager.doUpdate())
                    Log.d("Loc Msg","Could not update location");
                else
                    checkLoc = false;
            }
            //replicate if enabled
            if (doReplication)
            {
                IdentityModule.getInstance(mContext).replicateData();
            }
            try {
                Thread.sleep(THREAD_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

