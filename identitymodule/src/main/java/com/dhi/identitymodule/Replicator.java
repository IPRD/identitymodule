package com.dhi.identitymodule;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.cloudant.http.internal.interceptors.CookieInterceptor;
import com.cloudant.sync.event.Subscribe;
import com.cloudant.sync.event.notifications.ReplicationCompleted;
import com.cloudant.sync.event.notifications.ReplicationErrored;
import com.cloudant.sync.replication.ReplicatorBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
/**
 * This class offers an interface to replicate data from the android local DB to a remote DB
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class Replicator {
    private static final String LOG_TAG = "Replicator";
    public enum ReplicateType
    {
        PULL, PUSH, FULL;
    }

    Context mAppContext;
    DeviceStore mStore;
    private com.cloudant.sync.replication.Replicator mPush;
    private com.cloudant.sync.replication.Replicator mPull;
    private final Handler mHandler;
    private IdentityModule mIDListener;
    private RemoteDB mRemoteDB;

    public Replicator(Context ctx, DeviceStore store, Map<String,String> dbParams) {
        mAppContext = ctx;
        mStore = store;
        mRemoteDB = new RemoteDB(dbParams);
        try {
            this.reloadReplicationSettings();
        } catch (URISyntaxException e) {
            Log.e(LOG_TAG, "Unable to construct remote database URI from configuration", e);
        }
        this.mHandler = new Handler(Looper.getMainLooper());
    }

    public boolean reloadReplicator(Map<String,String> dbParams)
    {
        if (!mRemoteDB.reloadRemoteDB(dbParams))
            return false;
        try {
            this.reloadReplicationSettings();
            return true;
        } catch (URISyntaxException e) {
            Log.e(LOG_TAG, "Unable to construct remote database URI from configuration", e);
            return false;
        }
    }

    public void setReplicationListener(IdentityModule listener) { this.mIDListener = listener; }

    /**
     * This method is used to stop replication
     *
     */
    public boolean stopReplication() {
        try {
            if (this.mPull != null) {
                this.mPull.stop();
                this.mPull = null;
            }
            if (this.mPush != null) {
                this.mPush.stop();
                this.mPush = null;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error while stopping replication process."+e.toString());
            return false;
        }
        return true;
    }

    /**
     * This method is used to start replication - either Push or pull or both.
     * @param type : ReplicateType
     * @return : boolean
     */
    public boolean startReplication(ReplicateType type) {
        try {
            if (type == ReplicateType.FULL && this.mPull != null && this.mPush != null) {
                this.startPull();
                this.startPush();
            } else if (type == ReplicateType.PULL && this.mPull != null) {
                this.startPull();
            } else if (type == ReplicateType.PUSH && this.mPush != null) {
                this.startPush();
            }
        } catch (InterruptedException e) {
            Log.e(LOG_TAG, "Error while starting replication process."+e.toString());
            return false;
        }
        return true;
    }

    private void startPull() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        Listener listener = new Listener(latch);
        mPull.getEventBus().register(listener);
        mPull.start();
        latch.await();
        mPull.getEventBus().unregister(listener);
        if (mPull.getState() != com.cloudant.sync.replication.Replicator.State.COMPLETE) {
            Log.e(LOG_TAG,"Error replicating FROM remote TO local");
            Log.e(LOG_TAG, listener.errors.toString());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mIDListener != null) {
                        mIDListener.replicationError(ReplicateType.PULL);
                    }
                }
            });
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mIDListener != null) {
                        mIDListener.replicationComplete(ReplicateType.PULL);
                    }
                }
            });
        }
        IdentityModule.getInstance(mAppContext).cacheData();
        SegmentationAndMatching.loadBioListFromDB(mAppContext);
    }

    private void startPush() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        Listener listener = new Listener(latch);
        mPush.getEventBus().register(listener);
        mPush.start();
        latch.await();
        mPush.getEventBus().unregister(listener);
        if (mPush.getState() != com.cloudant.sync.replication.Replicator.State.COMPLETE) {
            Log.e(LOG_TAG,"Error replicating FROM local TO remote");
            Log.e(LOG_TAG, listener.errors.toString());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mIDListener != null) {
                        mIDListener.replicationError(ReplicateType.PUSH);
                    }
                }
            });
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mIDListener != null) {
                        mIDListener.replicationComplete(ReplicateType.PUSH);
                    }
                }
            });
        }
    }

    /**
     * <p>Stops running replications and reloads the replication settings</p>
     */
    public boolean reloadReplicationSettings()
            throws URISyntaxException {

        this.stopReplication();

        // Set up the new replicator objects
        //sample URI object: uri = new URI("http://admin:apra1234@10.0.2.2:5984/clinic");
        URI uri;
        try {
            uri = this.getRemoteServerURI();
            Log.d("Remote URI: ",uri.toString());
        } catch (URISyntaxException val)
        {
            Log.e("Issue while encoding", val.getMessage());
            return false;
        }

        if (mRemoteDB.isLoginNeeded()) {
            CookieInterceptor ci = new CookieInterceptor(mRemoteDB.getUserName(), mRemoteDB.getPassword(), uri.toString());
            mPull = ReplicatorBuilder.pull().to(mStore.getDocumentStore()).from(uri)
                    .addRequestInterceptors(ci).addResponseInterceptors(ci).build();

            ci = new CookieInterceptor(mRemoteDB.getUserName(), mRemoteDB.getPassword(), uri.toString());
            mPush = ReplicatorBuilder.push().from(mStore.getDocumentStore()).to(uri)
                    .addRequestInterceptors(ci).addResponseInterceptors(ci).build();
        } else {
            mPull = ReplicatorBuilder.pull().to(mStore.getDocumentStore()).from(uri).build();
            mPush = ReplicatorBuilder.push().from(mStore.getDocumentStore()).to(uri).build();
        }

        mPush.getEventBus().register(this);
        mPull.getEventBus().register(this);

        Log.d(LOG_TAG, "Set up replicators for URI:" + uri.toString());
        return true;
    }

    /**
     * <p>Returns the URI for the remote database, based on the app's
     * configuration.</p>
     * @return the remote database's URI
     * @throws URISyntaxException if the settings give an invalid URI
     */
    private URI getRemoteServerURI() throws URISyntaxException {
        return mRemoteDB.getURI();
    }

    /**
     * A {@code ReplicationListener} that sets a latch when it's told the
     * replication has finished.
     * Date: Aug 30 2019
     * @author IPRD
     * @version 1.0
     */
    public class Listener {

        private final CountDownLatch latch;
        public List<Throwable> errors = new ArrayList<Throwable>();
        public int documentsReplicated = 0;
        public int batchesReplicated = 0;

        Listener(CountDownLatch latch) {
            this.latch = latch;
        }

        @Subscribe
        public void complete(ReplicationCompleted event) {
            this.documentsReplicated += event.documentsReplicated;
            this.batchesReplicated += event.batchesReplicated;
            latch.countDown();
        }

        @Subscribe
        public void error(ReplicationErrored event) {
            this.errors.add(event.errorInfo);
            latch.countDown();
        }
    }
}
