package com.dhi.identitymodule;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;
/**
 * This class represents a remote DB server to which we could replicate data from our android phone.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
class RemoteDB {
    private String mDBHost;
    private String mDBPort;
    private String mDBName;
    private boolean mDBSecure;
    private String mUser;
    private String mPwd;
    private URI mURI;

    RemoteDB(Map<String,String> dbParams) {
        if (!extractDBParams(dbParams))
            Log.e("Startup error","Could not read db config");
        if(!setURI())
            Log.e("Startup error","Unable to get the DB URI");
    }

    void setDBHost(String dbHost) {
        mDBHost = dbHost;
    }
    String getDBHost() {
        return mDBHost;
    }

    void setDBPort(String dbPort) {
        mDBPort = dbPort;
    }
    String getDBPort() { return mDBPort; }

    void setDBName(String dbName) {
        mDBName = dbName;
    }
    String getDBName() { return mDBName; }

    void changeLoginNeededFlag(boolean loginFlag) { mDBSecure = loginFlag; }
    boolean isLoginNeeded() { return mDBSecure; }

    void setUserName(String user) {
        mUser = user;
    }
    String getUserName() { return mUser; }

    void setPassword(String password) {
        mPwd = password;
    }
    String getPassword() { return mPwd; }

    public boolean reloadRemoteDB(Map<String,String> dbParams)
    {
        boolean ret = true;
        if (!extractDBParams(dbParams)) {
            Log.e("Startup error", "Could not read db config");
            ret = false;
        }
        if(!setURI()) {
            Log.e("Startup error", "Unable to get the DB URI");
            ret = false;
        }
        return ret;
    }

    private boolean extractDBParams(Map<String,String> dbParams)
    {
        mDBHost = dbParams.get("host");
        if (mDBHost == null)
            return false;
        mDBPort = dbParams.get("port");
        if (mDBPort == null)
            return false;
        mDBName = dbParams.get("dbName");
        if (mDBName == null)
            return false;
        String sLogin = dbParams.get("secureLogin");
        if (sLogin == null)
            return false;
        else
            mDBSecure = Boolean.parseBoolean(sLogin);
        if (mDBSecure)
        {
            //user name and pwd needed only if secure login is enabled
            mUser = dbParams.get("user");
            if (mUser == null)
                return false;
            mPwd = dbParams.get("password");
            if (mPwd == null)
                return false;
        }
        return true;
    }

    private boolean setURI() {
        String urlPrefix = "http://";
        if (mDBSecure) {
            try {
                urlPrefix += URLEncoder.encode(mUser,"UTF-8") + ":" + URLEncoder.encode(mPwd,"UTF-8") + "@";
            } catch (UnsupportedEncodingException e) {
                Log.e("Issue while encoding", e.getMessage());
                return false;
            }
        }
        mURI = URI.create(urlPrefix + mDBHost + ":" + mDBPort + "/" + mDBName);
        return true;
    }
    public URI getURI() { return mURI; }

}

