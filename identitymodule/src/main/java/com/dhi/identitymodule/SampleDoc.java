package com.dhi.identitymodule;

import android.support.annotation.Nullable;

import com.cloudant.sync.documentstore.DocumentRevision;

import java.util.HashMap;
import java.util.Map;
/**
 * This is a base class used to represent various documents.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class SampleDoc {
    static final String DOC_TYPE = "com.iprdgroup.idlink";

    private SampleDoc() { }

    public SampleDoc(String docName) {
        this.setName(docName);
        this.setCompleted(false);
        this.setType(DOC_TYPE);
    }

    // this is the revision in the database representing this task
    private DocumentRevision mRev;
    public DocumentRevision getDocumentRevision() {
        return mRev;
    }
    public void setDocumentRevision(DocumentRevision newRev) { mRev = newRev; }

    private String mType;
    public String getType() {
        return mType;
    }
    public void setType(String type) {
        this.mType = type;
    }

    private boolean mCompleted;
    public boolean isCompleted() {
        return this.mCompleted;
    }
    public void setCompleted(boolean completed) {
        this.mCompleted = completed;
    }

    private String mName;
    public String getName() {
        return this.mName;
    }
    public void setName(String name) {
        this.mName = name;
    }

    private String mIndType;
    public String getIndType() {
        return this.mIndType;
    }
    public void setIndType(String indType) { this.mIndType = indType; }

    private int mAge;
    public int getAge() {
        return this.mAge;
    }
    public void setAge(int age) { this.mAge = age; }

    private String mGender;
    public String getGender() {
        return this.mGender;
    }
    public void setGender(String gender) { this.mGender = gender; }

    private String mSkill;
    public String getSkill() {
        return this.mSkill;
    }
    public void setSkill(String skill) { this.mSkill = skill; }

    private String mDocContent;
    public String getContent() { return this.mDocContent; }
    public void setContent(String docContent) { this.mDocContent = docContent; System.out.printf(this.mDocContent); }

    private int mVersionNum;
    public int getVersionNum() {
        return mVersionNum;
    }

    public void setVersionNum(int versionNum) {
        this.mVersionNum = versionNum;
    }

    private String mPhoneID;

    public String getPhoneID() {
        return mPhoneID;
    }

    public void setPhoneID(String phoneID) {
        this.mPhoneID = phoneID;
    }


    @Override
    public String toString() {
        return "{ name: " + getName() + ", type: " + getIndType() + ", skill: " + getSkill() + ", age: " + getAge() + "}";
    }

    /***
     *
     * Creating SampleDoc from Revesion Document
     * @param rev : Document Revision
     * @return : SampleDoc
     */
    @Nullable
    public static SampleDoc fromRevision(DocumentRevision rev) {
        SampleDoc t = new SampleDoc();
        t.mRev = rev;
        Map<String, Object> map = rev.getBody().asMap();
        if(map.containsKey("type") && map.get("type").equals(SampleDoc.DOC_TYPE)) {
            t.setType((String) map.get("type"));
            t.setCompleted((Boolean) map.get("completed"));
            t.setName((String) map.get("name"));
            return t;
        }
        return null;
    }

    /**
     * This method returns a map that has all the data contained in this class.
     * @return
     */
    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", mType);
        map.put("completed", mCompleted);
        map.put("name", mName);
        map.put("indType",mIndType);
        map.put("age",mAge);
        map.put("gender",mGender);
        map.put("skill",mSkill);
        map.put("version",mVersionNum);
        map.put("phoneID",mPhoneID);
        return map;
    }
}
