package com.dhi.identitymodule;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import org.iprd.sharedutils.*;
import org.iprd.sharedutils.RestAPIHandler;
//import org.iprd.sharedutils.RestAPIHandler;

/**
 * This class acts as an interface to request an OTP to be sent to a particular phone number (for authentication) as well as verify an OTP sent to a phone number
 * Internally, it contacts a Twillio sms service using REST API for the above functionality. (See RestAPIHandler in sharedutils for more)
 * This was implemented for authenticating phone numbers during the login process. However, this feature is not being used at this moment.
 * Note: this requires a custom API key. Twillio is a paid service and one may need to procure an API key for using this functionality.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class SmsService {
    String mSmsServer = "";
    RestAPIHandler mRestClient;
    String mAPIKey;
    final String VERIFY_URL="/protected/json/phones/verification/check?";
    final String SEND_SMS_URL="/protected/json/phones/verification/start?";
    final String PHONE_VAR="phone_number";
    final String COUNTRY_CODE="country_code";
    final String VERIFICATION_CODE="verification_code";
    final String SMS_VIA="via";
    final String API_STR="api_key";
    final String API_KEY="RBzQGUPGF7KOLjboMGXRjM01RbVdLGuN";

    public SmsService() {
        mSmsServer = "api.authy.com";
        mRestClient = new RestAPIHandler(mSmsServer);
        setAPIKey(API_KEY);
    }

    public String getAPIKey() {
        return mAPIKey;
    }

    public void setAPIKey(String apiKey) {mAPIKey = apiKey;}

    /**
     *
     * This method is used to send a verification Msg to a given phone number
     * @param phoneNum
     * @param countryCode
     * @param apiKey
     * @param howToSendMsg
     * @param rspStr
     * @return : boolean
     */
    public boolean sendVerificationMsg(String phoneNum,String countryCode,String apiKey,String howToSendMsg,StringBuffer rspStr) {
        String fullSMSURL=SEND_SMS_URL+API_STR+"="+API_KEY+"&"+SMS_VIA+"="+howToSendMsg+"&"+PHONE_VAR+"="+phoneNum+"&"+COUNTRY_CODE+"="+countryCode;
        String postBody = "";
        return mRestClient.handlePostRequest(fullSMSURL,postBody,rspStr,true);
    }

    /**
     *
     * This method is used for veryfying a code sent to a phone number
     * @param phoneNum
     * @param countryCode
     * @param verificationCode
     * @param rspStr
     * @return
     */
    public boolean verifyCode(String phoneNum,String countryCode,String verificationCode,StringBuffer rspStr) {
        String fullVerifyURL=VERIFY_URL+PHONE_VAR+"="+phoneNum+"&"+COUNTRY_CODE+"="+countryCode+"&"+ VERIFICATION_CODE+"="+verificationCode;
        String fullReq = "https://api.authy.com"+fullVerifyURL;
        Map<String,String> headerParams = new HashMap<String,String>();
        headerParams.put("X-Authy-API-Key","RBzQGUPGF7KOLjboMGXRjM01RbVdLGuN");
        if (!mRestClient.handleGetRequest(fullVerifyURL,rspStr,true,headerParams))
            return false;
        //process rspStr to send the output.
        return true;
    }

    public boolean testCodeVerification() {
        StringBuffer rspStr = new StringBuffer();
        if (!verifyCode("7829246387","91","9402",rspStr))
            return false;
        else
        {
            Log.d("Output",rspStr.toString());
            return true;
        }
    }

    public boolean testCodeSend() {
        StringBuffer rspStr = new StringBuffer();
        if (!sendVerificationMsg("7829246387","91",API_KEY,"sms",rspStr))
            return false;
        else
        {
            Log.d("Output",rspStr.toString());
            return true;
        }
    }
}

