package com.dhi.identitymodule;

import java.util.HashMap;
import java.util.Map;
/**
 * This class is a derived class from SampleDoc that stores transactions.
 * Sample transactions: searching for some user, found a user, login a user
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class TransactionDoc extends SampleDoc {
    public TransactionDoc(String name, String txGUID, String txtime,String hcwGUID,String patientGUID,String txcontent) {
        super(name); // name is the type of transaction that was done. Ex: a HCW added a new patient
        this.setTxGUID(txGUID); //this is a GUID generated for the transaction/
        this.setHcwGUID(hcwGUID); //Where applicable, GUID of the HCW who did this transaction - either login or adding a patient, et.
        this.setPatientGUID(patientGUID); //where applicable, GUID of the patient. if no patient involved, this is ""
        this.setTxtime(txtime); //current time when thi happened
        this.setTxContent(txcontent); //details of waht was done. ex: if a HCW logged in, this should be th HCW user or if a patient was added, the name of the patient who got added.
    }

    private String mTxGUID;
    public String getTxGUID() {
        return this.mTxGUID;
    }
    public void setTxGUID(String txGUID) { this.mTxGUID = txGUID; }

    private String mTxtime;
    public String getTxTime() {
        return this.mTxtime;
    }
    public void setTxtime(String txtime) {
        this.mTxtime = txtime;
    }

    private String mHcwGUID;
    public String getHcwGUID() {
        return this.mHcwGUID;
    }
    public void setHcwGUID(String hcwGUID) { this.mHcwGUID = hcwGUID; }

    private String mPatientGUID;
    public String getPatientGUID() { return this.mPatientGUID; }
    public void setPatientGUID(String patientGUID) { this.mPatientGUID = patientGUID; }

    private String mTxContent;
    public String getTxContent() { return this.mTxContent; }
    public void setTxContent(String txcontent) { this.mTxContent = txcontent; }

    /**
     *
     * @return
     */
    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", getType());
        map.put("completed", isCompleted());
        map.put("name", getName());
        map.put("txGUID",mTxGUID);
        map.put("txtime",mTxtime);
        map.put("hcwGUID",mHcwGUID);
        map.put("patientGUID",mPatientGUID);
        map.put("TxContent",mTxContent);
        return map;
    }

}
