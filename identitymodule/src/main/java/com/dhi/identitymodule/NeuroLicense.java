package com.dhi.identitymodule;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.neurotec.lang.NCore;
import com.neurotec.licensing.NLicense;
import com.neurotec.plugins.NDataFileManager;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class NeuroLicense {
    private static final String TAG = NeuroLicense.class.getName();
    private static Context mCtx;
    private boolean m_obtainedAlllic=false;
    private Map<String,Boolean> mLicmap=new HashMap<String ,Boolean>();
    String Components = "FingerClient,IrisClient,FingerMatcher,IrisMatcher,FaceClient,FaceMatcher";//??

    public NeuroLicense(Context c){
        mCtx=c;
        NCore.setContext(mCtx);
        NDataFileManager.getInstance().addFromDirectory( Environment.getExternalStorageDirectory().getAbsolutePath() + "/Neurotechnology", false);
    }
    public boolean getLicStatus(){ return m_obtainedAlllic;}
    public boolean getFPLicStatus(){
        boolean b = mLicmap.getOrDefault("FingerClient",false);
        boolean b1 = mLicmap.getOrDefault("FingerMatcher",false);
        if (b1 && b){ return true;}
        else {return false;}
    }
    public boolean getIrisLicStatus(){
        boolean b = mLicmap.getOrDefault("IrisClient",false);
        boolean b1 = mLicmap.getOrDefault("IrisMatcher",false);
        if (b1 && b){ return true;}
        else {return false;}
    }
    public boolean getFaceLicStatus(){ //?
        boolean b = mLicmap.getOrDefault("FaceClient",false);
        boolean b1 = mLicmap.getOrDefault("FaceMatcher",false);
        if (b1 && b){ return true;}
        else {return false;}
    }

    /**
     * Converts stream to string from given input stream data.
     * @param is : Input Stream.
     * @return : String
     * @throws IOException
     */
    public static String convertStreamToString(InputStream is) throws IOException {
        // http://www.java2s.com/Code/Java/File-Input-Output/ConvertInputStreamtoString.htm
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        Boolean firstLine = true;
        while ((line = reader.readLine()) != null) {
            if(firstLine){
                sb.append(line);
                firstLine = false;
            } else {
                sb.append("\n").append(line);
            }
        }
        reader.close();
        return sb.toString();
    }

    /**
     *
     * Getting string from file, method parameter is file path.
     * @param filePath
     * @return
     * @throws IOException
     */
    public static String getStringFromFile (String filePath) throws IOException {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    /**
     *
     * @return
     */
    public static String loadExternalDisk(){
        return "/mnt/sdcard";
    }

    /**
     *
     * Load license from file
     * @return : boolean
     */
    protected boolean loadLicenseFromfile()
    {
        boolean rt = false;
        String basedir=loadExternalDisk();
        String licpath= basedir + "/Neurotechnology/Licenses";
        NDataFileManager.getInstance().addFromDirectory(basedir+"/Neurotechnology", false);
        File dir = new File(licpath);
        File[] licFiles = dir.listFiles();
        Log.i(TAG,Long.toString(licFiles.length));

        for(int i=0;i< licFiles.length;i++){
            String licPath=licpath+"/"+licFiles[i].getName();
            if(licPath.contains(".lic")) {
                try {
					Log.i(TAG,"Adding LIC file " + licPath);
                    String content = getStringFromFile(licPath);
                    NLicense.add(content);
                    rt = true;
                } catch (Exception ex) {
                    Log.e(TAG, "Add License Failed for file " + licPath + " " + ex.getMessage());
                }
            }
        }
       return rt;
    }

    public boolean checkLic() {
        String r[] ={"Biometrics.IrisExtraction","Biometrics.IrisMatching","Biometrics.FingerExtraction","Biometrics.FingerMatching"};
        boolean ret = false;
        for(int i=0;i<4;i++) {
            try {
                ret = !( NLicense.isComponentActivated(r[i]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return !ret;
    }

    /**
     * Check if obtains the License.
     * @param server : server
     * @param port : port
     * @param components : components
     * @return : boolean
     */
    boolean obtainLic(String  server ,int port , String components){
        boolean rt = false;
        try
        {
            rt = NLicense.obtain(server, port, components);
        }
        catch (Exception ex)
        {
            String message = "Failed to obtain licenses for components.\nError message: "+ ex.getMessage();
            Log.e(TAG,message);
        }
        return rt;
    }

    /**
     *
     * License check with server
     * @return : boolean
     */
    public boolean licenseCheckWithServer(){
        boolean ret = false;
        m_obtainedAlllic = false;
        boolean test = false;
//        String Components = "Biometrics.IrisExtraction,Biometrics.IrisMatching,Biometrics.FingerExtraction,Biometrics.FingerMatching";
        String Components = "FingerClient,IrisClient,FingerMatcher,IrisMatcher,FaceClient,FaceMatcher";//??
        try {
            loadLicenseFromfile();
            List<String> items = Arrays.asList(Components.split("\\s*,\\s*"));
            for (int i = 0; i < items.size(); i++) {
                  ret = obtainLic("/local", 5000, items.get(i));
                  mLicmap.put(items.get(i),ret);
                  test = (test || (!ret));
                  Log.i(TAG,(ret?"Successfully able to get Lic for ":"Unable to get lic for ") + items.get(i));
            }
            if (test){
                ret = false;
                m_obtainedAlllic = false;
            }else{
                m_obtainedAlllic = true;
				ret= true;
            }
            return ret;
        }
        catch (Exception ex) {
            Log.e(TAG,ex.getMessage());
        }
        return ret;
    }
}
