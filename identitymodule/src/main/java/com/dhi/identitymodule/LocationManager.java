package com.dhi.identitymodule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
/**
 * This class gives the location using the Google location service
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
class LocationManager {
    private FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;
    Context mLocContext;

    @SuppressLint("MissingPermission")
    LocationManager(Context ctx) {
        mLocContext = ctx;
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mLocContext.getApplicationContext());
        this.mFusedLocationClient = new FusedLocationProviderClient(ctx);
        if (this.mFusedLocationClient == null)
            Log.d("LocationError","Loc client is null");
        else
            Log.d("LocationDetails",mFusedLocationClient.toString());
        createmLocationRequest();
    }


    private void createmLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
    }

    @SuppressLint("MissingPermission")
    private boolean startLocationUpdates() {
        if (Looper.myLooper() == null)
        {
            Looper.prepare();
        }
        Log.d("LocationUpdate","starting loc updates");

        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                                                                       public void onSuccess(Location location) {
                                                                           if (location != null)
                                                                               SessionProperties.getInstance(mLocContext).setLocationStr(getActualLocation(location));
                                                                       }
                                                                   }
        );
        return true;
    }

    private void processLocation() {
        return;
    }

    private boolean monitorLocation()
    {
        if ((SessionProperties.getInstance(mLocContext).getLocationStr() == null) || (SessionProperties.getInstance(mLocContext).getLocationStr() == "") || (SessionProperties.getInstance(mLocContext).getLocationStr().equals("Unknown")))
            return false;
        else
            return true;
    }

    private String getActualLocation(Location location) {
        Geocoder geoCoder = new Geocoder(mLocContext, Locale.getDefault());
        String finalAddress = "";
        try {
            Log.d("GPS::"+Double.toString(location.getLatitude()),Double.toString(location.getLongitude()));
            List<Address> address = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            finalAddress = address.get(0).getLocality(); //This is the complete address.
        } catch (IOException e) {
            finalAddress="";
        }
        catch (NullPointerException e) {
            finalAddress="";
        }
        if((finalAddress ==null)||(finalAddress.length()==0)){
            finalAddress ="Unknown";
        }
        Log.d("GPS::",finalAddress);
        return finalAddress;
    }

    boolean doUpdate() {
        startLocationUpdates();
        if (!monitorLocation())
            return false;
        Log.d("LocationTest","in doWork function");
        return true;
    }
}
