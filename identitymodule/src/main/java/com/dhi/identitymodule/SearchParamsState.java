package com.dhi.identitymodule;

import android.content.Context;
import android.util.Log;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * *This tells us the state of every search param
 * Ex: if a user enters name and it is found, then the search parameter "name" will have a state value FOUND
 * if a user does not specify right fingerprint at all, then the search parameter "right FP" will have a state value NOT_ENTERED.
 *
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */

public class SearchParamsState {
    private static SearchParamsState mOurInstance = null;
    private static final int NOT_ENTERED = 0;
    private static final int NOT_FOUND = 1;
    private static final int FOUND = 2;
    private Map<String, Integer> mSearchParamsStateMap;
    private int mTotalStateValue = 0;
    private Map<Integer,Integer> mTotalStateValueToOutcomeMap;
    private Context mCtx;

    public static SearchParamsState getInstance(Context ctx) {
        if (mOurInstance == null) {
            mOurInstance = new SearchParamsState(ctx);
        }
        return mOurInstance;
    }

    private SearchParamsState(Context ctx) {
        mCtx = ctx;
        mSearchParamsStateMap = new HashMap<String,Integer>();
        mTotalStateValueToOutcomeMap = new HashMap<Integer, Integer>();
        loadMapFromFile(ctx);
    }

    /**
     *
     * This method reads a truth table file and loads a map containing the total state value (based on whether various biometric entries are present) to its outcome.
     * @param ctx
     * @return : boolean
     */
    public boolean loadMapFromFile(Context ctx) {
        String truthTableFromFile = "";
        if (SessionProperties.getInstance(ctx).getAppType() == AppType.GENERAL_APP)
            truthTableFromFile = IdentityUtils.loadJSONFromAsset(ctx,R.string.truthtable);
        else if (SessionProperties.getInstance(ctx).getAppType() == AppType.BIOMETRIC_ONLY)
            truthTableFromFile = IdentityUtils.loadJSONFromAsset(ctx,R.string.truthtableBio);
        else {
            Log.e("App type error", "Invalid app type: "+SessionProperties.getInstance(ctx).getAppType().toString());
            return false;
        }
        if (truthTableFromFile == null || truthTableFromFile.length() == 0) {
            Log.e("App type error","Invalid truth table file");
            return false;
        }
        String truthTableStr = truthTableFromFile.replaceAll("\\r","");
        if (truthTableStr == null || truthTableStr.length() == 0) {
            Log.e("App type error","Invalid truth table recvd");
            return false;
        }
        String delim1 = "\n";
        String delim2 = ",";
        String [] lines = truthTableStr.split(delim1);
        if (lines.length == 0) {
            Log.e("App type error","invalid no of lines");
            return false;
        }
        for (int i=0;i<lines.length;i++)
        {
            String [] indLineSplit = lines[i].split(delim2);
            if (indLineSplit.length < 1)
                return false;
            mTotalStateValueToOutcomeMap.put(Integer.parseInt(indLineSplit[1]),Integer.parseInt(indLineSplit[0]));
        }
        return true;
    }

    /**
     *
     *
     * This function adds parameter state value for a given search parameter
     * @param searchParam
     * @param stateValue
     */
    public void addParamState(String searchParam,int stateValue) {
        mSearchParamsStateMap.put(searchParam,stateValue);
    }

    /**
     *
     * This function gets a state value for a given search parameter.
     * @param searchParam
     * @return : int
     */
    public int getParamStateValue(String searchParam) {
        if (mSearchParamsStateMap.isEmpty())
            return -1;
        if (!mSearchParamsStateMap.containsKey(searchParam))
            return -1;
        return mSearchParamsStateMap.get(searchParam);
    }

    /**
     *
     * This gets the total state value for a user - based on all parameters.
     * Should ideally be called once we are done processing every search parameter - maybe somewhere in isUserPresent
     */
    public void setTotalStateValue() {
        int ageVal,uidVal,nameVal,rightFPVal,leftFPVal,eyeVal,faceVal = 0;
        if (SessionProperties.getInstance(mCtx).getAppType() == AppType.GENERAL_APP) {
            ageVal = (1) * getParamStateValue("age");
            uidVal = (10) * getParamStateValue("uid");
            nameVal = (100) * getParamStateValue("name");
            rightFPVal = (1000) * getParamStateValue("rightFP");
            leftFPVal = (10000) * getParamStateValue("leftFP");
            eyeVal = (100000) * getParamStateValue("bestEye");
            faceVal = (1000000) * getParamStateValue("face");
            mTotalStateValue = ageVal+uidVal+nameVal+rightFPVal+leftFPVal+eyeVal+faceVal;
            Log.i("State value is ",Integer.toString(mTotalStateValue));
        }
        else if (SessionProperties.getInstance(mCtx).getAppType() == AppType.BIOMETRIC_ONLY)
        {
            uidVal = (1) * getParamStateValue("uid");
            rightFPVal = (10) * getParamStateValue("rightFP");
            leftFPVal = (100) * getParamStateValue("leftFP");
            eyeVal = (1000) * getParamStateValue("bestEye");
            mTotalStateValue = uidVal+rightFPVal+leftFPVal+eyeVal;
        }
    }

    /**
     *
     * @return : int
     */
    public int getTotalStateValue() {
        return mTotalStateValue;
    }

    /**
     * This function gets the result (if user is present or not present) based on the total state value at that time.
     * A return value of -2 indicates the state value is invalid.
     * @return : int
     */
    public int getFinalOutcome() {
        Log.d("Size of truth table map: ",Integer.toString(mTotalStateValueToOutcomeMap.size()));
        if (!mTotalStateValueToOutcomeMap.containsKey(mTotalStateValue))
            return -2; //the total state value is probably not done right
        return mTotalStateValueToOutcomeMap.get(mTotalStateValue);
    }
}
