package com.dhi.identitymodule;

import java.util.HashMap;
import java.util.Map;
/**
 * This class is a derived class from SampleDoc that stores biometric data.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class BiometricDoc extends SampleDoc {
    private Map<String,String> mBioData;
    private String mBiometricData;


    public BiometricDoc(String docName,Map<String,String> mBiometricData) {
        super(docName);
        mBioData = new HashMap<String,String>();
        if (mBiometricData.size() != 0)
            mBioData.putAll(mBiometricData);
    }

    public BiometricDoc(String docName,String bioJSON) {
        super(docName);
        mBiometricData = bioJSON;
    }
    public String getmBiometricData() {
        return mBiometricData;
    }

    public void setmBiometricData(String mBiometricData) {
        this.mBiometricData = mBiometricData;
    }

    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("BiometricName",getName());
        map.put("BiometricData",mBiometricData);
        return map;
    }
}
