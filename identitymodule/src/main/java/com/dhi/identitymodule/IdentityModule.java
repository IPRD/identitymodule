package com.dhi.identitymodule;

import android.content.Context;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.cloudant.sync.documentstore.ConflictException;
import com.cloudant.sync.documentstore.DocumentBodyFactory;
import com.cloudant.sync.documentstore.DocumentException;
import com.cloudant.sync.documentstore.DocumentNotFoundException;
import com.cloudant.sync.documentstore.DocumentRevision;
import com.cloudant.sync.documentstore.DocumentStoreException;
import com.cloudant.sync.internal.documentstore.InternalDocumentRevision;
import com.cloudant.sync.query.FieldSort;
import com.cloudant.sync.query.Index;
import com.cloudant.sync.query.QueryException;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
/**
 * This initializes the core identity services - including db required for this.
 * This also has functions that can be used to do db related functions as well as adding and searching for users.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */

public class IdentityModule  {
    final int INDIVIDUAL_DOC = 0;
    final int TRANSACTION_DOC = 1;
    final int BIOMETRIC_DOC = 2;
    final int GENERAL_DOC = 3;
    final String USR_SEARCH_TRANSACTION = "User search";
    final String USR_LOGIN_TRANSACTION = "User Login";
    final String HCW_CONSENT_TRANSACTION = "HCW Consent";
    final String PATIENT_CONSENT_TRANSACTION = "Patient Consent";
    final String PATIENT_POS_TRANSACTION = "Patient POS";
    final String SUBJECT_NAME = "$SUBJECT";
    final static int INVALID_GUID = 0;
    final static int HCW = 1;
    final static int PATIENT = 2;
    final static int TRANSACTION  = 3;
    final static int BIOMETRIC_DATA = 10;
    final static int BIOMETRIC_HCW = 4;
    final static int BIOMETRIC_PATIENT = 5;
    final static int BIOMETRIC_FINGER_RIGHT = 6;
    final static int BIOMETRIC_FINGER_LEFT = 7;
    final static int BIOMETRIC_EYE_RIGHT = 8;
    final static int BIOMETRIC_EYE_LEFT = 9;
    final static int GENERAL_DATA = 11;
    final static int GUID_INDICATOR_SIZE = 4;
    final char OPENING_BRACE = '{';
    final char CLOSING_BRACE = '}';
    final int DB_ONLY = 1;
    final int CACHE_ONLY = 2;
    final int DB_AND_CACHE = 3;
    final int INITIAL_VERSION = 0;

    int mSearchMethod = CACHE_ONLY;
    private DeviceStore mStore;
    private DeviceStore mLocalStore;
    private Replicator mReplicator;
    private Context mIDContext;
    DocsListAdapter mDocsAdapter;
    private int mHcwCt;
    private int mPatientCt;
    private List<String> mTxList;
    private static IdentityModule mIdmInstance = null;
    private Set<String> mSpecifiedSearchParams;

    private IdentityModule(Context ctx) {
        mIDContext = ctx;
        mSpecifiedSearchParams = new HashSet<String>();
        //this tells us which search parameters are even allowed for searching.
        //ex: name, phone number should be acceptable search parameters, but skill may not be one
        mSpecifiedSearchParams.add("name");
        mSpecifiedSearchParams.add("phone");
        mSpecifiedSearchParams.add("countryCode");
        mSpecifiedSearchParams.add("DOB");
        mSpecifiedSearchParams.add("indType");

        if (mStore == null) {
            this.mStore = new DeviceStore(ctx,"USER");
        }

        if (mLocalStore == null)
            this.mLocalStore = new DeviceStore(ctx,"LOCAL");

        //reloadDocsFromStore();
        mTxList = new ArrayList<String>();
        cacheData();
        
    }

    /**
     * This method initializes the replicator svc using which we can replicate data from the local android db to a remote DB
     * For now, we have hardcoded the values of the host,db name,login for the remote DB
     * @param ip
     * @param ctx
     * @return
     */
    public boolean initReplicator(String ip,Context ctx)
    {
        Map<String,String> dbParams = new HashMap<String,String>();
        dbParams.put("host",ip);
        dbParams.put("port","5984");
        dbParams.put("dbName","clinic");
        dbParams.put("secureLogin","false");
        dbParams.put("user","admin");
        dbParams.put("password","apra1234");
        if (mReplicator == null) {
            this.mReplicator = new Replicator(ctx, mStore, dbParams);
        }
        else {
            if (!mReplicator.reloadReplicator(dbParams))
                return false;
        }
        this.mReplicator.setReplicationListener(this);
        return true;
    }

    /**
     * This is a function that may be used to index data from the sqlite db
     * for this, we need to pass the fields that need to be indexed.
     * Note: this is different from the cache data function below which stores the data in internal memory.
     * Here, some optimization is done by the DB itself.
     * We have not used it since we did not see an improvement in performance due to this.
     * @param fields2index
     * @return
     */
    boolean indexData(List<String> fields2index)
    {
        //read list and add each of the entries to the fields list to be indexed.
        List<FieldSort> fields = new ArrayList<FieldSort>();
        for (int i=0;i<fields2index.size();i++)
        {
            fields.add(new FieldSort(fields2index.get(i)));
        }
        return this.getDeviceStore().setQueryTextIndex(fields,"idNameIndex");
    }


    /***
     * Store the db data in memory.
     * Query db for all guids
     * for every guid, get the name, uid,age. store in a hashmap.
     * basically, we get 3 hashmaps: age->guid(s), name->guid(s), uid->guid(s)
     * Basically each of them has the format: key: String,Value: List<String>
     * While adding:
     * Suppose you are adding a user with name u1 and uid id1. Check if u1 is present. if yes, then add its guid to the entry u1
     * @return : boolean
     */
    public boolean cacheData()
    {
        List<String> userList;
        try {
            userList = this.mStore.getDocumentStore().database().getIds();
            for (int i=0;i<userList.size();i++)
            {
                //for each entry get name,uid,age
                String guid = userList.get(i);
                String indType = "";
                if (getGUIDType(guid) == HCW)
                    indType = "HCW";
                else if (getGUIDType(guid) == PATIENT)
                    indType = "Patient";
                if (indType.equals("HCW") || indType.equals("Patient"))
                {
                    String name = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("name");
                    String uid = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("uid");
                    String age = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("age");
                    String photo = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("photo");
                    UserCache.getCache().addDataToCache(indType,"name",name,guid);
                    UserCache.getCache().addDataToCache(indType,"uid",uid,guid);
                    UserCache.getCache().addDataToCache(indType,"age",age,guid);
                    UserCache.getCache().addDataToCache(indType,"photo",photo,guid);
                }
            }
            return true;
        } catch (DocumentStoreException dEx) {
            return false;
        } catch (DocumentNotFoundException dEx1) {
            Log.e("Caching error",dEx1.toString());
            return false;
        }
    }

    /**
     * For removing the index given by index name
     * @param indexName
     * @return
     */
    boolean rmIndex(String indexName) {
        try {
            this.getDeviceStore().getDocumentStore().query().deleteIndex(indexName);
            return true;
        } catch (QueryException qEx) {
            return false;
        }
    }

    /**
     * This method gets the list of all indices. They are populated in the indList list passed as an argument.
     * Function returns true for success and false if it encounters any failure.
     * @param indList
     * @return
     */
    boolean getIndices(List<Index> indList) {
        try {
            indList = this.getDeviceStore().getDocumentStore().query().listIndexes();
        } catch (QueryException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     *
     * remove all indices.
     * @return : boolean.
     */
    boolean rmAllIndices()
    {
        List<Index> indList = new ArrayList<Index>();
        boolean indRemoval = true;
        for (int i=0;i<indList.size();i++)
        {
            if(!rmIndex(indList.get(i).toString()))
                indRemoval = false;
        }
        return indRemoval;
    }


    /**
     * This method checks if indexing has been done. True means yes. False is no.
     * @return
     */
    boolean isIndexingDone()
    {
        //this is needed cos indexing should be done just once.
        List<Index> indList = null;
        try {
            indList = this.getDeviceStore().getDocumentStore().query().listIndexes();
        } catch (QueryException e) {
            e.printStackTrace();
            return false;
        }
        if (indList.size() > 0)
            return true;
        else
            return false;
    }

    DeviceStore getDeviceStore() {
        return mStore;
    }

    public static IdentityModule getInstance(Context ctx) {
        if (mIdmInstance == null)
            mIdmInstance = new IdentityModule(ctx);
        return mIdmInstance;
    }

    /**
     * This is a static function that gets the current time and returns as a string
     * @return
     */
    static String getCurrentTime() {
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    }

    /**
     * This is a function to check if a given guid is a HCW.
     * @param guid
     * @return
     */
    static boolean isHCW(String guid){
        return (guid.startsWith("HCW_"));
    }

    /**
     * This is a function to check if the given guid is a patient
     * @param guid
     * @return
     */
    static boolean isPatient(String guid) {
        return (guid.startsWith("PAT_"));
    }

    /**
     * This is a function to check if a guid is a biometric GUID - either of a biometric doc for HCW/Patient.
     * Or could also be an individual biometric doc - like Fingerprint or eye image.
     * @param guid :
     * @return : boolean
     */
    static boolean isBiometric(String guid) {
        String guidIndicator = guid.substring(0,GUID_INDICATOR_SIZE);
        if (guidIndicator.equals("BHW_") || guidIndicator.equals("BPT_") || (guidIndicator.equals("BFR_")) || (guidIndicator.equals("BFL_")) || (guidIndicator.equals("BER_")) || (guidIndicator.equals("BEL_")))
            return true;
        return false;
    }




    static boolean isBioMasterDoc(String guid)
    {
        String guidIndicator = guid.substring(0,GUID_INDICATOR_SIZE);
        if (guidIndicator.equals("BHW_") || guidIndicator.equals("BPT_"))
            return true;
        else
            return false;
    }


    /**
     * This is a static function to generate a GUID based on the ID Type.
     * ID type could be one of the following: HCW,Patient,Transaction,Biometric-HCW,Biometric-Patient,Biometric-RightFinger,Biometric-LeftFinger,Biometric-RightEye,Biometric-LeftEye
     * GUID's have the following format: IndicatorString_randomString
     * Ex: HCW_aakdalkdjlajd. Here the first part HCW indicates this is a HCW. the second part is a random string.
     * @param idType
     * @return
     */
    static String getGUID(String idType) {
        String docId = "";
        switch(idType)
        {
            case "HCW":
                docId = "HCW_" + UUID.randomUUID().toString();
                break;
            case "Patient":
                docId = "PAT_" + UUID.randomUUID().toString();
                break;
            case "Transaction":
                docId = "INT_" + UUID.randomUUID().toString();
                break;
            case "Biometric-HCW":
                docId = "BHW_" + UUID.randomUUID().toString();
                break;
            case "Biometric-Patient":
                docId = "BPT_" + UUID.randomUUID().toString();
                break;
            case "Biometric-RightFinger":
                docId = "BFR_" + UUID.randomUUID().toString();
                break;
            case "Biometric-LeftFinger":
                docId = "BFL_" + UUID.randomUUID().toString();
                break;
            case "Biometric-RightEye":
                docId = "BER_" + UUID.randomUUID().toString();
                break;
            case "Biometric-LefttEye":
                docId = "BEL_" + UUID.randomUUID().toString();
                break;
            case "Biometric-face":
                docId = "BFC_" + UUID.randomUUID().toString();
                break;
            case "General":
                docId = "GEN_" + "0001";
                break;
            default:
                break;
        }
        return docId;
    }

    /**
     *
     * This is similar to the above function. May be used to generate a GUID.
     * However, here, we can pass a String to be appended to the GUID instead of some random string.
     * GUID's are of the following typr: IndicatorString_randomString
     * The Indicator String is decided based on the Id type. However the randomString is generated randomly in the previous function
     * In this function, you can decide what the random string should be.
     * This is helpful in biometric docs. Ex: the biometric doc for a HCW always has the same random string. Only the indicator string is different
     * Ex: if there is a HCW: HCW_abcd. His biometric doc will be: BHW_abcd.
     * @param idType
     * @param guid
     * @return
     */
    static String getGUID(String idType,String guid) {
        String docId = "";
        switch(idType)
        {
            case "Biometric-HCW":
                docId = "BHW_" + guid.toString();
                break;
            case "Biometric-Patient":
                docId = "BPT_" + guid.toString();
                break;
            case "Biometric-RightFinger":
                docId = "BFR_" + guid.toString();
                break;
            case "Biometric-LeftFinger":
                docId = "BFL_" + guid.toString();
                break;
            case "Biometric-RightEye":
                docId = "BER_" + guid.toString();
                break;
            case "Biometric-LeftEye":
                docId = "BEL_" + guid.toString();
                break;
            case "Biometric-face":
                docId = "BFC_" + guid.toString();
                break;
            case "Transaction":
                docId = "INT_" + guid.toString();
                break;
            default:
                break;
        }
        return docId;
    }

    public void reloadDocsFromStore() {
        try {
            this.mDocsAdapter = new DocsListAdapter(mIDContext, this.mStore.allDocs());
        } catch (DocumentStoreException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     *
     * Called by TasksModel when it receives a replication complete callback.
     * TasksModel takes care of calling this on the main thread.
     * @param type : Replicator.ReplicateType
     */
    void replicationComplete(Replicator.ReplicateType type) {
        //reloadDocsFromStore();
        Toast.makeText(mIDContext, (type == Replicator.ReplicateType.PUSH? "Push": "Pull")
                        + " Replication Completed",
                Toast.LENGTH_LONG).show();
    }

    /**
     * Called by TasksModel when it receives a replication error callback.
     * TasksModel takes care of calling this on the main thread.
     *  @param type : Replicator.ReplicateType
     */

    void replicationError(Replicator.ReplicateType type) {
        //reloadDocsFromStore();
        Toast.makeText(mIDContext, (type == Replicator.ReplicateType.PUSH? "Push": "Pull")
                        + " Replication Error",
                Toast.LENGTH_LONG).show();
    }

    /**
     * This function replicates the data to a remote DB server.
     * @return
     */
    public boolean replicateData() {
        if (mReplicator == null)
            return false;
        return this.mReplicator.startReplication(Replicator.ReplicateType.FULL);
    }

    /**
     * This is a static function that tells us the GUID type depending on the input GUID.
     * If it returns INVALID_GUID, it means it's a wrong GUID.
     * @param guid : GU id (String type)
     * @return : int
     */
    public static int getGUIDType(String guid) {
        String guidIndicator = guid.substring(0,GUID_INDICATOR_SIZE);
        switch (guidIndicator)
        {
            case "HCW_":
                return HCW;
            case "PAT_":
                return PATIENT;
            case "INT_":
                return TRANSACTION;
            case "BHW":
                return BIOMETRIC_HCW;
            case "BPT":
                return BIOMETRIC_PATIENT;
            case "BFR":
                return BIOMETRIC_FINGER_RIGHT;
            case "BFL":
                return BIOMETRIC_FINGER_LEFT;
            case "BER":
                return BIOMETRIC_EYE_RIGHT;
            case "BEL":
                return BIOMETRIC_EYE_LEFT;
            default:
                return INVALID_GUID;
        }
     }

    /**
     *
     * This function is used for adding a transaction.
     * @param name
     * @param hcwGUID
     * @param patientGUID
     * @param content
     * @return
     */
    public boolean addTransaction(String name,String hcwGUID, String patientGUID,String content)
    {
        String txGUID = getGUID("Transaction");
        String txtime = getCurrentTime();
        StringBuffer localGUID = new StringBuffer();
        return (addNewDoc(TRANSACTION_DOC,localGUID,name,txGUID,txtime,hcwGUID,patientGUID,content));
    }


    /**
     *
     * This updates the member variables mHcwCt and mPatientCt. After this is called, we can read these variables to get the total user count.
     * Make sure this returns true.
     * @return
     */
    public boolean getNoOfUsers() {
        List<String> userList;
        try {
            userList = this.mStore.getDocumentStore().database().getIds();
            for (int i=0;i<userList.size();i++)
            {
                //check if the entry is a user - HCW or patient
                String guid = userList.get(i);
                if (getGUIDType(guid) == HCW)
                    mHcwCt++;
                else if (getGUIDType(guid) == PATIENT)
                    mPatientCt++;
            }
            return true;
        } catch (DocumentStoreException dEx) {
            return false;
        }
    }

    /**
     *
     * For getting all users.
     * @return
     */
    public boolean getAllUsers() {
        Set<String> users;
        try {
            users = new TreeSet<String>(this.mStore.getDocumentStore().database().getIds());
        } catch (DocumentStoreException dEx) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public int getHCWCt()
    {
        return mHcwCt;
    }

    /**
     *
     * @return
     */
    public int getPatientCt()
    {
        return mPatientCt;
    }

    /*
    This function is can be used to load a specific type of data. Ex: just all transactions
    see below for example.
     */
    private boolean loadSpecificData(int dataType,List<String> dataList) {
        //we'll store all transactions in a list as a string
        List<String> entriesList;
        InternalDocumentRevision lDoc;
        try {
            entriesList = this.mStore.getDocumentStore().database().getIds();
            for (int i=0;i<entriesList.size();i++)
            {
                String guid = entriesList.get(i);
                if (getGUIDType(guid) == dataType)
                {
                    try {
                        lDoc = (InternalDocumentRevision) this.mStore.getDocumentStore().database().read(guid);
                        dataList.add(lDoc.getBody().toString());
                    } catch (DocumentNotFoundException var1) {
                        var1.printStackTrace();
                    }
                }
            }
            return true;
        } catch (DocumentStoreException dEx) {
            return false;
        }
    }

    /**
     * This function can be used for getting all transactions
     * @param dataList
     * @return
     */
    public boolean getAllTransactions(List<String> dataList){
        return loadSpecificData(TRANSACTION,dataList);
    }

    /**
     * This method will load bio-metric data to given map (biometri data map)
     * Returns false if it encounters an issue in reading the datastore itself
     * @param bioMap : biometri data map (Map<String,String>)
     * @return : boolean
     */
    public boolean loadBiometricData(Map<String,String> bioMap) {
        //we'll store all transactions in a list as a string
        List<String> entriesList;
        InternalDocumentRevision lDoc;
        SampleDoc tDoc;
        try {
            entriesList = this.mStore.getDocumentStore().database().getIds();
            for (int i=0;i<entriesList.size();i++)
            {
                String guid = entriesList.get(i);
                if (isBiometric(guid))
                {
                    try {
                        lDoc = (InternalDocumentRevision) this.mStore.getDocumentStore().database().read(guid);
                        String bioStr = lDoc.getBody().toString().replaceAll("","");
                        bioMap.put(guid,bioStr);
                    } catch (DocumentNotFoundException var1) {
                        var1.printStackTrace();
                        Log.e("Issue:","Could not load some biometric info");
                    }
                }
            }
            return true;
        } catch (DocumentStoreException dEx) {
            return false;
        }
    }

    public boolean loadBioMasterDoc(Map<String,String> bioMap) {
        List<String> entriesList;
        InternalDocumentRevision lDoc;
        SampleDoc tDoc;
        try {
            entriesList = this.mStore.getDocumentStore().database().getIds();
            for (int i=0;i<entriesList.size();i++)
            {
                String guid = entriesList.get(i);
                if (isBioMasterDoc(guid))
                {
                    try {
                        lDoc = (InternalDocumentRevision) this.mStore.getDocumentStore().database().read(guid);
                        String bioStr = lDoc.getBody().toString().replaceAll("","");
                        bioMap.put(guid,bioStr);
                    } catch (DocumentNotFoundException var1) {
                        var1.printStackTrace();
                        Log.e("Issue:","Could not load some biometric info");
                    }
                }
            }
            return true;
        } catch (DocumentStoreException dEx) {
            return false;
        }
    }

    private boolean deleteAllBiometric(String bioGUID)
    {
        if (bioGUID != null && bioGUID.length() > 0)
        {
            //this is the master Biometric doc. from thios get the guids of fingerprints available.
            String frGUID = "BFR_"+bioGUID.substring(GUID_INDICATOR_SIZE);
            String flGUID = "BFL_"+bioGUID.substring(GUID_INDICATOR_SIZE);
            String erGUID = "BER_"+bioGUID.substring(GUID_INDICATOR_SIZE);
            String elGUID = "BEL_"+bioGUID.substring(GUID_INDICATOR_SIZE);
            if (frGUID != null && frGUID.length() > 0)
                deleteDoc(BIOMETRIC_FINGER_RIGHT,frGUID);
            if (flGUID != null && flGUID.length() > 0)
                deleteDoc(BIOMETRIC_FINGER_LEFT,flGUID);
            if (erGUID != null && erGUID.length() > 0)
                deleteDoc(BIOMETRIC_EYE_RIGHT,erGUID);
            if (elGUID != null && elGUID.length() > 0)
                deleteDoc(BIOMETRIC_EYE_LEFT,elGUID);
            if (bioGUID.substring(0,GUID_INDICATOR_SIZE).equals("BHW_"))
                deleteDoc(BIOMETRIC_HCW,bioGUID);
            else if (bioGUID.substring(0,GUID_INDICATOR_SIZE).equals("BPT_"))
                deleteDoc(BIOMETRIC_PATIENT,bioGUID);
            else
                return false;
        }
        else
            return false;
        return true;
    }

    /**
     *
     *  This methods will delete individual from database - HCW or patient based on the entries in the delParams map.
     *  It is mandatory to have indType as an entry in this map. This tells whether to delete HCW or Patient.
     * @param delParams : delete parameter(Map<String,String>)
     * @return : boolean
     */
    public boolean deleteIndividualFromDB(Map<String,String> delParams)
    {
        if (delParams.size() == 0)
            return false;
        String userType = delParams.get("indType");
        if (userType == null || userType.length() == 0)
            return false;
        Set<Integer> indTypes = new HashSet<Integer>();
        List<String> entriesList;
        DocumentRevision lDoc;
        //get all users that are of this user type.
        if (userType.equals("HCW"))
            indTypes.add(HCW);
        else if (userType.equals("Patient"))
            indTypes.add(PATIENT);
        else if (userType.equals("Biometric")) {
            indTypes.add(BIOMETRIC_HCW);
            indTypes.add(BIOMETRIC_PATIENT);
        }
        else if (userType.equals("All")) {
            indTypes.add(HCW);
            indTypes.add(PATIENT);
        } else
            return false;
        if (mSearchMethod == CACHE_ONLY)
        {
            entriesList = UserCache.getCache().getAllIds(indTypes);
            for (int i=0;i<entriesList.size();i++)
            {
                String guid = entriesList.get(i);
                String bioGUID = "";
                if (userType.equals("HCW"))
                    bioGUID = getGUID("Biometric-HCW",guid.substring(GUID_INDICATOR_SIZE));
                else
                    bioGUID = getGUID("Biometric-Patient",guid.substring(GUID_INDICATOR_SIZE));
                int guidType = getGUIDType(guid);
                if (guidType == INVALID_GUID)
                    return false;
                if (indTypes.contains(guidType))
                {
                    deleteDoc(guidType,guid);
                    deleteAllBiometric(bioGUID);
                }
            }
            return true;
        } else if (mSearchMethod == DB_ONLY || mSearchMethod == DB_AND_CACHE)
        {
            try {
                entriesList = this.mStore.getDocumentStore().database().getIds();
                for (int i=0;i<entriesList.size();i++)
                {
                    String guid = entriesList.get(i);
                    String bioGUID = "B_"+guid.substring(2);
                    int guidType = getGUIDType(guid);
                    if (guidType == INVALID_GUID)
                        return false;
                    if (indTypes.contains(guidType))
                    {
                        deleteDoc(guidType,guid);
                        deleteAllBiometric(bioGUID);
                    }
                }
                return true;
            } catch (DocumentStoreException dEx) {
                return false;
            }
        }
        return true;
    }

    /**
     * This functions checks if a user is present based on the searchParams map.
     * It is mandatory to have indType in this. This tells whether we are searching for HCW or Patient.
     * Return value: 1 - unique user present. Use the guid returned.
     * -1 - multiple users present. Maybe repeat search with more parameters. use missingparams string to know whats missing
     *  0 - not present.
     *  -2 - internal error.
     * @param user2search
     * @param searchParams
     * @param guiddata
     * @param missingParams
     * @return : int
     */
    public int isUserPresent(String user2search,Map<String,String> searchParams, StringBuffer guiddata, StringBuffer missingParams) {
        //loop thru all docs.
        //check if user name is present
        //handle case where no search parametrs are pro
        if (searchParams.size() == 0)
            return -1;
        long isUserStart = System.currentTimeMillis();
        List<String> userList = new ArrayList<String>();
        StringBuffer txGuid = new StringBuffer();
        String biometricJSON = "";
        biometricJSON = searchParams.get("BiometricData"); //use biometricJSON to search for user.
        if (biometricJSON != null && biometricJSON.length() > 0)
            Log.d("BioJSON",biometricJSON.toString());
        searchParams.remove("BiometricData");
        String bioJSON = "";
        String searchTxGUID = UUID.randomUUID().toString();
        boolean noDataProvided = true; //keeps track of params entered. if this is true in the end, then it means no values were entered for any field. so we should indicate this
        boolean noImpDataProvided = true; //keeps track of imp params - biometrics, uid without which no user matching is possible. if this is true till the end, we need to alert the user.
        //add transaction to indicate that a user search was done

        List<String> outGUIDList = new ArrayList<String>();

        if (!addNewDoc(TRANSACTION_DOC,txGuid,USR_SEARCH_TRANSACTION,searchTxGUID,getCurrentTime(),"","",user2search))
            Log.e("Status","Could not add user search transaction");

        //now use biometricJSON to do matching
        StringBuffer checkRet = new StringBuffer();
        //extract finger print
        if (biometricJSON != null && biometricJSON.length() > 0)
        {
            long bioStartTime = System.currentTimeMillis();
            try {
                JSONObject resJSON = new JSONObject(biometricJSON);
                String rightIndex = resJSON.getJSONObject("fingerPrint").getJSONObject("right").getString("index");
                String leftIndex = resJSON.getJSONObject("fingerPrint").getJSONObject("left").getString("index");
                String rightImg = resJSON.getJSONObject("eye").getJSONObject("image").getString("right");
                String leftImg = resJSON.getJSONObject("eye").getJSONObject("image").getString("left");
                String faceImg = resJSON.getJSONObject("face").getString("image");

                if ((rightImg == null || rightImg.length() == 0) && (leftImg == null || leftImg.length() == 0))
                    SearchParamsState.getInstance(mIDContext).addParamState("bestEye",30);
                else
                {
                    noDataProvided = false;
                    noImpDataProvided = false;
                    //need to check eyes for matching
                    byte[] rightImgByte = SegmentationAndMatching.verifyPersonEyeOrFingerLAndR(rightImg,checkRet,BiometricType.EYE_R);// false,true,false,false);
                    if (checkRet.toString().length() == 0 )
                    {
                        //user biometric info not found. create new user and add to backend
                        Log.d("rightEye-match","No user found");
                        if (rightImgByte != null)
                            resJSON.getJSONObject("eye").getJSONObject("iris").put("right",android.util.Base64.encodeToString(rightImgByte, Base64.NO_WRAP));
                        SearchParamsState.getInstance(mIDContext).addParamState("bestEye",32);
                    }
                    else {
                        Log.d("rightEye-match", "Matching user found");
                        if (guiddata.length() == 0)
                            guiddata.append(checkRet);
                        String guid2add = "";
                        if (searchParams.get("indType").equals("HCW"))
                            guid2add = "HCW_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        else
                            guid2add = "PAT_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        if(guid2add != null && !outGUIDList.contains(guid2add))
                            outGUIDList.add(guid2add);
                        SearchParamsState.getInstance(mIDContext).addParamState("bestEye",31);
                        checkRet.delete(0,checkRet.length());
                    }

                    byte[] leftImgByte = SegmentationAndMatching.verifyPersonEyeOrFingerLAndR(leftImg,checkRet,BiometricType.EYE_L);// true,false,false,false);
                    if (checkRet.toString().length() == 0 )
                    {
                        //user biometric info not found. create new user and add to backend
                        Log.d("leftEye-match","No user found");
                        if (leftImgByte != null)
                            resJSON.getJSONObject("eye").getJSONObject("iris").put("left",android.util.Base64.encodeToString(leftImgByte, Base64.NO_WRAP));
                        SearchParamsState.getInstance(mIDContext).addParamState("bestEye",32);
                    }
                    else {
                        Log.d("leftEye-match", "Matching user found");
                        if (guiddata.length() == 0)
                            guiddata.append(checkRet);
                        String guid2add = "";
                        if (searchParams.get("indType").equals("HCW"))
                            guid2add = "HCW_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        else
                            guid2add = "PAT_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        if(guid2add != null && !outGUIDList.contains(guid2add))
                            outGUIDList.add(guid2add);

                        SearchParamsState.getInstance(mIDContext).addParamState("bestEye",31);
                        checkRet.delete(0,checkRet.length());
                    }
                }
                if (leftIndex == null || leftIndex.length() == 0)
                    SearchParamsState.getInstance(mIDContext).addParamState("leftFP",10);
                else
                {
                    noDataProvided = false;
                    noImpDataProvided = false;
                    byte[] fplIndex = SegmentationAndMatching.verifyPersonEyeOrFingerLAndR(leftIndex,checkRet, BiometricType.FINGER_L);// false,false,true,false);
                    if (checkRet.toString().length() == 0 )
                    {
                        //user biometric info not found. create new user and add to backend
                        Log.d("fingerPrint-match","No user found");
                        if (fplIndex != null)
                            resJSON.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("left").put("index",android.util.Base64.encodeToString(fplIndex, Base64.NO_WRAP));
                        SearchParamsState.getInstance(mIDContext).addParamState("leftFP",12);
                    }
                    else {
                        Log.d("fingerPrint-match", "Matching user found");
                        if (guiddata.length() == 0)
                            guiddata.append(checkRet);
                        String guid2add = "";
                        if (searchParams.get("indType").equals("HCW"))
                            guid2add = "HCW_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        else
                            guid2add = "PAT_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        if(guid2add != null && !outGUIDList.contains(guid2add))
                            outGUIDList.add(guid2add);

                        SearchParamsState.getInstance(mIDContext).addParamState("leftFP",11);
                        checkRet.delete(0,checkRet.length());
                    }
                }

                if (rightIndex == null || rightIndex.length() == 0)
                    SearchParamsState.getInstance(mIDContext).addParamState("rightFP",10);
                else {
                    noDataProvided = false;
                    noImpDataProvided = false;
                    byte[] fprIndex = SegmentationAndMatching.verifyPersonEyeOrFingerLAndR(rightIndex, checkRet, BiometricType.FINGER_R);//false, false, false, true
                    if (checkRet.toString().length() == 0) {
                        //user biometric info not found. create new user and add to backend
                        Log.d("fingerPrint-match", "No user found");
                        if (fprIndex != null)
                            resJSON.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("right").put("index", android.util.Base64.encodeToString(fprIndex, Base64.NO_WRAP));
                        SearchParamsState.getInstance(mIDContext).addParamState("rightFP", 12);
                    } else {
                        Log.d("fingerPrint-match", "Matching user found");
                        if (guiddata.length() == 0)
                            guiddata.append(checkRet);
                        String guid2add = "";
                        if (searchParams.get("indType").equals("HCW"))
                            guid2add = "HCW_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        else
                            guid2add = "PAT_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        if(guid2add != null && !outGUIDList.contains(guid2add))
                            outGUIDList.add(guid2add);

                        SearchParamsState.getInstance(mIDContext).addParamState("rightFP", 11);
                        checkRet.delete(0,checkRet.length());
                    }
                }

                if (faceImg == null || faceImg.length() == 0)
                    SearchParamsState.getInstance(mIDContext).addParamState("face",10);
                else {
                    noDataProvided = false;
                    noImpDataProvided = false;
                    byte[] fprIndex = SegmentationAndMatching.verifyPersonEyeOrFingerLAndR(faceImg, checkRet, BiometricType.FACE);//false, false, false, true
                    if (checkRet.toString().length() == 0) {
                        //user biometric info not found. create new user and add to backend
                        Log.d("faceImg-match", "No user found");
                        if (fprIndex != null)
                            resJSON.getJSONObject("face").put("template", android.util.Base64.encodeToString(fprIndex, Base64.NO_WRAP));
                        SearchParamsState.getInstance(mIDContext).addParamState("face", 12);
                    } else {
                        Log.d("faceImg-match", "Matching user found");
                        if (guiddata.length() == 0)
                            guiddata.append(checkRet);
                        String guid2add = "";
                        if (searchParams.get("indType").equals("HCW"))
                            guid2add = "HCW_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        else
                            guid2add = "PAT_"+checkRet.toString().substring(GUID_INDICATOR_SIZE);
                        if(guid2add != null && !outGUIDList.contains(guid2add))
                            outGUIDList.add(guid2add);

                        SearchParamsState.getInstance(mIDContext).addParamState("face", 11);
                        checkRet.delete(0,checkRet.length());
                    }
                }

                //pack the templates in the bio json and add it to the map
                //   searchParams.put("BiometricData",resJSON.toString());
                bioJSON = resJSON.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e1) {
                e1.printStackTrace();
            }
            long bioEndTime = System.currentTimeMillis();
            System.out.println("Biometric search time: "+(bioEndTime-bioStartTime));
        }
        else
        {
            //no biometric data was provided. so set the state values appropriately.
            SearchParamsState.getInstance(mIDContext).addParamState("leftFP",10);
            SearchParamsState.getInstance(mIDContext).addParamState("rightFP",10);
            SearchParamsState.getInstance(mIDContext).addParamState("bestEye",30);
            SearchParamsState.getInstance(mIDContext).addParamState("face",10);
        }

        //check search parameters and if something is not found, set the state value accordingly
        if (!searchParams.containsKey("name"))
            SearchParamsState.getInstance(mIDContext).addParamState("name",0);
        else
            noDataProvided = false;
        if (!searchParams.containsKey("age"))
            SearchParamsState.getInstance(mIDContext).addParamState("age",0);
        else
            noDataProvided = false;
        if (!searchParams.containsKey("uid"))
            SearchParamsState.getInstance(mIDContext).addParamState("uid",0);
        else {
            noDataProvided = false;
            noImpDataProvided = false;
        }

        if (noDataProvided)
            return -1;

      /*  List<String> outGUIDList = new ArrayList<String>();
        if (guiddata.length() > 0)
        {
            if (searchParams.get("indType").equals("HCW"))
                outGUIDList.add("HCW_"+guiddata.toString().substring(GUID_INDICATOR_SIZE));
            else
                outGUIDList.add("PAT_"+guiddata.toString().substring(GUID_INDICATOR_SIZE));
        } */

        //check if the search parameters are present and make sure you get their state values populated.
        if (mSearchMethod == CACHE_ONLY || mSearchMethod == DB_AND_CACHE)
        {
            if (this.mStore.getParamsStateFromCache(searchParams,outGUIDList))
            {
                SearchParamsState.getInstance(mIDContext).setTotalStateValue();
            }
            else
                return -2;
        } else if (mSearchMethod == DB_ONLY || mSearchMethod == DB_AND_CACHE)
        {
            if (this.mStore.getParamsState(searchParams))
            {
                SearchParamsState.getInstance(mIDContext).setTotalStateValue();
            }
            else
                return -2;
        }


        searchParams.put("BiometricData",bioJSON);
        int finalOutcome = SearchParamsState.getInstance(mIDContext).getFinalOutcome();
        Log.d("Final Outcome ", String.valueOf(finalOutcome));
        //return of -2 means an internal error
        if (finalOutcome == -2)
            return -2;
        else if (finalOutcome == 20)
        {
            if (mSearchMethod == DB_ONLY)
            {
                //a user was found. get the guid of this user
                //we have to make sure the biometric data is not included in the search params. so remove
                searchParams.remove("BiometricData");
                String gid = "";
                if (guiddata.length() > 0)
                {
                    //this is true if a GUID was found. That means a biometric match was found.
                    if (searchParams.get("indType").equals("HCW"))
                        gid = "HCW_"+guiddata.toString().substring(GUID_INDICATOR_SIZE);
                    else
                        gid = "PAT_"+guiddata.toString().substring(GUID_INDICATOR_SIZE);
                    Log.d("GUID of user found",gid);
                    String rev = "";
                    String name = "";
                    try {
                        rev = this.mStore.getDocumentStore().database().read(gid).getRevision();
                        name = (String) this.mStore.getDocumentStore().database().read(gid).getBody().asMap().get("name");
                        if (getGUIDType(gid) == HCW) {
                            SessionProperties.getInstance(mIDContext).setHcwRevision(rev);
                       //     if (!addNewDoc(TRANSACTION_DOC, txGuid,USR_LOGIN_TRANSACTION, getGUID("Transaction"), getCurrentTime(), gid, "", user2search))
                         //       Log.d("Status", "Could not add HCW Login transaction");
                        }
                        else if (getGUIDType(gid) == PATIENT) {
                            SessionProperties.getInstance(mIDContext).setPatientRevision(rev);
                           // if (!addNewDoc(TRANSACTION_DOC, txGuid,USR_LOGIN_TRANSACTION, getGUID("Transaction"), getCurrentTime(), "", gid, user2search))
                             //   Log.d("Status","Could not add Patient login treansaction");
                        }
                        else
                            return 0;
                        return 1;
                    } catch (DocumentNotFoundException e) {
                        Log.e("DB error",e.toString());
                        return 0; //RETHINK IF THIS SHOULD RETURN 0 OR -2. I FEEL WE SHOULD EVENTUALLY MAKE THIS RETURN -2 AS A DOC NOT FOUND EXC HERE MEANS SOMETHING WRONG INTERNALLY.
                    } catch (DocumentStoreException e) {
                        Log.e("DB error",e.toString());
                        return -2;
                    }

                }
                for (Map.Entry<String,String> entrySet : searchParams.entrySet())
                    System.out.println("Key: "+entrySet.getKey()+" Value: "+entrySet.getValue());
                if ((!this.mStore.runQuery(searchParams,userList)) || (userList.size() == 0))
                {
                    return 0;
                }
                else if (userList.size() > 1)
                {
                    //this means that multiple entries were found for the search parameters used.
                    String missedParams = "";
                    for (String s:mSpecifiedSearchParams)
                    {
                        if (!searchParams.containsKey(s)) {
                            if (missedParams.length() == 0)
                                missedParams = missedParams + s;
                            else
                                missedParams = missedParams + "," + s;
                        }
                    }
                    missingParams.append(missedParams);
                    searchParams.put("BiometricData",bioJSON);
                    return -4;
                }
                else
                {
                    String guid = userList.get(0);
                    guiddata.append(guid);
                    String rev = "";
                    String name = "";
                    String photo = "";
                    String uid = "";
                    try {
                        rev = this.mStore.getDocumentStore().database().read(guid).getRevision();
                        name = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("name");
                        photo = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("photo");
                    } catch (DocumentNotFoundException e) {
                        e.printStackTrace();
                        return 0;
                    } catch (DocumentStoreException e) {
                        e.printStackTrace();
                        return 0;
                    }
                    if (getGUIDType(guid) == HCW) {
                        SessionProperties.getInstance(mIDContext).setHcwRevision(rev);
                        if (photo != null && photo.length() > 0) {
                            SessionProperties.getInstance(mIDContext).setHCWPhotoStr(photo);
                            Log.d("HCW-photo from DB",photo.toString());
                        }
                     //   if (!addNewDoc(TRANSACTION_DOC, txGuid,USR_LOGIN_TRANSACTION, getGUID("Transaction"), getCurrentTime(), guid, "", user2search))
                       //     Log.d("Status", "Could not add HCW Login transaction");
                    }
                    else if (getGUIDType(guid) == PATIENT) {
                        SessionProperties.getInstance(mIDContext).setPatientRevision(rev);
                        if (photo != null && photo.length() > 0) {
                            SessionProperties.getInstance(mIDContext).setPatientPhotoStr(photo);
                            Log.d("Patient-photo from DB",photo.toString());
                        }
                   //     if (!addNewDoc(TRANSACTION_DOC, txGuid,USR_LOGIN_TRANSACTION, getGUID("Transaction"), getCurrentTime(), "", guid, user2search))
                     //       Log.d("Status","Could not add Patient login treansaction");
                    }
                    else
                        return 0;
                }
            } else if (mSearchMethod == CACHE_ONLY)
            {
                long userFromCacheStart = System.currentTimeMillis();
                //search outguid list for guids that matched
                if (outGUIDList.size() == 0)
                    return 0;
                else if (outGUIDList.size() > 1)
                {
                    return -4;
                }
                else if (outGUIDList.size() == 1)
                {
                    String guid = "";
                    String indType = searchParams.get("indType");
                    if (indType.equals("HCW")) {
                        Log.d("1st elem of GUID: ",outGUIDList.get(0).toString());
                        guid = "HCW_" + outGUIDList.get(0).substring(GUID_INDICATOR_SIZE);
                    }
                    else
                        guid = "PAT_"+outGUIDList.get(0).substring(GUID_INDICATOR_SIZE);
                    if (guiddata.length() == 0)
                        guiddata.append(guid);
                    else
                    {
                        guiddata.delete(0,guiddata.length());
                        guiddata.append(guid);
                    }
                    String rev = "";
                    String name = "";
                    String photo = "";
                    String uid = "";
                    int age = 0;
                    try {
                        rev = this.mStore.getDocumentStore().database().read(guid).getRevision();
                        name = UserCache.getCache().searchForUserParam(indType,guid,"name");
                        photo = UserCache.getCache().searchForUserParam(indType,guid,"photo");
                        uid = UserCache.getCache().searchForUserParam(indType,guid,"uid");
                        String ageStr = UserCache.getCache().searchForUserParam(indType,guid,"age");
                        System.out.println("Age from cache: "+ageStr);
                        if (ageStr != null && ageStr.length() > 0)
                            age = Integer.parseInt(ageStr);
                        if (photo == null || photo.length() == 0) {
                            Log.d("Photo-warning","Photo not in cache. Searching db");
                            photo = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("photo");
                        }
                        if (name == null || name.length() == 0) {
                            Log.d("Name-warning","Name not in cache. Searching db");
                            name = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("name");
                        }
                        if (uid ==  null || uid.length() == 0) {
                            Log.d("uid-warning","UID not in cache. Searching db");
                            uid = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("uid");
                        }
                        if (age == 0) {
                            Log.d("age-warning","age not in cache. Searching db");
                            ageStr = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("age");
                            if (ageStr != null && ageStr.length() > 0)
                                age = Integer.parseInt(ageStr);
                        }
                    } catch (DocumentNotFoundException e) {
                        e.printStackTrace();
                     } catch (DocumentStoreException e) {
                        e.printStackTrace();
                     }
                    if (getGUIDType(guid) == HCW) {
                        SessionProperties.getInstance(mIDContext).setHcwRevision(rev);
                        if (photo != null && photo.length() > 0) {
                            SessionProperties.getInstance(mIDContext).setHCWPhotoStr(photo);
                            Log.d("HCW-photo from DB",photo.toString());
                        }
                        if (name != null && name.length() > 0)
                        {
                            SessionProperties.getInstance(mIDContext).setHcwName(name);
                            Log.d("HCW-name from DB",name.toString());
                        }
                        if (uid != null && uid.length() > 0)
                        {
                            SessionProperties.getInstance(mIDContext).setID(true,uid);
                            Log.d("HCW-uid from DB",uid.toString());
                        }
                        if (age > 0)
                        {
                            SessionProperties.getInstance(mIDContext).setHcwAge(age);
                            Log.d("HCW-age from DB",Integer.toString(age));
                        }
                    //    if (!addNewDoc(TRANSACTION_DOC, txGuid,USR_LOGIN_TRANSACTION, getGUID("Transaction"), getCurrentTime(), guid, "", user2search))
                      //      Log.d("Status", "Could not add HCW Login transaction");
                    }
                    else if (getGUIDType(guid) == PATIENT) {
                        SessionProperties.getInstance(mIDContext).setPatientRevision(rev);
                        if (photo != null && photo.length() > 0) {
                            SessionProperties.getInstance(mIDContext).setPatientPhotoStr(photo);
                            Log.d("Patient-photo from DB",photo.toString());
                        }
                        if (name != null && name.length() > 0)
                        {
                            SessionProperties.getInstance(mIDContext).setPatientName(name);
                            Log.d("Patient-name from DB",name.toString());
                        }
                        if (uid != null && uid.length() > 0)
                        {
                            SessionProperties.getInstance(mIDContext).setID(false,uid);
                            Log.d("Patient-uid from DB",uid.toString());
                        }
                        if (age > 0)
                        {
                            SessionProperties.getInstance(mIDContext).setPatientAge(age);
                            Log.d("Patient-age from DB",Integer.toString(age));
                        }
                     //   if (!addNewDoc(TRANSACTION_DOC, txGuid,USR_LOGIN_TRANSACTION, getGUID("Transaction"), getCurrentTime(), "", guid, user2search))
                       //     Log.d("Status","Couldn't add Patient login treansaction");
                    }
                    else
                        return 0;
                }
                long userFromCacheEnd = System.currentTimeMillis();
                System.out.println("Time to search user data from cache"+(userFromCacheEnd-userFromCacheStart));
            }

        }
        else if (finalOutcome == -1) {
            if (noImpDataProvided)
                return -3;//this means no user found. but some imp data - biometrics/uid wasnt provided.
            else
                return 0;
        }

        //check db by running runQuery()
        long isUserEnd = System.currentTimeMillis();
        System.out.println("Time in isUser function: "+(isUserEnd-isUserStart));
        return 1;
    }

    private boolean handleBiometricData(String origGuid,String bioJSON)
    {
        long bioAddStart = System.currentTimeMillis();
        StringBuffer guiddata = new StringBuffer();
        if (bioJSON != null && bioJSON.length() > 0) {
            Log.d("Bio-JSON", bioJSON.toString());
            Log.d("Bio data length",Integer.toString(bioJSON.length()));
            //enroll user to local list.
            //get byte array from biojson string
            try {
                JSONObject tmpBio = new JSONObject(bioJSON);
                String leftEye = tmpBio.getJSONObject("eye").getJSONObject("image").getString("left");
                if (leftEye != null && leftEye.length() > 0)
                {
                    if (!addNewDoc(BIOMETRIC_DOC,guiddata,"Biometric-LeftEye",origGuid,leftEye))
                        return false;
                    else
                    {
                        tmpBio.getJSONObject("eye").getJSONObject("image").put("left",guiddata.toString());
                        guiddata.delete(0,guiddata.length());
                    }
                }

                String rightEye = tmpBio.getJSONObject("eye").getJSONObject("image").getString("right");
                if (rightEye != null && rightEye.length() > 0)
                {
                    if (!addNewDoc(BIOMETRIC_DOC,guiddata,"Biometric-RightEye",origGuid,rightEye))
                        return false;
                    else
                    {
                        tmpBio.getJSONObject("eye").getJSONObject("image").put("right",guiddata.toString());
                        guiddata.delete(0,guiddata.length());
                    }
                }

                String leftFinger = tmpBio.getJSONObject("fingerPrint").getJSONObject("left").getString("index");
                if (leftFinger != null && leftFinger.length() > 0)
                {
                    if (!addNewDoc(BIOMETRIC_DOC,guiddata,"Biometric-LeftFinger",origGuid,leftFinger))
                        return false;
                    else
                    {
                        tmpBio.getJSONObject("fingerPrint").getJSONObject("left").put("index",guiddata.toString());
                        guiddata.delete(0,guiddata.length());
                    }
                }

                String rightFinger = tmpBio.getJSONObject("fingerPrint").getJSONObject("right").getString("index");
                if (rightFinger != null && rightFinger.length() > 0)
                {
                    if (!addNewDoc(BIOMETRIC_DOC,guiddata,"Biometric-RightFinger",origGuid,rightFinger))
                        return false;
                    else
                    {
                        tmpBio.getJSONObject("fingerPrint").getJSONObject("right").put("index",guiddata.toString());
                        guiddata.delete(0,guiddata.length());
                    }
                }
                String face = tmpBio.getJSONObject("face").getString("image");
                if (face != null && face.length() > 0)
                {
                    if (!addNewDoc(BIOMETRIC_DOC,guiddata,"Biometric-face",origGuid,face)) //?
                        return false;
                    else
                    {
                        tmpBio.getJSONObject("face").put("image",guiddata.toString());
                        guiddata.delete(0,guiddata.length());
                    }
                }

                //now add the template to the local cache.
                byte [] leftImgByte = android.util.Base64.decode(tmpBio.getJSONObject("eye").getJSONObject("iris").getString("left"),Base64.NO_WRAP);
                byte [] rightImgByte = android.util.Base64.decode(tmpBio.getJSONObject("eye").getJSONObject("iris").getString("right"),Base64.NO_WRAP);
                byte [] fplIndex = android.util.Base64.decode(tmpBio.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("left").getString("index"),Base64.NO_WRAP);
                byte [] fprIndex = android.util.Base64.decode(tmpBio.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("right").getString("index"),Base64.NO_WRAP);
                byte [] faceByte = android.util.Base64.decode(tmpBio.getJSONObject("face").getString("template"),Base64.NO_WRAP);
                String bioName = "";
                if (isHCW(origGuid))
                    bioName = "Biometric-HCW";
                else
                    bioName = "Biometric-Patient";
                SegmentationAndMatching.enrollPerson(leftImgByte,rightImgByte,fplIndex,fprIndex,faceByte,getGUID(bioName,origGuid.substring(GUID_INDICATOR_SIZE)));
                if (!addNewDoc(BIOMETRIC_DOC,guiddata,bioName,origGuid,tmpBio.toString()))
                    return false;
            } catch (JSONException e1) {
                e1.printStackTrace();
                Log.e("Bio-Issue","Could not add biometric data");
                return false;
            } catch (NullPointerException e2) {
                e2.printStackTrace();
                Log.e("Bio-Issue","Could not add biometric data");
                return false;
            }
        }
        long bioAddEnd = System.currentTimeMillis();
        System.out.println("Time to add bio data: "+(bioAddEnd-bioAddStart));
        return true;
    }

    /**
     * This is used to add a doc that has general data needed for the app. Ex: phone ID as of now
     * @param phoneID
     * @return
     */
    public boolean addGeneralRecord(String phoneID)
    {
        StringBuffer guiddata = new StringBuffer();
        return addNewDoc(GENERAL_DOC,guiddata,"General",phoneID);
    }

    /**
     *
     * @return
     */
    public String getPhoneID()
    {
        String pID = "";
        try {
            pID = (String) this.mLocalStore.getDocumentStore().database().read("GEN_0001").getBody().asMap().get("PhoneID");
        } catch (DocumentNotFoundException e) {
            Log.e("Error getting PhoneID: ",e.toString());
        } catch (DocumentStoreException e) {
            Log.e("Error getting PhoneID: ",e.toString());
        }
        return pID;
    }

    /**
     *
     * add individual
     * @param guiddata
     * @param name
     * @param uid
     * @param govID
     * @param age
     * @param phone
     * @param countryCode
     * @param dob
     * @param indType
     * @param gender
     * @param skill
     * @param biometricJSON
     * @return : true : if added else return false.
     */
    public boolean addIndividual(StringBuffer guiddata,String name,String uid,String govID,int age,String phone,String countryCode,String dob,String indType,String gender,String skill,String biometricJSON) {
        long addFnStart = System.currentTimeMillis();
        SampleDoc doc;
        //first compute the DOB from the age (in days).
        StringBuffer dobStr = new StringBuffer();
        long dobFromAge = IdentityUtils.getDOBFromAge(age,dobStr);
        String txGUID = UUID.randomUUID().toString();
        //long dobFromAge = IdentityUtils.getDOBFromAge(age);
        if (!addNewDoc(INDIVIDUAL_DOC,guiddata,name,phone,countryCode,Long.toString(dobFromAge),indType,gender,skill,uid,govID,Integer.toString(age),txGUID)) {
            Log.e("Add-Error","Could not add individual doc");
            return false;
        }
        else
        {
            //cache the data you just added.
            long userCacheAddStart = System.currentTimeMillis();
            Log.d("GUID added: ",guiddata.toString());
            Log.d("Name added: ",name.toString());
            System.out.println("Age added to DB: "+Integer.toString(age));
            System.out.println("DOB from age: "+Long.toString(dobFromAge));
            UserCache.getCache().addDataToCache(indType,"name",name,guiddata.toString());
            UserCache.getCache().addDataToCache(indType,"age",Integer.toString(age),guiddata.toString());
            UserCache.getCache().addDataToCache(indType,"epochDOB",Long.toString(dobFromAge),guiddata.toString());
            UserCache.getCache().addDataToCache(indType,"uid",uid,guiddata.toString());
            long userCacheAddEnd = System.currentTimeMillis();
            System.out.println("Time to add user to cache: "+(userCacheAddEnd-userCacheAddStart));
            //now add biometric data as well
            if (!handleBiometricData(guiddata.toString(),biometricJSON)) {
                Log.e("Add-error","Failed to add biometric data");
                return false;
            }
            //we could add the individual doc to DB. therefore add as a transaction
            if (indType.equals("HCW")) {
                SessionProperties.getInstance(mIDContext).setHcwGUID(guiddata.toString());
                SessionProperties.getInstance(mIDContext).setHcwName(name);
                SessionProperties.getInstance(mIDContext).setHCWPhone(phone);
                SessionProperties.getInstance(mIDContext).setHCWCountryCode(countryCode);
                SessionProperties.getInstance(mIDContext).setHcwAge(age);
                SessionProperties.getInstance(mIDContext).setEpochDOB(true,dobFromAge);
                SessionProperties.getInstance(mIDContext).setID(true,uid);
            }
            else {
                SessionProperties.getInstance(mIDContext).setPatientGUID(guiddata.toString());
                SessionProperties.getInstance(mIDContext).setPatientName(name);
                SessionProperties.getInstance(mIDContext).setPatientPhone(phone);
                SessionProperties.getInstance(mIDContext).setPatientCountryCode(countryCode);
                SessionProperties.getInstance(mIDContext).setPatientAge(age);
                SessionProperties.getInstance(mIDContext).setEpochDOB(false,dobFromAge);
                SessionProperties.getInstance(mIDContext).setID(false,uid);
            }
            StringBuffer guiddataTx = new StringBuffer();
            String txname = indType + " Addition";
            String txtime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            String hcwGUID = "";
            String content = "";
            String patientGUID = "";
            if (indType.equals("HCW")) {
                hcwGUID = guiddata.toString();
                content = name;
            }
            else {
                patientGUID = guiddata.toString();
                content = name;
            }
            if (!addNewDoc(TRANSACTION_DOC,guiddataTx,txname,txGUID,txtime,hcwGUID,patientGUID,content))
                Log.e("Error","Failure while adding new user addition transaction");
            long addFnEnd = System.currentTimeMillis();
            System.out.println("Time in add function: "+(addFnEnd-addFnStart));
            return true;
        }
    }


    /**
     *
     * we expect inputString in the following format: whaat is {noun}{plural} name?
     * ex: if above is for  patientType newborn, and audienceType external - it should return: what is the baby's name?
     * @param inputString
     * @param patientType
     * @param audType
     * @param outStrRsp
     * @return
     */
    public boolean interpretGrammar(String inputString, String patientType,String audType, StringBuffer outStrRsp)
    {
        //first read inputString. look for "{". if seen, what follows is a grammarVar till we encounter another "}".
        String outStr = "";
        boolean isGrammarVar = false;
        String grammarVar = "";
        for (int i = 0; i < inputString.length(); i++) {
            char c = inputString.charAt(i);
            if (c == OPENING_BRACE)
            {
                //what starts after this is a grammarVar
                isGrammarVar = true;
            }
            else if (c == CLOSING_BRACE)
            {
                //if we see this, most likely it means we were reading a grammar variable before this.
                //now stop interpreting it as a grammar var. the closinbg brace signifies that.
                if (isGrammarVar) {
                    isGrammarVar = false;
                    StringBuffer grammarRsp = new StringBuffer();
                    //also use whatever is in the grammarVar string and get its interpretation
                    if (!processJSONFile(mIDContext,patientType,audType,grammarVar,grammarRsp))
                        return false;
                    else {
                        outStr = outStr + grammarRsp.toString();
                        //also make sure that grammarVar is reset back to "".
                        grammarVar = "";
                    }
                }
            }
            else
            {
                //if isGrammarVar is false, then copy char as is to the outStr.
                //if set to true, then what is coming up is a grammarVar.
                if (!isGrammarVar)
                    outStr = outStr + c;
                else
                {
                    //add this to grammarVar
                    grammarVar = grammarVar + c;
                }
            }
        }
        outStrRsp.append(outStr);
        return true;
    }

    private boolean processJSONFile(Context ctx, String patientType, String audType, String grammarVar, StringBuffer grRsp) {
        try {
            String jsonTxt = IdentityUtils.loadJSONFromAsset(ctx, R.string.grammar_json);;
            JSONObject grammarJS = new JSONObject(jsonTxt);
            String grammar = grammarJS.getString("patientType");
            JSONObject patientTypeGrammar = new JSONObject(grammar);
            Iterator<String> keys = patientTypeGrammar.keys();

            while(keys.hasNext()) {
                String key = keys.next();
                if (key.equals(patientType))
                {
                    if (patientTypeGrammar.get(key) instanceof JSONObject) {
                        // do something with jsonObject here

                        Log.d("Key",key);
                        JSONObject pTypeGrammar = (JSONObject) patientTypeGrammar.get(key);
                        JSONObject extGrammar = (JSONObject) pTypeGrammar.get("external");
                        JSONObject selfGrammar = (JSONObject) pTypeGrammar.get("self");
                        if (audType == "self")
                            grRsp.append(selfGrammar.getString(grammarVar));
                        else if (audType == "external") {
                            if (extGrammar.getString(grammarVar).equals(SUBJECT_NAME))
                                grRsp.append(SessionProperties.getInstance(mIDContext).getPatientName());
                            else
                                grRsp.append(extGrammar.getString(grammarVar));
                        }
                        else
                            return false;
                        return true;
                    }
                }
            }
            return false;
        } catch (JSONException var3) {
            return false;
        }
    }

    /**
     * This method will update the individual based on the entries in the updateParams map.
     * It is mandatory to have indType as an entry to tell if we want to update HCW or Patient
     * @param updateParams
     * @return : boolean
     */
    public boolean updateIndividual(Map<String,String> updateParams,String content)
    {
        String indType = updateParams.get("indType");
        if (indType == null)
            return false;
        String name = updateParams.get("name");
        String phone = updateParams.get("phone");
        String countryCode = updateParams.get("countryCode");
        String dob = updateParams.get("DOB");
        String gender = updateParams.get("gender");
        String skill = updateParams.get("skill");
        String consent = updateParams.get("consent");
        String location = updateParams.get("location");
        String POS = updateParams.get("POS");
        String photo  = updateParams.get("photo");
        String guid = updateParams.get("guid");
        String uid = updateParams.get("uid");
        String ageStr = updateParams.get("age");
        int age = 0;
        if (ageStr != null && ageStr.length() > 0)
            age = Integer.parseInt(ageStr);
        String txID = UUID.randomUUID().toString();
        SampleDoc updateDoc = new IndividualDoc(name,phone,countryCode,dob,indType,gender,skill,"");
        ((IndividualDoc) updateDoc).setUID(uid);
        updateDoc.setAge(age);
        if (indType.equals("HCW")) {
            if (SessionProperties.getInstance(mIDContext).getHcwName() == null && SessionProperties.getInstance(mIDContext).getHcwName().length() == 0)
                SessionProperties.getInstance(mIDContext).setHcwName(name);
            if (SessionProperties.getInstance(mIDContext).getHCWPhotoStr() == null || SessionProperties.getInstance(mIDContext).getHCWPhotoStr().length() == 0)
                SessionProperties.getInstance(mIDContext).setHCWPhotoStr(photo);
            if (SessionProperties.getInstance(mIDContext).getHcwGUID() == null || SessionProperties.getInstance(mIDContext).getHcwGUID().length() == 0)
                SessionProperties.getInstance(mIDContext).setHcwGUID(guid);
            else
                guid = SessionProperties.getInstance(mIDContext).getHcwGUID();
            Log.d("HName-sessionProp:",SessionProperties.getInstance(mIDContext).getHcwName());
            Log.d("HGUID-sessionProp:",SessionProperties.getInstance(mIDContext).getHcwGUID());
            if (name == null)
                name = SessionProperties.getInstance(mIDContext).getHcwName();
            if (phone == null)
                phone = SessionProperties.getInstance(mIDContext).getHCWPhone();
            if (countryCode == null)
                countryCode = SessionProperties.getInstance(mIDContext).getHCWCountryCode();
            if (dob == null)
                dob = SessionProperties.getInstance(mIDContext).getHCWDOB();
            if (gender == null)
                gender = SessionProperties.getInstance(mIDContext).getHCWGender();
            if (skill == null)
                skill = SessionProperties.getInstance(mIDContext).getHCWSkill();
            if (consent == null)
                consent = SessionProperties.getInstance(mIDContext).getHCWConsent();
            if (photo == null)
                photo = SessionProperties.getInstance(mIDContext).getHCWPhotoStr();
            UserCache.getCache().addDataToCache(indType,"photo",photo,SessionProperties.getInstance(mIDContext).getHcwGUID());
            if (uid == null)
                uid = SessionProperties.getInstance(mIDContext).getID(true);
            if (age <= 0 && SessionProperties.getInstance(mIDContext).getHcwAge() != null && SessionProperties.getInstance(mIDContext).getHcwAge().length() > 0)
                age = Integer.parseInt(SessionProperties.getInstance(mIDContext).getHcwAge());
            StringBuffer dobStr = new StringBuffer();
            long dobFromAge = IdentityUtils.getDOBFromAge(age,dobStr);
            SessionProperties.getInstance(mIDContext).setEpochDOB(true,dobFromAge);
            } else if (indType.equals("Patient")) {
            if (SessionProperties.getInstance(mIDContext).getPatientName() == null || SessionProperties.getInstance(mIDContext).getPatientName().length() == 0)
                SessionProperties.getInstance(mIDContext).setPatientName(name);
            if (SessionProperties.getInstance(mIDContext).getPatientPhotoStr() == null || SessionProperties.getInstance(mIDContext).getPatientPhotoStr().length() == 0)
                SessionProperties.getInstance(mIDContext).setPatientPhotoStr(photo);
            if (SessionProperties.getInstance(mIDContext).getPatientGUID() == null || SessionProperties.getInstance(mIDContext).getPatientGUID().length() == 0)
                SessionProperties.getInstance(mIDContext).setPatientGUID(guid);
            else
                guid = SessionProperties.getInstance(mIDContext).getPatientGUID();
            Log.d("PName-sessionProp:",SessionProperties.getInstance(mIDContext).getPatientName());
            Log.d("PGUID-sessionProp:",SessionProperties.getInstance(mIDContext).getPatientGUID());
            if (name == null)
                name = SessionProperties.getInstance(mIDContext).getPatientName();
            if (phone == null)
                phone = SessionProperties.getInstance(mIDContext).getPatientPhone();
            if (countryCode == null)
                countryCode = SessionProperties.getInstance(mIDContext).getPatientCountryCode();
            if (dob == null)
                dob = SessionProperties.getInstance(mIDContext).getPatientDOB();
            if (gender == null)
                gender = SessionProperties.getInstance(mIDContext).getPatientGender();
            if (skill == null)
                skill = SessionProperties.getInstance(mIDContext).getPatientSkill();
            if (consent == null)
                consent = SessionProperties.getInstance(mIDContext).getPatientConsent();
            if (photo == null)
                photo = SessionProperties.getInstance(mIDContext).getPatientPhotoStr();
            UserCache.getCache().addDataToCache(indType,"photo",photo,SessionProperties.getInstance(mIDContext).getPatientGUID());
            if (uid == null)
                uid = SessionProperties.getInstance(mIDContext).getID(false);
            if (age <= 0 && (SessionProperties.getInstance(mIDContext).getPatientAge() != null && SessionProperties.getInstance(mIDContext).getPatientAge().length() > 0))
                age = Integer.parseInt(SessionProperties.getInstance(mIDContext).getPatientAge());
            StringBuffer dobStr = new StringBuffer();
            long dobFromAge = IdentityUtils.getDOBFromAge(age,dobStr);
            SessionProperties.getInstance(mIDContext).setEpochDOB(false,dobFromAge);
        } else
            return false;
        if (location == null)
            location = SessionProperties.getInstance(mIDContext).getLocationStr();
        if (POS == null)
            POS = SessionProperties.getInstance(mIDContext).getPOS();
        if (updateDoc(updateDoc,INDIVIDUAL_DOC,guid,name,phone,countryCode,dob,indType,gender,skill,consent,location,POS,photo,uid,Integer.toString(age),txID))
        {
            //add transaction
            StringBuffer guiddataTx = new StringBuffer();
            String txname = indType + " "+ content;
            String txtime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            String hcwGUID = "";
            //String content = "";
            String patientGUID = "";
            if (indType.equals("HCW"))
                hcwGUID = guid.toString();
            else
                patientGUID = guid.toString();
            if (!addNewDoc(TRANSACTION_DOC,guiddataTx,txname,txID,txtime,hcwGUID,patientGUID,name))
                Log.e("Error","Failure while adding update user transaction");
            return true;
        }
        else
        {
            return false;
        }
        //return updateDoc(updateDoc,INDIVIDUAL_DOC,guid,name,phone,countryCode,dob,indType,gender,skill,consent,location,POS,photo,uid,Integer.toString(age),txID);
    }

    private boolean updateDoc(SampleDoc outDoc,int docType,String guid,String name,String...vars) {
        if (docType ==TRANSACTION_DOC || docType == GENERAL_DOC)
            return false;
        SampleDoc doc;
        String phoneNum = vars[0];
        String countryCode = vars[1];
        String dob = vars[2];
        String indType = vars[3];
        String revision = "";
        String gender = vars[4];
        String skill = vars[5];
        String consent = vars[6];
        String location = vars[7];
        String POS = vars[8];
        String photo = vars[9];
        String uid = vars[10];
        int age = Integer.parseInt(vars[11]);
        String txID = getGUID("Transaction",vars[12]);
        if (indType.equals("HCW"))
        {
            if (!isHCW(guid))
                return false;
            revision = SessionProperties.getInstance(mIDContext).getHcwRevision();
            try {
                if (name == null || name.length()==0)
                    name = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("name");
                if (phoneNum == null || phoneNum.length() == 0)
                    phoneNum = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("phone");
                if (skill == null || skill.length() == 0)
                    skill = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("skill");
            } catch (DocumentNotFoundException e) {
                e.printStackTrace();
                Log.d("Update error",e.toString());
                return false;
            } catch (DocumentStoreException e) {
                e.printStackTrace();
                Log.d("Update error",e.toString());
                return false;
            }
        }
        else
        {
            if (!isPatient(guid))
                return false;
            revision = SessionProperties.getInstance(mIDContext).getPatientRevision();
        }
        String docId = guid;
        doc = new IndividualDoc(name,phoneNum,countryCode,dob,indType,gender,skill,"");
        ((IndividualDoc) doc).setPhotoStr(photo);
        ((IndividualDoc) doc).setConsent(consent);
        ((IndividualDoc) doc).setLocation(location);
        ((IndividualDoc) doc).setPOS(POS);
        ((IndividualDoc) doc).setUID(uid);
        doc.setAge(age);
        try {
            List<String> txList = (List<String>) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("Transactions");
            if (txList == null)
                txList = new ArrayList<String>();
            txList.add(txID);
            ((IndividualDoc) doc).setTxList(txList);

            //int curVersionForDoc = (int) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("version");
            //doc.setVersionNum(curVersionForDoc+1);
            Log.d("PhoneID used for updating doc: ",SessionProperties.getInstance(mIDContext).getPhoneID());
            doc.setPhoneID(SessionProperties.getInstance(mIDContext).getPhoneID());
            outDoc = this.mStore.updateDocument(doc,docId,revision);
            if (indType.equals("HCW"))
                SessionProperties.getInstance(mIDContext).setHcwRevision(outDoc.getDocumentRevision().getRevision());
            else
                SessionProperties.getInstance(mIDContext).setPatientRevision(outDoc.getDocumentRevision().getRevision());
        } catch (ConflictException e) {
            e.printStackTrace();
            Log.d("Update error",e.toString());
            return false;
        } catch (DocumentStoreException | DocumentNotFoundException e) {
            e.printStackTrace();
            Log.d("Update error",e.toString());
            return false;
        }

        return true;
    }

    /**
     *
     * This can be used to delete a doc from its guid. Mention the docType as well.
     * This will work only for HCW,PATIENT and biometric doc types.
     * You are not allowed to delete transactions
     * @param docType : document type.
     * @param guid : GU id
     * @return : boolean.
     */
    public boolean deleteDoc(int docType,String guid)
    {
        if (docType ==TRANSACTION)   //not allowed to delete transactions
            return false;
        String name = "";
        String phoneNum = "";
        String countryCode = "";
        String dob = "";
        String indType = "";
        if (docType == HCW)
            indType = "HCW";
        else if (docType == PATIENT)
            indType = "Patient";
        String gender = "";
        String skill = "";
        String consent = "";
        String location = "";
        String POS = "";
        String photo = "";
        String uid = "";
        String bioData = "";
        String bioName = "";
        int age = 0;
        List<String> guidList = new ArrayList<String>();
        SampleDoc doc;
        String docId = guid;
        DocumentRevision docRev = new DocumentRevision(guid);
        //handle biometric case
        if (isBiometric(guid))
        {
            try {
                bioData = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("BiometricData");
                bioName = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("BiometricName");
                docRev = (DocumentRevision) this.mStore.getDocumentStore().database().read(guid);
                doc = new BiometricDoc(bioName,bioData);
                doc.setDocumentRevision(docRev);
            } catch (DocumentNotFoundException e) {
                Log.e("Bio-error",e.toString());
                return false;
            } catch (DocumentStoreException e) {
                Log.e("Bio-error",e.toString());
                return false;
            }
        }
        else
        {
            if (mSearchMethod == CACHE_ONLY)
            {
                name = UserCache.getCache().searchForUserParam(indType,guid,"name");
                phoneNum = UserCache.getCache().searchForUserParam(indType,guid,"phone");
                skill = UserCache.getCache().searchForUserParam(indType,guid,"skill");
                String ageStr = UserCache.getCache().searchForUserParam(indType,guid,"age");
                if (ageStr != null && ageStr.length() > 0)
                    age = Integer.parseInt(ageStr);
                uid = UserCache.getCache().searchForUserParam(indType,guid,"uid");
                photo = UserCache.getCache().searchForUserParam(indType,guid,"photo");
                consent = UserCache.getCache().searchForUserParam(indType,guid,"consent");
                location = UserCache.getCache().searchForUserParam(indType,guid,"location");
                POS = UserCache.getCache().searchForUserParam(indType,guid,"POS");
                dob = UserCache.getCache().searchForUserParam(indType,guid,"DOB");
            }
            else if (mSearchMethod == DB_AND_CACHE || mSearchMethod == DB_ONLY)
            {
                try {
                    name = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("name");
                    phoneNum = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("phone");
                    skill = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("skill");
                    String ageStr = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("age");
                    if (ageStr != null && ageStr.length() > 0)
                        age = Integer.parseInt(ageStr);
                    uid = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("uid");
                    photo = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("photo");
                    consent = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("consent");
                    location = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("location");
                    POS = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("POS");
                    dob = (String) this.mStore.getDocumentStore().database().read(guid).getBody().asMap().get("DOB");
                } catch (DocumentNotFoundException e) {
                    e.printStackTrace();
                    Log.e("Update error",e.toString());
                    return false;
                } catch (DocumentStoreException e) {
                    e.printStackTrace();
                    Log.e("Update error",e.toString());
                    return false;
                }
            }

            try {
                docRev = (DocumentRevision) this.mStore.getDocumentStore().database().read(guid);
            } catch (DocumentNotFoundException e) {
                e.printStackTrace();
                Log.e("Update error",e.toString());
                return false;
            } catch (DocumentStoreException e) {
                e.printStackTrace();
                Log.e("Update error",e.toString());
                return false;
            }

            if (indType.equals("HCW"))
            {
                if (!isHCW(guid))
                    return false;

            }
            else
            {
                if (!isPatient(guid))
                    return false;
            }
            doc = new IndividualDoc(name,phoneNum,countryCode,dob,indType,gender,skill,"");
            ((IndividualDoc) doc).setPhotoStr(photo);
            ((IndividualDoc) doc).setConsent(consent);
            ((IndividualDoc) doc).setLocation(location);
            ((IndividualDoc) doc).setPOS(POS);
            ((IndividualDoc) doc).setUID(uid);
            doc.setAge(age);
            doc.setDocumentRevision(docRev);
        }

        try {
            this.mStore.deleteDocument(doc,docId,docRev.getRevision());
        } catch (ConflictException e) {
            e.printStackTrace();
            Log.e("Update error",e.toString());
            return false;
        } catch (DocumentStoreException e) {
            e.printStackTrace();
            Log.e("Update error",e.toString());
            return false;
        } catch (DocumentNotFoundException e) {
            e.printStackTrace();
            Log.e("Update error",e.toString());
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            Log.e("Update error",e.toString());
            return false;
        }
        //make sure u remove cache as well.
        //to be done only for HCW and patient biometric docs. We are not storing fingerprints in cache
        if (guid.substring(0,GUID_INDICATOR_SIZE).equals("BHW_") || guid.substring(0,GUID_INDICATOR_SIZE).equals("BPT_"))
        {
            SegmentationAndMatching.deleteBioJSON(guid);
        }
        else if (!isBiometric(guid))
        {
            UserCache.getCache().deleteUserFromParamsMap(indType,"name",name,guid);
            UserCache.getCache().deleteUserFromParamsMap(indType,"age",Integer.toString(age),guid);
            UserCache.getCache().deleteUserFromParamsMap(indType,"uid",uid,guid);
            UserCache.getCache().deleteUserFromParamsMap(indType,"epochDOB",dob,guid);
            UserCache.getCache().deleteFromGUIDMap(guid);
        }
        return true;
    }

    private boolean readBiometricJSON(String bioJSON,Map<String,String> biometricData) {
        JSONObject biometricJSON = null;
        try {
            biometricJSON = new JSONObject(bioJSON);
            Log.d("Biometric data : ",bioJSON);
            JSONObject leftHand = biometricJSON.getJSONObject("fingerPrint").getJSONObject("left");
            JSONObject rightHand = biometricJSON.getJSONObject("fingerPrint").getJSONObject("right");
            JSONObject leftHandT = biometricJSON.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("left");
            JSONObject rightHandT = biometricJSON.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("right");
            String tmpStr = "";
            tmpStr = biometricJSON.getJSONObject("eye").getJSONObject("image").getString("left");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftImage",tmpStr);
            tmpStr = biometricJSON.getJSONObject("eye").getJSONObject("image").getString("right");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightImage",tmpStr);
            tmpStr = biometricJSON.getJSONObject("eye").getJSONObject("iris").getString("left");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftIris",tmpStr);
            tmpStr = biometricJSON.getJSONObject("eye").getJSONObject("iris").getString("right");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightIris",tmpStr);
            tmpStr = leftHand.getString("thumb");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftThumbFingerPrint",tmpStr);

            tmpStr = leftHand.getString("index");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftIndexFingerPrint",tmpStr);
            tmpStr = leftHand.getString("middle");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftMiddleFingerPrint",tmpStr);
            tmpStr = leftHand.getString("ring");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftRingFingerPrint",tmpStr);
            tmpStr = leftHand.getString("pinky");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftPinkyFingerPrint",tmpStr);
            tmpStr = rightHand.getString("thumb");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightThumbFingerPrint",tmpStr);
            tmpStr = rightHand.getString("index");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightIndexFingerPrint",tmpStr);
            tmpStr = rightHand.getString("middle");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightMiddleFingerPrint",tmpStr);
            tmpStr = rightHand.getString("ring");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightRingFingerPrint",tmpStr);
            tmpStr = rightHand.getString("pinky");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightPinkyFingerPrint",tmpStr);

            tmpStr = leftHandT.getString("thumb");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftThumbFingerTemplate",tmpStr);
            tmpStr = leftHandT.getString("index");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftIndexFingerTemplate",tmpStr);
            tmpStr = leftHandT.getString("middle");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftMiddleFingerTemplate",tmpStr);
            tmpStr = leftHandT.getString("ring");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftRingFingerTemplate",tmpStr);
            tmpStr = leftHandT.getString("pinky");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("leftPinkyFingerTemplate",tmpStr);
            tmpStr = rightHandT.getString("thumb");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightThumbFingerTemplate",tmpStr);
            tmpStr = rightHandT.getString("index");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightIndexFingerTemplate",tmpStr);
            tmpStr = rightHandT.getString("middle");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightMiddleFingerTemplate",tmpStr);
            tmpStr = rightHandT.getString("ring");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightRingFingerTemplate",tmpStr);
            tmpStr = rightHandT.getString("pinky");
            if (tmpStr != null && tmpStr.length() > 0)
                biometricData.put("rightPinkyFingerTemplate",tmpStr);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("Update error",e.toString());
            return false;
        } catch (NullPointerException e1) {
            e1.printStackTrace();
            Log.d("Update error",e1.toString());
            return false;
        }
        return true;
    }

    private boolean addNewDoc(int docType,StringBuffer guiddata,String name,String...vars) {
        long dbAddStart = System.currentTimeMillis();
        Random rand = new Random();
        SampleDoc doc;
        int randomNum = rand.nextInt(18)*79000;
        String docId = "";
        DocumentRevision rev;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Date date = new Date();
        String indType = "";
        if (docType == INDIVIDUAL_DOC)
        {
            String phoneNum = vars[0];
            String countryCode = vars[1];
            String dob = vars[2];
            indType = vars[3];
            if (indType.equals("HCW"))
                docId = getGUID("HCW");
            else
                docId = getGUID("Patient");
            guiddata.append(docId);
            String gender = vars[4];
            String skill = vars[5];
            String uid = vars[6];
            String govID = vars[7];
            int age = 0;
            if (vars[8] != null && vars[8].length() > 0)
                age = Integer.parseInt(vars[8]);
            String txID = getGUID("Transaction",vars[9]);
            String consent = "";
            doc = new IndividualDoc(name,phoneNum,countryCode,dob,indType,gender,skill,txID);
            ((IndividualDoc) doc).setUID(uid);
             doc.setAge(age);
             if (indType.equals("HCW"))
                ((IndividualDoc) doc).setConsent(SessionProperties.getInstance(mIDContext).getHCWConsent());
             else
                 ((IndividualDoc) doc).setConsent(SessionProperties.getInstance(mIDContext).getPatientConsent());
            ((IndividualDoc) doc).setEpochDOB(dob);
            Log.d("PhoneID used for adding doc: ",SessionProperties.getInstance(mIDContext).getPhoneID());
            doc.setPhoneID(SessionProperties.getInstance(mIDContext).getPhoneID());
        }
        else if (docType == TRANSACTION_DOC)
        {
            String txGUID = vars[0];
            String txtime = vars[1];
            String hcwGUID = vars[2];
            String patientGUID = vars[3];
            String content = vars[4];
            docId = getGUID("Transaction",txGUID);
            guiddata.append(docId);
            doc = new TransactionDoc(name,txGUID,txtime,hcwGUID,patientGUID,content);
        }
        else if (docType == BIOMETRIC_DOC)
        {
            docId = getGUID(name,vars[0].substring(GUID_INDICATOR_SIZE));
            guiddata.append(docId);
            String bioJSON = vars[1];
            doc = new BiometricDoc(name,bioJSON);
        }
        else if (docType == GENERAL_DOC)
        {
            docId = getGUID("General");
            guiddata.append(docId);
            String phoneID = vars[0];
            doc = new GeneralDoc(name);
            doc.setPhoneID(phoneID);
        }
        else
            return false;

        doc.setVersionNum(INITIAL_VERSION);
        rev = new DocumentRevision(docId);
        rev.setBody(DocumentBodyFactory.create(doc.asMap()));

        try {
            long revCreateStart = System.currentTimeMillis();
            DocumentRevision created;
            if (docType == GENERAL_DOC)
                created = this.mLocalStore.getDocumentStore().database().create(rev);
            else
                created = this.mStore.getDocumentStore().database().create(rev);
            if (docType ==  INDIVIDUAL_DOC || docType == BIOMETRIC_DOC)
                doc.setDocumentRevision(created);
            long revCreateEnd = System.currentTimeMillis();
            System.out.println("Time to create a new rev in DB: "+(revCreateEnd-revCreateStart));
            if (docType == INDIVIDUAL_DOC)
            {
                if (indType.equals("HCW"))
                    SessionProperties.getInstance(mIDContext).setHcwRevision(created.getRevision());
                else
                    SessionProperties.getInstance(mIDContext).setPatientRevision(created.getRevision());
            }

            Log.d("PrintRev1",created.getRevision());
            long docsReloadStart = System.currentTimeMillis();
            //reloadDocsFromStore();
            long docsReloadEnd = System.currentTimeMillis();
            System.out.println("Time to reload docs: "+(docsReloadEnd-docsReloadStart));
            long dbAddEnd = System.currentTimeMillis();
            System.out.println("Time to add doc to DB: "+(dbAddEnd-dbAddStart));
            return true;
        } catch (DocumentException de) {
            Log.e("Replicator", de.getMessage());
            return false;
        } catch (DocumentStoreException de) {
            Log.e("Replicator", de.getMessage());
            return false;
        }
    }
}
