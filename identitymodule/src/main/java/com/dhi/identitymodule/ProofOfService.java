package com.dhi.identitymodule;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Environment;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
/**
 * This class contains utility functions needed to generate the images in the Proof of service screen in IdM
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class ProofOfService {

    private static Bitmap scaleAndCrop(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    private static Bitmap scaleCropToFit(Bitmap original, int targetWidth, int targetHeight){
        //Need to scale the image, keeping the aspect ration first
        if(original == null){
            Bitmap t = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
            t.eraseColor(Color.GRAY);
            return  t;
        }
        int width = original.getWidth();
        int height = original.getHeight();

        float widthScale = (float) targetWidth / (float) width;
        float heightScale = (float) targetHeight / (float) height;
        float scaledWidth;
        float scaledHeight;

        int startY = 0;
        int startX = 0;

        if (widthScale > heightScale) {
            scaledWidth = targetWidth;
            scaledHeight = height * widthScale;
            //crop height by...
            startY = (int) ((scaledHeight - targetHeight) / 2);
        } else {
            scaledHeight = targetHeight;
            scaledWidth = width * heightScale;
            //crop width by..
            startX = (int) ((scaledWidth - targetWidth) / 2);
        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(original, (int) scaledWidth, (int) scaledHeight, true);

        Bitmap resizedBitmap = Bitmap.createBitmap(scaledBitmap, startX, startY, targetWidth, targetHeight);
        return resizedBitmap;
    }


    private static Bitmap createProofImage( String message, Bitmap newhcw, Bitmap newpatient, Bitmap newsign) {
        int Finalw=1080;
        int Finalh=1920;
        int patientw=Finalw;
        int patienth= Finalh*3/4;
        int hcww=patientw/3;
        int hcwh=patientw/3;
        int signw=Finalw;
        int signh=Finalh/4;

        //Lets Rescale all images
        Bitmap hcw = scaleCropToFit(newhcw,hcww,hcwh);
        Bitmap patient = scaleCropToFit(newpatient,patientw,patienth);
        Bitmap sign = scaleCropToFit(newsign,signw,signh);

        Bitmap finalBitmap = Bitmap.createBitmap(Finalw, Finalh, patient.getConfig());
        Canvas canvas = new Canvas(finalBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(patient, new Matrix(), null);
        canvas.drawBitmap(sign, 0, patient.getHeight(), null);
        canvas.drawBitmap(hcw, patient.getWidth() - hcw.getWidth(), patient.getHeight() - hcw.getHeight(), null);
        {
            Paint p=new Paint();
            p.setColor(Color.WHITE);
            p.setAlpha(128);
            canvas.drawRect(0,patienth-hcwh,patientw-hcww,patienth,p);
        }

        // new antialiased Paint
        int scale=1;
        TextPaint paint=new TextPaint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        paint.setTextSize((int) (50 * scale));
        paint.setAlpha(255);
        int textWidth = (patient.getWidth() - hcw.getWidth()) - (int) (16 * scale);
        StaticLayout.Builder sb = StaticLayout.Builder.obtain(message,0,message.length(), paint, textWidth)
                .setAlignment(Layout.Alignment.ALIGN_LEFT)
                .setLineSpacing(0.0f, 1.0f)
                .setIncludePad (false);
        StaticLayout textLayout = sb.build();

        // get height of multiline text
        int textHeight = textLayout.getHeight();

        // get position of text's top left corner
        float x = 0;
        float y = patient.getHeight() - hcw.getHeight();

        // draw text to the Canvas center
        canvas.save();
        canvas.translate(x, y);
        textLayout.draw(canvas);
        canvas.restore();

        //DumpBitmap(finalBitmap,"final.png");
        return finalBitmap;
    }

    private static void dumpBitmap(Bitmap toDisk, String fmname) {

        try {
            String path = Environment.getExternalStorageDirectory()+ "/mad/" + fmname;
            File p = new File(path);
            toDisk.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(p));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    private static void saveBitmap(Bitmap toDisk, String path) {
        try {

            File p = new File(path);
            toDisk.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(p));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    /**
     *
     * This method gets a Base64 encoded string from given bitmap
     * @param bmp : bit map
     * @return : String
     */
    public static String getStringImageBase64(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        byte[] imageBytes = baos.toByteArray();
        String len = Integer.toString(imageBytes.length);
        Log.d("Compressed Image Size ",len);
        String encodedImage = android.util.Base64.encodeToString(imageBytes, android.util.Base64.NO_WRAP);
        return encodedImage;
    }

    /**
     * This method gets a bitmap from base64 String.
     * @param base64Str : Base 64 String information.
     * @return : Bitmap.
     */
    public static Bitmap getImageFromBase64String(String base64Str) {
        byte[] imgBytes = android.util.Base64.decode(base64Str, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imgBytes,0,imgBytes.length);
    }

    private static Bitmap getProofOfServiceImage(Context ctx) {
        String txtime = IdentityModule.getCurrentTime();
        String wr = SessionProperties.getInstance(ctx).getHcwGUID() +"\n" +  SessionProperties.getInstance(ctx).getPatientGUID() + "\n" + "Time: "+ txtime + "\n" +"Location: "+ SessionProperties.getInstance(ctx).getLocationStr() +"\n";// + "HCW: "+m_hcw +"\n" +"Patient: "+ m_patient + "\n";
        Bitmap bMap_HCW =   SessionProperties.getInstance(ctx).getPhoto(SessionProperties.getInstance(ctx).HCW_PIC);
        Bitmap bMap_patient = SessionProperties.getInstance(ctx).getPhoto(SessionProperties.getInstance(ctx).PATIENT_PIC);
        Bitmap bMap_sign = SessionProperties.getInstance(ctx).getPhoto(SessionProperties.getInstance(ctx).PATIENTSIG_PIC);

        return (ProofOfService.createProofImage(wr,bMap_HCW,bMap_patient,bMap_sign));
    }

    /**
     *
     * This method gives a complete proof of service (POS) bitmap by stitching together the HCW and Patient pic and signature with other session data required.
     * We also pass a StringBuffer which gets populated with the base 64 encoded string of the POS bitmap generated
     * @param ctx
     * @param posImageStr
     * @return
     */
    public static Bitmap handleProofOfService(Context ctx, StringBuffer posImageStr) {
        Bitmap posImage = getProofOfServiceImage(ctx);
        posImageStr.append(getStringImageBase64(posImage));
        return posImage;
    }

}