package com.dhi.identitymodule;

/**
 * This class represents the various app types supported by Identity apk. these can be configured by a config file.
 * Based on app type, we may show different functionality. Ex: if someone wants no demographics and just biometrics, we can do that by setting a flag: BIOMETRIC_ONLY
 * Right now, we support the following options:
 * 1. GENERAL_APP (default): all the default set of screens will be displayed.
 * 2. BIOMETRIC_ONLY: only biometric screens will be displayed. No demographic screens will be shown.
 * Date: Nov 20 2019
 * @author IPRD
 * @version 1.0
 */
public enum AppType {
    GENERAL_APP,
    BIOMETRIC_ONLY,
    NULL
}