package com.dhi.identitymodule;

import java.util.HashMap;
import java.util.Map;

/**
 * This class stores general information that are needed for the app and have nothing to do with the user
 * As of now, this doc will be unique and will have a standard GUID: GEN_0001.
 */

public class GeneralDoc extends SampleDoc {
    public GeneralDoc(String docName) {
        super(docName);
    }

    private String mPhoneID;
    public String getPhoneID() {
        return mPhoneID;
    }

    public void setPhoneID(String phoneID) {
        this.mPhoneID = phoneID;
    }

    /**
     * This method returns a map that has all the data contained in this class.
     *
     * @return
     */
    @Override
    public Map<String, Object> asMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("PhoneID", mPhoneID);
        return map;
    }
}
