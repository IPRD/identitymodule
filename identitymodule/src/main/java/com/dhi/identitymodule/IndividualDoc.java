package com.dhi.identitymodule;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * This class is a derived class from SampleDoc that stores individual data - HCW and Patient.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class IndividualDoc extends SampleDoc {
    public IndividualDoc(String name, String phoneNum,String countryCode,String dob,String indType, String gender, String skill,String txID) {
        super(name);
        if (indType != null && indType.length() > 0)
            Log.d("Type", indType);
        if (gender != null && gender.length() > 0)
            Log.d("Gender",gender);
        if (skill != null && skill.length() > 0)
            Log.d("Skill",skill);
        this.setPhoneNumber(phoneNum);
        this.setCountryCode(countryCode);
        this.setDOB(dob);
        this.setIndType(indType);
        this.setGender(gender);
        this.setSkill(skill);
        this.initContent();
        if (mTxList == null) {
            mTxList = new ArrayList<>();
        }
        if (txID != null)
            mTxList.add(txID);
    }

    private int mAge;
    private String mUid;

    public int getAge() {
        return mAge;
    }

    public void setAge(int age) {
        this.mAge = age;
    }

    public String getUID() {
        return mUid;
    }

    public void setUID(String uid) {
        this.mUid = uid;
    }

    private String mPhoneNumber;
    private String getPhoneNumber() { return this.mPhoneNumber; }
    private void setPhoneNumber(String phoneNumber) { this.mPhoneNumber = phoneNumber; }

    private String mCountryCode;
    private String getCountryCode() {
        return mCountryCode;
    }

    private void setCountryCode(String countryCode) {
        this.mCountryCode = countryCode;
    }

    private String mDOB;
    public String getDOB() { return this.mDOB; }
    public void setDOB(String dob) { this.mDOB = dob; }

    private long mEpochDOB;
    public long getEpochDOB() {
        return mEpochDOB;
    }

    public void setEpochDOB(long epochDOB) {
        this.mEpochDOB = epochDOB;
    }

    public void setEpochDOB(String epochDOB) {
        this.mEpochDOB = Long.parseLong(epochDOB);
    }

    private String mIndType;
    public String getIndType() {
        return this.mIndType;
    }
    public void setIndType(String indType) { this.mIndType = indType; }


    private String mGender;
    public String getGender() {
        return this.mGender;
    }
    public void setGender(String gender) { this.mGender = gender; }

    private String mSkill;
    public String getSkill() {
        return this.mSkill;
    }
    public void setSkill(String skill) { this.mSkill = skill; }

    private String mConsent;
    public String getConsent() { return mConsent; }
    public void setConsent(String consent) { this.mConsent = consent; }

    private String mPOS;
    public String getPOS() {
        return mPOS;
    }
    public void setPOS(String POS) {
        this.mPOS = POS;
    }

    private String mLocation;
    public String getLocation() {
        return mLocation;
    }
    public void setLocation(String location) {
        this.mLocation = location;
    }

    private String mPhotoStr;
    public String getPhotoStr() {
        return mPhotoStr;
    }

    public void setPhotoStr(String photoStr) {
        this.mPhotoStr = photoStr;
    }


    private HashMap<String,Object> mContentMap;
    public HashMap<String,Object> getContents() {
        return this.mContentMap;
    }
    public void initContent() {
        //to be called when u add a new user (HCW or patient)
        mContentMap = new HashMap<>();
    }
    public void add2Content(String txGUID, TransactionDoc txDoc) {
        mContentMap.put(txGUID,txDoc);
    }
    public int getContentLength() { return mContentMap.size(); }

    private List<String> mTxList;
    public List<String> getTxList() {
        return mTxList;
    }

    public void setTxList(List<String> txList) {
        this.mTxList = txList;
    }

    public boolean addToTxList(String txn)
    {
        if (mTxList != null)
            mTxList.add(txn);
        else
            return false;
        return true;
    }

    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", getType());
        map.put("completed", isCompleted());
        map.put("name", getName());
        map.put("phone",getPhoneNumber());
        map.put("countryCode",getCountryCode());
        map.put("DOB",getDOB());
        map.put("indType",mIndType);
        map.put("gender",mGender);
        map.put("skill",mSkill);
        map.put("consent",mConsent);
        map.put("POS",mPOS);
        map.put("location",mLocation);
        map.put("photo",mPhotoStr);
        map.put("uid",mUid);
        map.put("age",Integer.toString(mAge));
        map.put("contents",mContentMap);
        map.put("EpochDOB",Long.toString(mEpochDOB));
        map.put("Transactions",mTxList);
        map.put("version",getVersionNum());
        map.put("phoneID",getPhoneID());
        return map;
    }

}
