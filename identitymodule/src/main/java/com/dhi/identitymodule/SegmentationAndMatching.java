package com.dhi.identitymodule;

import android.content.Context;
import android.graphics.Rect;
import android.util.Base64;
import android.util.Log;

import com.neurotec.biometrics.NBiometricOperation;
import com.neurotec.biometrics.NBiometricStatus;
import com.neurotec.biometrics.NBiometricTask;
import com.neurotec.biometrics.NFPosition;
import com.neurotec.biometrics.NFace;
import com.neurotec.biometrics.NFinger;
import com.neurotec.biometrics.NIris;
import com.neurotec.biometrics.NLAttributes;
import com.neurotec.biometrics.NLivenessMode;
import com.neurotec.biometrics.NMatchingResult;
import com.neurotec.biometrics.NSubject;
import com.neurotec.biometrics.NTemplate;
import com.neurotec.biometrics.NTemplateSize;
import com.neurotec.biometrics.client.NBiometricClient;
import com.neurotec.devices.NDevice;
import com.neurotec.devices.NDeviceType;
import com.neurotec.images.NImage;
import com.neurotec.io.NBuffer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

/**
 *
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class SegmentationAndMatching {
    private static SegmentationAndMatching mOurInstance = null;
    private static NeuroLicense mLic = null;
    private static String TAG= "SegmentationAndMatching";
    private Context mCtx;
    private static Vector<NSubject> m_leftIrisList= null;
    private static Vector<NSubject> m_rightIrisList= null;
    private static Vector<NSubject> m_leftFingerList= null;
    private static Vector<NSubject> m_rightFingerList= null;
    private static NBiometricClient m_biometricClient = null;
    private static Vector<NSubject> m_faceList= null;
    private static boolean mLicenseCheckDone = false;
    private SegmentationAndMatching(Context ctx) {
        mCtx = ctx;
    }

    public NBiometricClient getBiometricClient(){
        return m_biometricClient;
    }

    public boolean getBiometricIrisLicStatus(){
        if(mLic == null) return false;
        return mLic.getIrisLicStatus();
    }
    public boolean getBiometricFPLicStatus(){
        if(mLic == null) return false;
        return mLic.getFPLicStatus();
    }

    public boolean getFaceLicStatus() {
        if (mLic == null) return false;
        return mLic.getFaceLicStatus();
    }

    public static boolean checkLicense() {
        if (mLic != null)
        {
            if(mLic.licenseCheckWithServer()) {
                mLicenseCheckDone = true;
                return true;
            }
            else
                return false;
        }
        return false;
    }

    /**
     *
     * Checking whether license is done or not.
     * @return : boolean.
     */
    public static boolean isLicenseCheckDone()
    {
        return mLicenseCheckDone;
    }

    /**
     *
     * @param ctx
     * @return
     */
    public static SegmentationAndMatching getInstance(Context ctx) {
        if (mOurInstance == null) {
            mOurInstance = new SegmentationAndMatching(ctx);
        }
        if(mLic == null){
            mLic = new NeuroLicense(ctx);
            mLic.licenseCheckWithServer();
            try {
                m_biometricClient = new NBiometricClient();
            } catch (ExceptionInInitializerError var1) {
                return null;
            }
            m_biometricClient.setFacesTemplateSize(NTemplateSize.LARGE); //??
            m_biometricClient.setIrisesTemplateSize(NTemplateSize.SMALL);
            m_biometricClient.setFingersTemplateSize(NTemplateSize.SMALL);
            m_biometricClient.setUseDeviceManager(true);
            m_biometricClient.setFacesLivenessMode(NLivenessMode.PASSIVE_AND_ACTIVE);
            m_biometricClient.getDeviceManager().setDeviceTypes(EnumSet.of(NDeviceType.FINGER_SCANNER,NDeviceType.CAMERA));
            Log.d("Neuro","Lets Initialize Biometric Client");
            m_biometricClient.initialize();
            Log.d("Neuro","Initialized Biometric Client");
            m_leftIrisList = new Vector<NSubject>();
            m_rightIrisList =  new Vector<NSubject>();
            m_leftFingerList =  new Vector<NSubject>();
            m_rightFingerList =  new Vector<NSubject>();
            m_faceList =  new Vector<NSubject>();

            loadBioListFromDB(ctx);
        }
        return mOurInstance;
    }

    /**
     * For getting number of scanner
     * @return
     */
    public static int getNumberOfScanner() {
        int count = 0;
        try {
            for (NDevice device : m_biometricClient.getDeviceManager().getDevices()) {
                Log.i("Device", String.format("Device name => %s", device.getDisplayName()));
                if (device.getDeviceType().contains(NDeviceType.FSCANNER)) {
                    count++;
                }
            }
        } catch (Exception e){
            Log.d("Neuro","GetScannerCount Exception " + e.getMessage());
            e.printStackTrace();
        }
        return count;
    }

    /**
     *
     * delete Bio-metric data from  GU id.
     * @param guid : GU id.
     * @return : boolean
     */
    public static boolean deleteBioJSON(String guid)
    {
           return removeBiometric(guid);
    }

    /**
     * This function loads all the biometric data from DB to the biometric lists stored in memory.
     * @param ctx
     * @return
     */
    public static boolean loadBioListFromDB(Context ctx) {
        Log.d("Info: ","About to load biometrics from DB");
        //get all biometric guids from DB
        Map<String,String> bioMap = new HashMap<String,String>();
        if (!IdentityModule.getInstance(ctx).loadBioMasterDoc(bioMap))
            return false;
        else
            Log.d("Info: ","Got biometrics from DB");
        //now lookup each from DB
        //map of biometric id and data (json)
        //from evcery individual biometric data json, generate te NSubject .
        //add each to either iris or fingerprint list
        Iterator<Map.Entry<String,String>> bioItr = bioMap.entrySet().iterator();
        while (bioItr.hasNext()) {
            Map.Entry<String,String> entry = bioItr.next();
            String guid = entry.getKey();
            String indBioRec = entry.getValue();
            //from this biometric, get the iris, finger prints
            try {
                JSONObject bData = new JSONObject(indBioRec);
                Log.d("Bio JSON recvd: ",bData.toString());
                String bioData = bData.getString("BiometricData").replaceAll("\\\\","");
                if((bioData != null) &&(bioData.length()>0)) {
                    JSONObject tmpBio = new JSONObject(bioData);
                    String data=tmpBio.getJSONObject("eye").getJSONObject("iris").getString("left");
                    byte[] leftImgByte = data.length()>0 ?android.util.Base64.decode(data, Base64.NO_WRAP):null;
                    data=tmpBio.getJSONObject("eye").getJSONObject("iris").getString("right");
                    byte[] rightImgByte = data.length()>0 ?android.util.Base64.decode(data, Base64.NO_WRAP):null;
                    data = tmpBio.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("right").getString("index");
                    byte[] fprIndex = data.length()>0 ?android.util.Base64.decode(data, Base64.NO_WRAP):null;
                    data = tmpBio.getJSONObject("fingerPrint").getJSONObject("template").getJSONObject("left").getString("index");
                    byte[] fplIndex = data.length()>0 ?android.util.Base64.decode(data, Base64.NO_WRAP):null;

                    data = tmpBio.getJSONObject("face").getString("template");
                    byte[] faceIndex = data.length()>0 ?android.util.Base64.decode(data, Base64.NO_WRAP):null;

                    if (!insertPersonData(leftImgByte, rightImgByte, fplIndex, fprIndex,faceIndex, guid))
                        Log.e("Issue", "Could not add data to local cache");
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
                Log.e("Issue",e1.toString());
            }
        }
        return true;
    }

    /**
     *
     * verify if person exists
     * @param data
     * @param //iseye
     * @param res
     * @return : bute[]
     */
    public static byte[] verifyPersonExist(String data, BiometricType biometricType,StringBuffer res){ //boolean iseye
        String ret = "";
        byte[] d = (byte[]) Base64.decode(data, Base64.NO_WRAP);
        byte[] lb =  extractTemplate(d,biometricType);
        if(lb != null){
            ret = matchPerson(lb,biometricType);
        }
        res.append(ret);
        return lb;
    }

    /**
     *
     * verify person's eye or finger (left anf right finger)
     * @param data
     * @param res
     * @param //lefteye
     * @param //righteye
     * @param //leftfp
     * @param //rightfp
     * @return : byte[]
     */
    public static byte[] verifyPersonEyeOrFingerLAndR(String data,StringBuffer res,BiometricType biometricType){//boolean lefteye,boolean righteye,boolean leftfp,boolean rightfp
        String ret = "";
        byte[] d = (byte[]) Base64.decode(data, Base64.NO_WRAP);
       // boolean eye = (lefteye|| righteye)?true:false;
        byte[] lb =  extractTemplate(d,biometricType);
        if(lb != null){
            ret= matchPersonFaceOrEyeOrFingerLAndR(lb,biometricType);//lefteye,righteye,leftfp,rightfp);
        }
        res.append(ret);
        return lb;
    }

    /**
     *
     * enroll the Person
     * @param lb
     * @param rb
     * @param lfb
     * @param rfb
     * @param guid
     * @return : boolean
     */
    public static boolean enrollPerson(byte[] lb,byte[] rb,byte[] lfb,byte[] rfb,byte[] fb, String guid){
        return insertPersonData(lb, rb, lfb, rfb, fb, guid);
    }

    private static String matchPerson(byte[] ds,BiometricType biometricType) {//boolean iseye
        String ret ="";
        int score = -1;
        StringBuffer res = new StringBuffer();
        if(biometricType == BiometricType.EYE_R || biometricType == BiometricType.EYE_L){
            StringBuffer l=new StringBuffer();
            StringBuffer r=new StringBuffer();
            int s1 =  matchAgainstDB(ds,m_leftIrisList,l);
            int s2 =  matchAgainstDB(ds,m_rightIrisList,r);
            if(s1 <s2){
                score = s2;
                res = r;
            }else{
                score = s1;
                res = l;
            }
        }else if(biometricType == BiometricType.FINGER_L || biometricType == BiometricType.FINGER_R){
            StringBuffer l=new StringBuffer();
            StringBuffer r=new StringBuffer();
            int s1 =  matchAgainstDB(ds,m_leftFingerList,l);
            int s2 =  matchAgainstDB(ds,m_rightFingerList,r);
            if(s1 <s2){
                score = s2;
                res = r;
            }else{
                score = s1;
                res = l;
            }
        }else if(biometricType == BiometricType.FACE){ //?
            StringBuffer f=new StringBuffer();
            int s =  matchAgainstDB(ds,m_faceList,f);
            score = s;
            res = f;
        }
        if(score < 100){
            StringBuffer b = new StringBuffer();
            res= b;
            Log.d(TAG,"Removing the item as matching score is less than threshold");
        }
        return res.toString();
    }

    private static String matchPersonFaceOrEyeOrFingerLAndR(byte[] ds, BiometricType biometricType) {
        String ret ="";
        int score = -1;
        StringBuffer res = new StringBuffer();
        if(biometricType == BiometricType.EYE_L || biometricType == BiometricType.EYE_R){
            if(biometricType == BiometricType.EYE_L) {
                score = matchAgainstDB(ds, m_leftIrisList, res);
            }
            if(biometricType == BiometricType.EYE_R) {
                score = matchAgainstDB(ds, m_rightIrisList, res);
            }
        }
        if(biometricType == BiometricType.FINGER_L || biometricType == BiometricType.FINGER_R){
            if(biometricType == BiometricType.FINGER_L) {
                score = matchAgainstDB(ds, m_leftFingerList, res);
            }
            if(biometricType == BiometricType.FINGER_R) {
                score = matchAgainstDB(ds, m_rightFingerList, res);
            }
        }
        if(biometricType == BiometricType.FACE){
            score = matchAgainstDB(ds,m_faceList,res);
        }
        if(score < 100){
            StringBuffer b = new StringBuffer();
            res = b;
            Log.d(TAG,"Removing the item as matching score is less than threshold");
        }
        return res.toString();
    }

    private static NSubject createSubjectFromTemplate(byte[] l, String id){
        NSubject n = new NSubject();
        NBuffer b = new NBuffer(l);
        n.setTemplateBuffer(b);
        n.setId(id);
        return n;
    }

    /**
     * this returns false if called and none of the parameters passed are valid
     * @param l
     * @param r
     * @param fl
     * @param fr
     * @param f
     * @param id
     * @return
     */
    private static boolean insertPersonData(byte[] l, byte[] r, byte[] fl,byte[] fr, byte[] f,String id){
        Log.d("Bio-match","InsertPersonData id.toString()");
        boolean ret = false;
        if((l != null)) {
            Log.d("Bio-match","left eye added");
            m_leftIrisList.add(createSubjectFromTemplate(l,id));
            ret = true;
        }
        if((r != null)) {
            Log.d("Bio-match","right eye added");
            m_rightIrisList.add(createSubjectFromTemplate(r,id));
            ret = true;
        }
        if((fl != null)) {
            Log.d("Bio-match","finger index added");
            m_leftFingerList.add(createSubjectFromTemplate(fl,id));
            ret = true;
        }
        if((fr != null)) {
            Log.d("Bio-match","finger index added");
            m_rightFingerList.add(createSubjectFromTemplate(fr,id));
            ret = true;
        }
        if(f != null){
            Log.d("Bio-match","face index added");
            m_faceList.add(createSubjectFromTemplate(f,id));
            ret = true;
        }
        return ret;
    }

    private static void removesubject( Vector<NSubject> l,String id){
        if (l == null)
            return;
        for (int i=0;i<l.size();i++)
        {
            NSubject lNS = l.get(i);
            if (lNS.getId().equals(id)) {
                l.remove(lNS);
            }
        }
    }

    private static boolean removeBiometric(String id) {
        Log.d("Bio-match","Deleting biometric data");
        removesubject(m_leftIrisList,id);
        removesubject(m_rightIrisList,id);
        removesubject(m_leftFingerList,id);
        removesubject(m_rightFingerList,id);
        removesubject(m_faceList, id);
        return true;
    }

    private static int matchAgainstDB(byte[] input,Vector<NSubject> sublist, StringBuffer b){
        int score = -1;
        NBiometricTask task = null;
        NBiometricStatus status = null;
        NSubject ins = null;
        String resid = "";

        ins = createSubjectFromTemplate(input,"Testing ID");
        for (int i = 0; i < sublist.size(); i++) {
            try {
                task = m_biometricClient.createTask(EnumSet.of(NBiometricOperation.VERIFY_OFFLINE), ins, sublist.elementAt(i));
                m_biometricClient.performTask(task);
                status = task.getStatus();
                if (task.getError() != null) {
                    Log.e(TAG, "Matching Failed" + task.getError());
                }
                for (NMatchingResult result : ins.getMatchingResults()) {
                    Log.d(TAG, "Matching Score " + "indx :: " + Integer.toString(i) + " score :: " + Integer.toString(result.getScore()));
                    if (result.getScore() > score) {
                        score = result.getScore();
                        resid = sublist.elementAt(i).getId();
                    }
                }
            }catch (Exception ex) {
                    Log.e(TAG+" Error",ex.getMessage());
            }
        }
        Log.d(TAG, "Matching Result GUID " + resid.toString() + "  Score: "+ Integer.toString(score));
        b.append(resid);
        return score;
    }

    /**
     *
     * Get bytes from file, file path is given as method parameter.
     * @param filePath
     * @return : byte[]
     */

    public static byte[] getbytesFromFile(String filePath)  {
        byte[] bFile = null;
        try {
                File file = new File(filePath);
                int size = (int) file.length();
                bFile = new byte[size];
                try {
                    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                    buf.read(bFile, 0, bFile.length);
                    buf.close();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
               }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bFile;
    }

    /**
     * load external disk information
     * @param ctx
     * @return
     */
    public static String loadExternalDisk(Context ctx){
        return "/mnt/sdcard";
    }

    private static byte[] extractTemplate(byte[] l,BiometricType biometricType) {
        byte[] ret=null;

        if (biometricType == BiometricType.FACE) {
            try {
                return generateImageTemplate(l);
            } catch (IOException e) {
                Log.e("Template extract error: ",e.toString());
                return ret;
            }
        }

        NSubject subject = null;
        NIris iris = null;
        NFinger finger = null;
        NFace face = null;
        NBiometricTask task = null;
        NBiometricStatus status = null;

        try {
            subject = new NSubject();
            if(biometricType == BiometricType.EYE_L || biometricType == BiometricType.EYE_R) {
                iris = new NIris();
                iris.setImage(NImage.fromMemory(ByteBuffer.wrap(l)));
                subject.getIrises().add(iris);

            }else if(biometricType == BiometricType.FINGER_L || biometricType == BiometricType.FINGER_R){
                finger = new NFinger();
                NImage n = NImage.fromMemory(NBuffer.fromArray(l));
                n.setResolutionIsAspectRatio(false);
                n.setHorzResolution(800);
                n.setVertResolution(750);
                Log.d(TAG,"Resolution :" + n.getHorzResolution()+n.getVertResolution());
                finger.setImage(n);
                finger.setPosition(NFPosition.UNKNOWN);
                subject.getFingers().add(finger);
            }else if(biometricType == BiometricType.FACE){
                face = new NFace();
                NImage n = NImage.fromMemory(NBuffer.fromArray(l));
                Log.d(TAG,"Resolution :" + n.getHorzResolution()+n.getVertResolution());
                face.setImage(n);
                subject.getFaces().add(face);
                //???????????????????????????????????????????????????
            }

            task = m_biometricClient.createTask(EnumSet.of(NBiometricOperation.CREATE_TEMPLATE), subject);
            m_biometricClient.performTask(task);

            if (task.getError() != null) {
                String excp =  task.getError().toString();
                Log.e(TAG,"Failure CREATING TEMPLATE "+excp);
                return null;
            }
            status = task.getStatus();
            if(biometricType == BiometricType.EYE_R || biometricType == BiometricType.EYE_L) {
                if (subject.getIrises().size() == 1) {
                    Log.d(TAG, "Success Extraction IRIS");
                }
            }else if(biometricType == BiometricType.FINGER_L || biometricType == BiometricType.FINGER_L){
                if (subject.getFingers().size() == 1) {
                    Log.d(TAG, "Success Extraction Fingers");
                }
            }else if(biometricType == BiometricType.FACE){
                if (subject.getFaces().size() == 1) {
                    Log.d(TAG, "Success Extraction Faces");
                }
            }


            if (status == NBiometricStatus.OK) {
                NTemplate n = subject.getTemplate();
                ret = subject.getTemplateBuffer().toByteArray();
                Log.d(TAG, "Extracted template of size : " +Integer.toString(ret.length));

            }
        }catch (Exception ex) {
            Log.e(TAG+" Error",ex.getMessage());
        } finally {
            if (subject != null) subject.dispose();
            if (iris != null) iris.dispose();
            if (finger != null) finger.dispose();
            if (face != null) face.dispose();
        }
        return ret;
    }
    public static byte[] generateImageTemplate(byte[] img) throws IOException {
        NSubject subject = null;
        NFace face = null;
        NBiometricTask task = null;
        NBiometricStatus status = null;
        byte[] ret = null;

        try {
            subject = new NSubject();
            face = new NFace();
            // Read face image and add it to NFace object
            face.setImage(NImage.fromMemory(ByteBuffer.wrap(img)));
            // Add face image to NSubject
            subject.getFaces().add(face);
            // Create task
            task = m_biometricClient.createTask(EnumSet.of(NBiometricOperation.CREATE_TEMPLATE, NBiometricOperation.ASSESS_QUALITY), subject);

            // Perform task
            m_biometricClient.performTask(task);
            status = task.getStatus();
            Log.d("Status of bio task: ",status.name());
            if (task.getError() != null) {
                Log.e("Error in bio task: ",task.getError().getMessage());
                return ret;
            }
            if (subject.getFaces().size() > 1)
                Log.d(TAG, String.format("Found %d faces\n", subject.getFaces().size() - 1));
            // List attributes for all located faces
            int q = 0;
            for (NFace nface : subject.getFaces()) {
                for (NLAttributes attribute : nface.getObjects()) {
                    Rect rect = attribute.getBoundingRect();
                    if(attribute.getQuality() > q) q = attribute.getQuality();
                    System.out.println("Quality :: " + attribute.getQuality());
                    Log.d(TAG, "format_face_rect" + rect.left + " x " + rect.top + " x " + rect.right + " x " + rect.bottom);
                    if ((attribute.getRightEyeCenter().confidence > 0) || (attribute.getLeftEyeCenter().confidence > 0)) {
                        if (attribute.getRightEyeCenter().confidence > 0) {
                            Log.d(TAG, "format_first_eye_location_confidence" + " REx " + attribute.getRightEyeCenter().x + " REy " + attribute.getRightEyeCenter().y + " REc " + attribute.getRightEyeCenter().confidence);
                        }
                        if (attribute.getLeftEyeCenter().confidence > 0) {
                            Log.d(TAG, "format_second_eye_location_confidence" + " LEx " + attribute.getLeftEyeCenter().x + " LEy " + attribute.getLeftEyeCenter().y + " LEc " + attribute.getLeftEyeCenter().confidence);
                        }
                    }
                    if (attribute.getNoseTip().confidence > 0) {
                        Log.d(TAG, "format_location_confidence " + "Nx" + attribute.getNoseTip().x + "Ny" + attribute.getNoseTip().y + "Nc" + attribute.getNoseTip().confidence);
                    }
                    if (attribute.getMouthCenter().confidence > 0) {
                        Log.d(TAG, "format_location_confidence " + "Mx" + attribute.getNoseTip().x + "My" + attribute.getNoseTip().y + "Mc" + attribute.getNoseTip().confidence);
                    }
                }
            }
            if (status == NBiometricStatus.OK) {
                ret = subject.getTemplateBuffer().toByteArray();
            }else{
                Log.d(TAG,"Status "+ status.name());
            }
        } finally {
            if (subject != null) subject.dispose();
            if (face != null) face.dispose();
        }
        return ret;
    }
}
