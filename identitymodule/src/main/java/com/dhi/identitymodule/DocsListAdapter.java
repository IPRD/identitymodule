package com.dhi.identitymodule;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;
/**
 *
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class DocsListAdapter extends BaseAdapter implements ListAdapter {

    private final Context mContext;
    private List<SampleDoc> mDocs;

    public DocsListAdapter(Context context, List<SampleDoc> docsList) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.mContext = context;
        this.mDocs = docsList;
    }

    @Override
    public int getCount() {
        return this.mDocs.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mDocs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return Adapter.IGNORE_ITEM_VIEW_TYPE;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        }

        SampleDoc doc = this.mDocs.get(position);
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return this.mDocs.isEmpty();
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    /**
     * Add the given Task at the end of the list
     */
    public void add(SampleDoc doc) {
        this.mDocs.add(doc);
        this.notifyDataSetChanged();
    }

    /**
     * Put the give Task at specified position
     */
    public void set(int position, SampleDoc doc) {
        this.mDocs.set(position, doc);
        this.notifyDataSetChanged();
    }

    /**
     * Remove the Task at specified position
     */
    public void remove(int position) {
        this.mDocs.remove(position);
    }
}
