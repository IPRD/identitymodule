package com.dhi.identitymodule;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import static com.dhi.identitymodule.ImageUtils.readImageAndAlignExif;
/**
 * This class stores the current session properties - like name,age,ID,location string, photo string, etc for both HCW and patient.
 * It internally stores these properties as SharedPreferences.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class SessionProperties {
    public final int HCW_PIC = 0;
    public final int PATIENT_PIC = 1;
    public final int PATIENTSIG_PIC = 2;
    private static SessionProperties mOurInstance = null;
    private Context mCtx;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEdit;
    private AppType mAppType;

    synchronized public static SessionProperties getInstance(Context ctx) {
        if (mOurInstance == null) {
            mOurInstance = new SessionProperties(ctx.getApplicationContext());
        }
        return mOurInstance;
    }

    private SessionProperties(Context ctx) {
        mCtx = ctx;
        mPref = ctx.getSharedPreferences("IdMSession",Context.MODE_PRIVATE);
        mEdit = mPref.edit();
    }

    String getPhoneID() {
        StringBuffer phoneID = new StringBuffer();
        sessionState(phoneID,true,true,"PhoneID");
        return phoneID.toString();
    }

    void setPhoneID(String phoneID) {
        StringBuffer storedID = new StringBuffer();
        storedID.append(phoneID.toString());
        sessionState(storedID,false,true,"PhoneID");
    }

    AppType getAppType() {
        StringBuffer uType = new StringBuffer();
        sessionState(uType,true,true,"AppType");
        if (uType.toString().equals("GENERAL_APP"))
            return AppType.GENERAL_APP;
        else if (uType.toString().equals("BIOMETRIC_ONLY"))
            return AppType.BIOMETRIC_ONLY;
        else
            return AppType.NULL;
    }

    void setAppType(AppType appType) {
        Log.d("App type info","Setting value");
        StringBuffer uType = new StringBuffer();
        uType.append(appType.toString());
        sessionState(uType,false,true,"AppType");
        //mAppType = appType;
    }

    public String getHcwName() {
        StringBuffer hcwName = new StringBuffer();
        sessionState(hcwName,true,true,"Name");
        return hcwName.toString();
    }

    public void setHcwName(String hcwName) {
        StringBuffer hcw = new StringBuffer();
        hcw.append(hcwName);
        sessionState(hcw,false,true,"Name");
    }

    public String getPatName() {
        String exValue = "";
        return mPref.getString("PatName", exValue);
    }

    public void setPatName(String pName) {
        mEdit.putString("PatName",pName);
        mEdit.commit();
    }

    private void sessionState(StringBuffer userName, boolean getFn,boolean hcwUser,String stateVar) {
        String dataKey = "";
        //following state vars are not tied to hcw/patient and are common
        if (stateVar.equals("Location") || stateVar.equals("POS") || stateVar.equals("AppType") || stateVar.equals("PhoneID"))
            dataKey = stateVar;
        if (hcwUser)
            dataKey = "hcw"+stateVar;
        else
            dataKey = "patient"+stateVar;

        if (getFn) {
            String exValue = "";
            try {
               userName.append(mPref.getString(dataKey, exValue));
            } catch (ClassCastException e) {
                userName.append(exValue);
            }
        } else {
            mEdit.putString(dataKey,userName.toString());
            mEdit.commit();
        }
        return;
    }

    public String getHcwAge() {
        StringBuffer hcwAge = new StringBuffer();
        sessionState(hcwAge,true,true,"Age");
        return hcwAge.toString();
    }

    public void setHcwAge(int age) {
        StringBuffer hcwAge = new StringBuffer();
        hcwAge.append(age);
        sessionState(hcwAge,false,true,"Age");
    }

    public String getPatientAge() {
        StringBuffer patientAge = new StringBuffer();
        sessionState(patientAge,true,false,"Age");
        return patientAge.toString();
    }

    public void setPatientAge(int age) {
        StringBuffer patientAge = new StringBuffer();
        patientAge.append(age);
        sessionState(patientAge,false,false,"Age");
    }

    public long getEpochDOB(boolean isHCW)
    {
        StringBuffer id = new StringBuffer();
        sessionState(id,true,isHCW,"EpochDOB");
        if (id != null && id.length() > 0)
            return Long.parseLong(id.toString());
        else
            return 0L;
     }

    public void setEpochDOB(boolean isHCW,long epochDOB)
    {
        StringBuffer id = new StringBuffer();
        id.append(Long.toString(epochDOB));
        sessionState(id,false,isHCW,"EpochDOB");
    }

    public void setEpochDOB(boolean isHCW,String epochDOB)
    {
        StringBuffer id = new StringBuffer();
        id.append(epochDOB);
        sessionState(id,false,isHCW,"EpochDOB");
    }

    public String getID(boolean isHCW) {
        StringBuffer id = new StringBuffer();
        sessionState(id,true,isHCW,"ID");
        return id.toString();
    }


    public void setID(boolean isHCW,String id2add) {
        StringBuffer id = new StringBuffer();
        id.append(id2add);
        sessionState(id,false,isHCW,"ID");
    }



    public String getPatientName() {
        StringBuffer patientName = new StringBuffer();
        sessionState(patientName,true,false,"Name");
        return patientName.toString();
    }

    public void setPatientName(String patientName) {
        StringBuffer patient = new StringBuffer();
        patient.append(patientName);
        sessionState(patient,false,false,"Name");
    }

    public String getPatientGUID() {
        StringBuffer patientGUID = new StringBuffer();
        sessionState(patientGUID,true,false,"GUID");
        return patientGUID.toString();
    }

    public void setPatientGUID(String patientGUID) {
        StringBuffer patient = new StringBuffer();
        patient.append(patientGUID);
        sessionState(patient,false,false,"GUID");
    }

    public String getHcwGUID() {
        StringBuffer hcwGUID = new StringBuffer();
        sessionState(hcwGUID,true,true,"GUID");
        return hcwGUID.toString();
    }

    public void setHcwGUID(String hcwGUID) {
        StringBuffer hcw = new StringBuffer();
        hcw.append(hcwGUID);
        sessionState(hcw,false,true,"GUID");
    }

    public String getLocationStr() {
        StringBuffer location = new StringBuffer();
        sessionState(location,true,true,"Location");
        return location.toString();
    }

    public void setLocationStr(String locationStr) {
        StringBuffer location = new StringBuffer();
        location.append(locationStr);
        sessionState(location,false,true,"Location");
    }

    public void setHCWPhone(String hcwPhone) {
        StringBuffer phone = new StringBuffer();
        phone.append(hcwPhone);
        sessionState(phone,false,true,"Phone");
    }
    public String getHCWPhone() {
        StringBuffer phone = new StringBuffer();
        sessionState(phone,true,true,"Phone");
        return phone.toString();
    }

    public String getHCWCountryCode() {
        StringBuffer countryCode = new StringBuffer();
        sessionState(countryCode,true,true,"CountryCode");
        return countryCode.toString();
    }

    public void setHCWCountryCode(String hcwCountryCode) {
        StringBuffer countryCode = new StringBuffer();
        countryCode.append(hcwCountryCode);
        sessionState(countryCode,false,true,"CountryCode");
    }

    public void setHCWDOB(String hcwDOB) {
        StringBuffer dob = new StringBuffer();
        dob.append(hcwDOB);
        sessionState(dob,false,true,"DOB");
    }
    public String getHCWDOB() {
        StringBuffer dob = new StringBuffer();
        sessionState(dob,true,true,"DOB");
        return dob.toString();
    }

    public String getPatientPhone() {
        StringBuffer phone = new StringBuffer();
        sessionState(phone,true,false,"Phone");
        return phone.toString();
    }

    public void setPatientPhone(String patientPhone) {
        StringBuffer phone = new StringBuffer();
        phone.append(patientPhone);
        sessionState(phone,false,false,"Phone");
    }

    public String getPatientCountryCode() {
        StringBuffer countryCode = new StringBuffer();
        sessionState(countryCode,true,false,"CountryCode");
        return countryCode.toString();
    }

    public void setPatientCountryCode(String patientCountryCode) {
        StringBuffer countryCode = new StringBuffer();
        countryCode.append(patientCountryCode);
        sessionState(countryCode,false,false,"CountryCode");
    }

    public String getPatientDOB() {
        StringBuffer dob = new StringBuffer();
        sessionState(dob,true,false,"DOB");
        return dob.toString();
    }

    public void setPatientDOB(String patientDOB) {
        StringBuffer dob = new StringBuffer();
        dob.append(patientDOB);
        sessionState(dob,false,false,"DOB");
    }

    public String getHCWGender() {
        StringBuffer gender = new StringBuffer();
        sessionState(gender,true,true,"Gender");
        return gender.toString();
    }

    public void setHCWGender(String hcwGender) {
        StringBuffer gender = new StringBuffer();
        gender.append(hcwGender);
        sessionState(gender,false,true,"Gender");
    }

    public String getPatientGender() {
        StringBuffer gender = new StringBuffer();
        sessionState(gender,true,false,"Gender");
        return gender.toString();
    }

    public void setPatientGender(String patientGender) {
        StringBuffer gender = new StringBuffer();
        gender.append(patientGender);
        sessionState(gender,false,false,"Gender");
    }

    public String getHCWSkill() {
        StringBuffer skill = new StringBuffer();
        sessionState(skill,true,true,"Skill");
        return skill.toString();
    }

    public void setHCWSkill(String hcwSkill) {
        StringBuffer skill = new StringBuffer();
        skill.append(hcwSkill);
        sessionState(skill,false,true,"Skill");
    }

    public String getPatientSkill() {
        StringBuffer skill = new StringBuffer();
        sessionState(skill,true,false,"Skill");
        return skill.toString();
    }

    public void setPatientSkill(String patientSkill) {
        StringBuffer skill = new StringBuffer();
        skill.append(patientSkill);
        sessionState(skill,false,false,"Skill");
    }

    public String getHCWConsent() {
        StringBuffer consent = new StringBuffer();
        sessionState(consent,true,true,"Consent");
        return consent.toString();
    }

    public void setHCWConsent(String hcwConsent) {
        StringBuffer consent = new StringBuffer();
        consent.append(hcwConsent);
        sessionState(consent,false,true,"Consent");
    }

    public String getPatientConsent() {
        StringBuffer consent = new StringBuffer();
        sessionState(consent,true,false,"Consent");
        return consent.toString();
    }

    public void setPatientConsent(String patientConsent) {
        StringBuffer consent = new StringBuffer();
        consent.append(patientConsent);
        sessionState(consent,false,false,"Consent");
    }

    public String getPOS() {
        StringBuffer pos = new StringBuffer();
        sessionState(pos,true,false,"POS");
        return pos.toString();
    }

    public void setPOS(String POS) {
        StringBuffer pos = new StringBuffer();
        pos.append(POS);
        sessionState(pos,false,false,"POS");
    }

    public String getHcwRevision() {
        StringBuffer revision = new StringBuffer();
        sessionState(revision,true,true,"Revision");
        return revision.toString();
    }

    public void setHcwRevision(String hcwRevision) {
        StringBuffer revision = new StringBuffer();
        revision.append(hcwRevision);
        sessionState(revision,false,true,"Revision");
    }

    public String getPatientRevision() {
        StringBuffer revision = new StringBuffer();
        sessionState(revision,true,false,"Revision");
        return revision.toString();
    }

    public void setPatientRevision(String patientRevision) {
        StringBuffer revision = new StringBuffer();
        revision.append(patientRevision);
        sessionState(revision,false,false,"Revision");
    }

    public String getHCWPhotoStr() {
        StringBuffer photo = new StringBuffer();
        sessionState(photo,true,true,"Photo");
        return photo.toString();
    }

    public void setHCWPhotoStr(String hcwPhotoStr) {
        StringBuffer photo = new StringBuffer();
        photo.append(hcwPhotoStr);
        sessionState(photo,false,true,"Photo");
    }

    public String getPatientPhotoStr() {
        StringBuffer photo = new StringBuffer();
        sessionState(photo,true,false,"Photo");
        return photo.toString();
    }

    public void setPatientPhotoStr(String patientPhotoStr) {
        StringBuffer photo = new StringBuffer();
        photo.append(patientPhotoStr);
        sessionState(photo,false,false,"Photo");
    }

    public String getSessionCtx() {
        StringBuffer sessionCtx = new StringBuffer();
        sessionState(sessionCtx,true,false,"SessionContext");
        return sessionCtx.toString();
    }

    public void setSessionCtx(String sessionCtx) {
        StringBuffer ctx = new StringBuffer();
        ctx.append(sessionCtx);
        sessionState(ctx,false,false,"SessionContext");
    }

    /**
     *
     * update my properties for user
     * @param isHCW
     * @param properties2update
     */
    public void updateMyPropertiesForUser(boolean isHCW, Map<String,String> properties2update)
    {
        String name = "";
        String phone = "";
        String countryCode = "";
        String dob = "";
        String consent = "";
        String photoStr = "";
        String locationStr = "";
        String posStr = "";
        String uid = "";

        String guid = properties2update.get("guid");
        String gender = properties2update.get("gender");
        String skill = properties2update.get("skill");
        name = properties2update.get("name");
        phone = properties2update.get("phone");
        countryCode = properties2update.get("countryCode");
        dob = properties2update.get("DOB");
        consent = properties2update.get("consent");
        photoStr = properties2update.get("photo");
        locationStr = properties2update.get("location");
        posStr = properties2update.get("POS");
        uid = properties2update.get("uid");

        if (isHCW)
        {
            //hcw. set all hcw properties
            //update only those props that are in the map.
            if (name != null)
                setHcwName(name);
            if (phone != null)
                setHCWPhone(phone);
            if (countryCode != null)
                setHCWCountryCode(countryCode);
            if (dob != null)
                setHCWDOB(dob);
            if (consent != null)
                setHCWConsent(consent);
            if (photoStr != null)
                setHCWPhotoStr(photoStr);
            if (locationStr != null)
                setLocationStr(locationStr);
            if (posStr != null)
                setPOS(posStr);
            if (gender != null)
                setHCWGender(gender);
            if (skill != null)
                setHCWSkill(skill);
            if (guid != null)
                setHcwGUID(guid);
        }
        else
        {
            if (name != null)
                setPatientName(name);
            if (phone != null)
                setPatientPhone(phone);
            if (countryCode != null)
                setPatientCountryCode(countryCode);
            if (dob != null)
                setPatientDOB(dob);
            if (consent != null)
                setPatientConsent(consent);
            if (photoStr != null)
                setPatientPhotoStr(photoStr);
            if (locationStr != null)
                setLocationStr(locationStr);
            if (posStr != null)
                setPOS(posStr);
            if (gender != null)
                setPatientGender(gender);
            if (skill != null)
                setPatientSkill(skill);
            if (guid != null)
                setPatientGUID(guid);
        }
        if (uid != null)
            setID(isHCW,uid);
    }

    /**
     * For getting photo
     * @param photoType
     * @return : bitmap
     */
    public Bitmap getPhoto(int photoType) {
        final File photo;
        Bitmap picBmp;
        switch (photoType) {
            case HCW_PIC:
  //              photo = new File(Environment.getExternalStorageDirectory(),  mCtx.getResources().getString(R.string.HCWPicName));
                picBmp = readImageAndAlignExif(Environment.getExternalStorageDirectory()+"/"+mCtx.getResources().getString(R.string.HCWPicName));
                break;
            case PATIENT_PIC:
  //              photo = new File(Environment.getExternalStorageDirectory(), mCtx.getResources().getString(R.string.patientPicName));
                picBmp = readImageAndAlignExif(Environment.getExternalStorageDirectory()+"/"+mCtx.getResources().getString(R.string.patientPicName));
                break;
            case PATIENTSIG_PIC:
                picBmp = readImageAndAlignExif(Environment.getExternalStorageDirectory()+"/"+mCtx.getResources().getString(R.string.signPicName));
                break;
            default:
                return null;
        }
            return picBmp;
    }

    private boolean clearPhoto(int photoType) {
        final File photo;
        switch (photoType) {
            case HCW_PIC:
                photo = new File(Environment.getExternalStorageDirectory(), "HCWPic.jpg");
                break;
            case PATIENT_PIC:
                photo = new File(Environment.getExternalStorageDirectory(), "PatientPic.jpg");
                break;
            case PATIENTSIG_PIC:
                photo = new File(Environment.getExternalStorageDirectory(), "sign.png");
                break;
            default:
                return false;
        }
        photo.delete();
        return true;
    }

    /**
     * clear patient
     */
    public void clearPatient() {
        clearData(false);
    }

    /**
     * clear the HCW
     */
    public void clearHCW() {
       clearData(true);
    }

    private void clearData(boolean userType) {
        StringBuffer val = new StringBuffer();
        val.append("");
        sessionState(val,false,userType,"Name");
        sessionState(val,false,userType,"GUID");
        sessionState(val,false,userType,"Phone");
        sessionState(val,false,userType,"CountryCode");
        sessionState(val,false,userType,"DOB");
        sessionState(val,false,userType,"Gender");
        sessionState(val,false,userType,"Skill");
        sessionState(val,false,userType,"Location");
        sessionState(val,false,userType,"Photo");
        sessionState(val,false,userType,"POS");
        sessionState(val,false,userType,"Consent");
    }

    /**
     *
     */
    public void clearAll() {
        clearHCW();
        clearPatient();
    }


}
