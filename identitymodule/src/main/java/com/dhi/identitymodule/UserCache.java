package com.dhi.identitymodule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * This class implements an in memory data cache.
 * At startup, it reads everything from the sqlite db and during every add/delete of a user, it gets updated too
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class UserCache {
   // Map<String, List<String>> paramGUIDMap; //ex: this may have names and a list of guids against each.
    Map<String,Map<String,Set<String>>> mParamsNameMap; //this could be a map of say "Name" and a paramGUIDMap against it.
    Map<String,Map<String,String>> mGuidValuesMap;
    //Ex: if someone wants to search for all hcw users with name "Raj".
    //then first search mParamsNameMap for "HCWName". This will give a paramGUIDMap that has all names with their resepctive guids for each.
    //ex; there is an entry named Raj. However, there are multiple users named Raj with different guids. now, this user named Raj will have all the guids listed agsinst him.
    //(to figure out exactly which user, u have to use additional search criteria.

    private static UserCache mCacheInstance = null;
    private UserCache() {
       // paramGUIDMap = new HashMap<String,List<String>>();
        mParamsNameMap = new HashMap<String,Map<String,Set<String>>>();
        mGuidValuesMap = new HashMap<String,Map<String,String>>();
    }

    synchronized public static UserCache getCache() {
        if (mCacheInstance == null)
            mCacheInstance = new UserCache();
        return mCacheInstance;
    }

    /**
     * This method returns a list of all the GUID's from the cache for a specific id Type
     * @param idType
     * @return
     */
    public List<String> getAllIds(int idType)
    {
        List<String> guids = new ArrayList<String>();
        for (Map.Entry<String,Map<String,String>> entry: mGuidValuesMap.entrySet())
        {
            if (IdentityModule.getGUIDType(entry.getKey()) == idType)
                guids.add(entry.getKey());
        }
        return guids;
    }

    /**
     * This method returns a list of all the GUID's
     * @param idTypes
     * @return
     */
    public List<String> getAllIds(Set<Integer> idTypes)
    {
        List<String> guids = new ArrayList<String>();
        for (Map.Entry<String,Map<String,String>> entry: mGuidValuesMap.entrySet())
        {
            if (idTypes.contains(IdentityModule.getGUIDType(entry.getKey())))
                guids.add(entry.getKey());
        }
        return guids;
    }

    /**
     * This method adds data to cache.
     * For this, pass the following parameters -
     * userType: HCW/Patient
     * param2add: the parameter to add like name,ID
     * paramVal: the value for the parameter. Ex: name could be "Ganesh"
     * guid: the guid of this user.
     * @param userType
     * @param param2add
     * @param paramVal
     * @param guid
     */
    public void addDataToCache(String userType,String param2add,String paramVal,String guid) {
        String paramKey = userType+param2add;//do this by concatenating usertype and param2add - ex: HCW+name = HCWname
        Map<String,Set<String>> paramGUIDMap = mParamsNameMap.get(paramKey);
        if (paramGUIDMap != null)
        {
            //this key already is present
            if (paramGUIDMap.containsKey(paramVal))
            {
                paramGUIDMap.get(paramVal).add(guid);
                mParamsNameMap.put(paramKey,paramGUIDMap);
            }
            else
            {
                //ex: the hcwname "Raj" is not present at all. then add
                Set<String> guids = new HashSet<String>();
                guids.add(guid);
                paramGUIDMap.put(paramVal,guids);
                mParamsNameMap.put(paramKey,paramGUIDMap);
            }

        }
        else
        {
            //add this param key
            Set<String> guids = new HashSet<String>();
            guids.add(guid);
            Map<String,Set<String>> localParamGUIDMap = new HashMap<String,Set<String>>();
            localParamGUIDMap.put(paramVal,guids);
            mParamsNameMap.put(paramKey,localParamGUIDMap);
        }
        Map<String,String> valuesForGuidMap = mGuidValuesMap.get(guid);
        if (valuesForGuidMap == null)
        {
            //this is a new guid to add. add with params
            Map<String,String> valueForGUID = new HashMap<String,String>();
            valueForGUID.put(paramKey,paramVal);
            mGuidValuesMap.put(guid,valueForGUID);
        }
        else
        {
            //check if the given param and value is found. update if required.
            valuesForGuidMap.put(paramKey,paramVal);
            mGuidValuesMap.put(guid,valuesForGuidMap);
        }
        return;
    }

    /**
     * This method deletes a user based on the inputs given from the cache.
     * For this, pass the following parameters -
     * userType: HCW/Patient
     * param2del: the parameter to add like name,ID
     * paramVal: the value for the parameter. Ex: name could be "Ganesh"
     * resGUID: the guid of this user.
     * @param userType
     * @param param2del
     * @param paramVal
     * @param resGUID
     * @return
     */
    public boolean deleteUserFromParamsMap(String userType,String param2del,String paramVal,String resGUID) {
        String paramKey = userType+param2del;
        Map<String,Set<String>> paramGUIDMap = mParamsNameMap.get(paramKey);
        //search this above map if a specific value of paramKey exists.
        if (paramGUIDMap == null)
            return false;
        if (!paramGUIDMap.containsKey(paramVal))
            return false;
        else
        {
            Set<String> guidList = paramGUIDMap.get(paramVal);
            if (!guidList.contains(resGUID))
                return false;
            paramGUIDMap.get(paramVal).remove(resGUID);
            if (guidList.size() == 1)
            {
                //this means only one guid for this value. so the key can be removed.
                paramGUIDMap.remove(paramVal);
            }
            mParamsNameMap.remove(paramKey);
            mParamsNameMap.put(paramKey,paramGUIDMap);
        }
        return true;
    }

    /**
     * This method deletes a GUID from the GUID's map.
     * @param guid
     * @return
     */
    public boolean deleteFromGUIDMap(String guid) {
        if (mGuidValuesMap.remove(guid) == null)
            return false;
        return true;
    }

    /**
     * This method searches the cache for user based on the search inputs.
     * @param userType
     * @param param2search
     * @param paramVal
     * @return
     */
    public Set<String> searchForUser(String userType,String param2search,String paramVal) {
        String paramKey = userType+param2search;//do this by concatenating usertype and param2add - ex: HCW+name = HCWname
        Map<String,Set<String>> paramGUIDMap = mParamsNameMap.get(paramKey);
        Set<String> guidResult = new HashSet<String> ();
        if (param2search.equals("age"))
        {
            //approach dob differently. we need to do approximate matching
            if (paramGUIDMap != null) {
                for (Map.Entry<String, Set<String>> entry : paramGUIDMap.entrySet()) {
                    System.out.println("Age entered during login: "+paramVal);
                    System.out.println("DOB in days from DB: "+entry.getKey());
                    if (IdentityUtils.validateAge(Long.parseLong(paramVal), Long.parseLong(entry.getKey())))
                        guidResult = entry.getValue();
                }
            }
        }
        else
        {
            if (paramGUIDMap != null)
            {
                //the param we are searching for exists.
                if (paramGUIDMap.containsKey(paramVal))
                {
                    guidResult = paramGUIDMap.get(paramVal);
                }
            }
        }
        return guidResult;
    }

    public Set<String> searchUser(String userType, String param2search, String paramVal) {
        String paramKey = userType+param2search;//do this by concatenating usertype and param2add - ex: HCW+name = HCWname
        Map<String,Set<String>> paramGUIDMap = mParamsNameMap.get(paramKey);
        Set<String> guidResult = new HashSet<String>();
        if (param2search.equals("age"))
        {
            //approach dob differently. we need to do approximate matching
            for (Map.Entry<String,Set<String>> entry : paramGUIDMap.entrySet())
            {
                if (IdentityUtils.validateAge(Long.parseLong(paramVal),Long.parseLong(entry.getKey())))
                    guidResult = entry.getValue();
            }
        }
        else
        {
            if (paramGUIDMap != null)
            {
                //the param we are searching for exists.
                if (paramGUIDMap.containsKey(paramVal))
                {
                    guidResult = paramGUIDMap.get(paramVal);
                }
            }
        }
        return guidResult;
    }

    /**
     *
     * Search for user parameter from given parameter (user type, GUID,  parameter to be search )
     * @param userType
     * @param guid
     * @param param2search
     * @return : string
     */
    public String searchForUserParam(String userType,String guid,String param2search)
    {
        String paramKey = userType+param2search;//do this by concatenating usertype and param2add - ex: HCW+name = HCWname
        Map<String,String> paramsMap = mGuidValuesMap.get(guid);
        if (paramsMap == null)
            return null;
        return paramsMap.get(paramKey);
    }
}
