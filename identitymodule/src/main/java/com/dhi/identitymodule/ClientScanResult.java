package com.dhi.identitymodule;
/**
 * This class is used to represent a sample client from a wifi scan.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class ClientScanResult {

    private String mIpAddr;

    private String mHWAddr;

    private String mDevice;

    private boolean mIsReachable;

    public ClientScanResult(String mIpAddr, String mHWAddr, String mDevice, boolean isReachable) {
        super();
        mIpAddr = mIpAddr;
        mHWAddr = mHWAddr;
        mDevice = mDevice;
        this.setReachable(isReachable);
    }

    public String getIpAddr() {
        return mIpAddr;
    }

    public void setIpAddr(String mIpAddr) {
        mIpAddr = mIpAddr;
    }

    public String getmHWAddr() {
        return mHWAddr;
    }

    public void setmHWAddr(String mHWAddr) {
        mHWAddr = mHWAddr;
    }

    public String getmDevice() {
        return mDevice;
    }

    public void setmDevice(String mDevice) {
        mDevice = mDevice;
    }

    public void setReachable(boolean isReachable) {
        this.mIsReachable = isReachable;
    }

    public boolean misReachable() {
        return mIsReachable;
    }
}
