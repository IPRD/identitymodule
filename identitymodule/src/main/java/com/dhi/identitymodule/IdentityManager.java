package com.dhi.identitymodule;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Looper;
import android.se.omapi.Session;
import android.util.Log;
import android.widget.Toast;

import com.cloudant.sync.documentstore.DocumentNotFoundException;
import com.cloudant.sync.documentstore.DocumentRevision;
import com.cloudant.sync.documentstore.DocumentStoreException;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.PhantomReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
/**
 * This class consists of functions that can be used by apk's using this library
 * These functions can be used to do functions like :
 * AddPerson
 * ConnectToIdentity
 * HandlePOS
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class IdentityManager {
    private String mConnectCtx;
    private String mAPIVersion;
    //private Map<String,SessionProperties> mCtxMap;
    private Set<String> mCtxMap;
    private static IdentityManager mIdmInstance = null;
    private SmsService mSvc;
    private Context mCtx;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEdit;


    private IdentityManager() {
        mAPIVersion = "API_1";
        mConnectCtx = "";
        mSvc = new SmsService();
       // mCtxMap = new HashMap<String,SessionProperties>();
        mCtxMap = new HashSet<String>();
    }

    /**
     *  This method initializes SharedPreferences for IdM.
     *  We also read the conents of the "ContextString" variable here and store internally in a map
     *  Call this as as you initialize IdentityManager.
     * @param ctx
     */
    public void initPersistentStorage(Context ctx)
    {
        mCtx = ctx;
        mPref = ctx.getSharedPreferences("IdM",Context.MODE_PRIVATE);
        mEdit = mPref.edit();
        StringBuffer ctxStr = new StringBuffer();
        if (readFromInStream(ctxStr))
            mCtxMap.add(ctxStr.toString());
        if (SessionProperties.getInstance(mCtx).getPhoneID() == null || SessionProperties.getInstance(mCtx).getPhoneID().length() == 0)
        {
            StringBuffer devID = new StringBuffer();
            if (getAppDeviceID(devID))
            {
                Log.d("Device ID is: ",devID.toString());
                SessionProperties.getInstance(mCtx).setPhoneID(devID.toString());
            }
            else
                setAppDeviceID();
        }
        else
            Log.d("Phone ID from session properties: ",SessionProperties.getInstance(mCtx).getPhoneID().toString());

    }

    private boolean getAppDeviceID(StringBuffer deviceID)
    {
        String exValue = "";
        try {
            String devIDFromPref = IdentityModule.getInstance(mCtx).getPhoneID();
            if (devIDFromPref != null && devIDFromPref.length() > 0) {
                deviceID.append(devIDFromPref);
                return true;
            }
            else
                return false;
        } catch (ClassCastException e) {
            return false;
        }
    }

    private void setAppDeviceID()
    {
        String phoneID = UUID.randomUUID().toString();
        IdentityModule.getInstance(mCtx).addGeneralRecord(phoneID);
        SessionProperties.getInstance(mCtx).setPhoneID(phoneID);
    }

    public boolean isValidContext(String inCtx) {
        return (mCtxMap.contains(inCtx));
    }

    public static IdentityManager getIDMInstance() {
        if (mIdmInstance == null)
            mIdmInstance = new IdentityManager();
        return mIdmInstance;
    }

    private boolean saveToOutStream(String ctx2save)
    {
        mEdit.putString("ContextString",ctx2save.toString());
        mEdit.commit();
        return true;
    }

    private boolean readFromInStream(StringBuffer ctxFromCache)
    {
        String exValue = "";
        try {
            ctxFromCache.append(mPref.getString("ContextString", exValue));
            return true;
        } catch (ClassCastException e) {
            ctxFromCache.append(exValue);
            return false;
        }
    }

     /**
     * Connect to identity. Return the current API and the current context value.
      * here,we need to pass the appType as a parameter as well. used to indicate if it is general app or biometric only
     * This function should be called first before calling any of the other API's
     * The context value should be saved by the calling app and used in subsequent API's.
     * @param appCtx
     * @param currCtx
     * @param currAPI
     * @param appType
     */

    public void ConnectToIdentity(Context appCtx,StringBuffer currCtx,StringBuffer currAPI,AppType appType) {
            mConnectCtx = UUID.randomUUID().toString();
            //clear existing map value. (Ideally this map should support multiple context strings.
            // In this release, we are supporting just one though. Will implement support for multiple strings later
            mCtxMap.clear();
            mCtxMap.add(mConnectCtx);
            saveToOutStream(mConnectCtx);
            currCtx.append(mConnectCtx);
            currAPI.append(mAPIVersion);
            SessionProperties.getInstance(appCtx).setSessionCtx(mConnectCtx);
            Log.d("App type value",appType.name());
            SessionProperties.getInstance(appCtx).setAppType(appType);
    }

    /**
     * Connect to identity. Return the current API and the current context value.
     * Here we assume this is a general app. For biometric build please call the above Connect function. (function below internally calls that)
     * This function should be called first before calling any of the other API's
     * The context value should be saved by the calling app and used in subsequent API's.
     * @param appCtx
     * @param currCtx
     * @param currAPI
     */

    private void ConnectToIdentity(Context appCtx,StringBuffer currCtx,StringBuffer currAPI) {
        ConnectToIdentity(appCtx,currCtx,currAPI,AppType.GENERAL_APP);
    }

    /**
     *
     * API to check if the user is present based on the parameters provided.
     * If not present, then add.
     * VIMP: make sure the
     * @param currCtx
     * @param currGUID
     * @param appCtx
     * @param name
     * @param uid
     * @param govID
     * @param age
     * @param countryCode
     * @param phone
     * @param dob
     * @param gender
     * @param skill
     * @param location
     * @param personType
     * @param photo
     * @param consent
     * @param POS
     * @param biometricJSON
     * @param guid
     * @param missingParams
     * @return
     */
    public int AddPerson(String currCtx, String currGUID, Context appCtx, String name, String uid, String govID, int age, String countryCode, String phone, String dob, String gender, String skill, String location, String personType, Bitmap photo,String consent,String POS,String biometricJSON,StringBuffer guid,StringBuffer missingParams)
    {
        if (!isValidContext(currCtx))
          return -3;
        Log.d("Name to add: ",name.toString());
        Log.d("uid to add: ",uid.toString());
        if (currGUID != null)
            Log.d("GUID to add: ",currGUID.toString());
        else
            Log.e("GUID Error: ","GUID is null - not right");
        if (personType.equals("HCW"))
            currGUID = SessionProperties.getInstance(appCtx).getHcwGUID();
        else
            currGUID = SessionProperties.getInstance(appCtx).getPatientGUID();
        Log.d("Idm_AddPerson","Entered this function"+currGUID);
        int addRet = 0;
        String photoStr = "";
        DocumentRevision docRev = null;
        Map<String,String> searchParams = new HashMap<String,String>();
        searchParams.put("indType", personType);
        if (currGUID != null && currGUID.length() > 0) {
            Log.d("IdM-AddPerson","Searching user by guid");
            searchParams.put("docId", currGUID);
            try {
                docRev = IdentityModule.getInstance(appCtx).getDeviceStore().getDocumentStore().database().read(currGUID);
                addRet = 1;
                Log.d("IdM-AddPerson","User found using guid");
                guid.append(currGUID);
            } catch (DocumentNotFoundException e) {
                e.printStackTrace();
                Log.e("IdM-AddPerson",currGUID+e.toString());
                return -2;
            } catch (DocumentStoreException e) {
                e.printStackTrace();
                Log.e("IdM-AddPerson",currGUID+e.toString());
                return -2;
            }
        }
        else {
            //first clear existing session parameters
            //if HCW, clear HCW and patient. For patient, just patient.
            if (personType.equals("HCW"))
            {
                SessionProperties.getInstance(appCtx).clearAll();
            }
            else if (personType.equals("Patient"))
                SessionProperties.getInstance(appCtx).clearPatient();

            Log.d("IdM-AddPerson","Searching user by parameters");
            if (name.length() != 0)
                searchParams.put("name", name);
            if (phone.length() != 0)
                searchParams.put("phone", phone);
            if (countryCode.length() != 0)
                searchParams.put("countryCode", countryCode);
            if (dob.length() != 0)
                searchParams.put("DOB", dob);
            if (age > 0)
                searchParams.put("age",Integer.toString(age));
            if (uid.length() != 0)
                searchParams.put("uid",uid);
            if (govID.length() != 0)
                searchParams.put("govID",govID);
            if (biometricJSON != null && biometricJSON.length() != 0)
                searchParams.put("BiometricData",biometricJSON);
            addRet = IdentityModule.getInstance(appCtx).isUserPresent(name,searchParams,guid,missingParams);
        }

        if (addRet == 1)
        {

            if (guid.toString().length() > 0)
            {
                String userIndicator = "";
                if (personType.equals("HCW"))
                    userIndicator = "HCW_";
                else
                    userIndicator = "PAT_";
                String userGUID = userIndicator+guid.toString().substring(IdentityModule.GUID_INDICATOR_SIZE);
                Log.d("Bio-auth","Found user with GUID: "+userGUID);
            }
            else {
                Log.d("Bio-auth","User not found");
                addRet = 0;
            }
        }

        //if this return value is 0, then it means user not found. so add. if -1,more details are needed.
        //if return value is 1, then user is present already. update doc.
        if (addRet == 0) {
            Log.d("IdM-AddPerson","User not found");
            //from search params, get the biometric json
            //from this json, get the fingerprint template

            String bioJSON = searchParams.get("BiometricData");

            //if guid was passed in, then we should not add another user.
            //if not, then it means we tried searching by other params  - mostly during initial login and couldnt find a user.
            if (currGUID == null || currGUID.length() == 0)
            {
                Map<String,String> updateParams = new HashMap<String,String>();
                //here check if biometric info was passed in for the user at all.
                //if not done, then we should not add the user at this stage.
                if (!IdentityModule.getInstance(appCtx).addIndividual(guid, name, uid, govID, age, phone, countryCode, dob, personType, gender, skill,bioJSON))
                    return -1;
                updateParams.put("guid",guid.toString());
                //added user. now set MyProperties for this session.
                boolean isHCW = true;
                if (personType.equals("Patient"))
                    isHCW = false;
                updateParams.putAll(searchParams);
                if (name != null && name.length() != 0)
                    updateParams.put("name", name);
                if (phone != null && phone.length() != 0)
                    updateParams.put("phone", phone);
                if (countryCode != null && countryCode.length() != 0)
                    updateParams.put("countryCode", countryCode);
                if (dob != null && dob.length() != 0)
                    updateParams.put("DOB", dob);
                SessionProperties.getInstance(appCtx).updateMyPropertiesForUser(isHCW,updateParams);
            } else
            {
                //this means the user is not present,but a guid has been generated.
                //usually happens if the user skips biometric authentication. hence doesnt get added to db
                return -2;
            }

        } else if (addRet == 1) {
            Log.d("IdM-AddPerson","User found");
            //here user is found.
            //some params to be updated based on the values provided
            //if a value is not provided get it by querying the bckend using the guid
            if (photo != null)
            {
                Log.d("Idm-AddPerson","About to set photo");
                photoStr = ProofOfService.getStringImageBase64(photo);
                if (personType.equals("HCW")) {
                    SessionProperties.getInstance(appCtx).setHCWPhotoStr(photoStr);
                    Log.d("IdM-AddPerson","Set hcw photo");
                }
                else if (personType.equals("Patient")) {
                    SessionProperties.getInstance(appCtx).setPatientPhotoStr(photoStr);
                    Log.d("IdM-AddPerson", "Set patient photo");
                }
            }
         /*   else
            {
                if (photoString != null || photoString.length() > 0) {
                    if (personType.equals("HCW")) {
                        SessionProperties.getInstance(appCtx).setHCWPhotoStr(photoString);
                    } else if (personType.equals("Patient"))
                        SessionProperties.getInstance(appCtx).setPatientPhotoStr(photoString);
                    photoStr = photoString;
                }
            } */
            Map<String,String> updateParams = new HashMap<String,String>();
            try {
                if (docRev == null)
                    docRev = IdentityModule.getInstance(appCtx).getDeviceStore().getDocumentStore().database().read(guid.toString());
            } catch (DocumentNotFoundException e) {
                e.printStackTrace();
                Log.e("IdM Error",e.toString());
                return -2;
            } catch (DocumentStoreException e) {
                e.printStackTrace();
                Log.e("IdM Error",e.toString());
                return -2;
            }
            updateParams.putAll(searchParams);
            updateParams.put("indType",personType);

            if (name != null && name.length() != 0)
                updateParams.put("name", name);
            else
                updateParams.put("name",(String) docRev.getBody().asMap().get("name"));
            if (phone != null && phone.length() != 0)
                updateParams.put("phone", phone);
            else
                updateParams.put("phone",(String) docRev.getBody().asMap().get("phone"));
            if (countryCode != null && countryCode.length() != 0)
                updateParams.put("countryCode", countryCode);
            else
                updateParams.put("countryCode",(String) docRev.getBody().asMap().get("countryCode"));
            if (dob != null && dob.length() != 0)
                updateParams.put("DOB", dob);
            else
                updateParams.put("DOB",(String) docRev.getBody().asMap().get("DOB"));

            if (gender != null && gender.length() > 0)
                updateParams.put("gender",gender);
            else
                updateParams.put("gender",(String) docRev.getBody().asMap().get("gender"));
            if (uid != null && uid.length() > 0)
                updateParams.put("uid",uid);
            else
                updateParams.put("uid",(String) docRev.getBody().asMap().get("uid"));
            if (skill != null && skill.length() > 0)
                updateParams.put("skill",skill);
            else
                updateParams.put("skill",(String) docRev.getBody().asMap().get("skill"));
            if (location != null && location.length() > 0)
                updateParams.put("location",location);
            else
                updateParams.put("location",(String) docRev.getBody().asMap().get("location"));
            if (consent != null && consent.length() > 0)
                updateParams.put("consent",consent);
            else
                updateParams.put("consent",(String) docRev.getBody().asMap().get("consent"));
            if (POS != null && POS.length() > 0)
                updateParams.put("POS",POS);
            else
                updateParams.put("POS",(String) docRev.getBody().asMap().get("POS"));
            if (photoStr != null && photoStr.length() > 0)
                updateParams.put("photo",photoStr);
            else
                updateParams.put("photo",(String) docRev.getBody().asMap().get("photo"));
            if (age > 0)
                updateParams.put("age",Integer.toString(age));
            else
                updateParams.put("age",(String) docRev.getBody().asMap().get("age"));
            List<String> existingTx = (List<String>) docRev.getBody().asMap().get("Transactions");


            boolean isHCW = true;
            if (personType.equals("Patient"))
                isHCW = false;
            SessionProperties.getInstance(appCtx).updateMyPropertiesForUser(isHCW,updateParams);
            if (personType.equals("HCW"))
            {
                SessionProperties.getInstance(appCtx).setHcwGUID(guid.toString());
                SessionProperties.getInstance(appCtx).setHcwRevision((String) docRev.getRevision());
            }
            else if (personType.equals("Patient")) {
                Log.d("After patient update","PatientName: "+SessionProperties.getInstance(appCtx).getPatientName());
                SessionProperties.getInstance(appCtx).setPatientGUID(guid.toString());
                SessionProperties.getInstance(appCtx).setPatientRevision((String) docRev.getRevision());
            }
            else
                return -2;
            if (!IdentityModule.getInstance(appCtx).updateIndividual(updateParams,"Update"))
                return -2;
            else
                Log.d("Update log","Updated individual");
            //now that the individual has been updated, make sure u add a transaction as well for consent, POS addition.
            //for user search and user login (both hcw and patient), this is already taken care of in the code.
            if (consent != null && consent.length() > 0)
            {
                String consentTxt = personType+" Consent";
                String hGUID = "";
                String pGUID = "";

                if (personType.equals("HCW"))
                {
                    hGUID = SessionProperties.getInstance(appCtx).getHcwGUID();
                    pGUID = "";
                } else if (personType.equals("Patient"))
                {
                    hGUID = SessionProperties.getInstance(appCtx).getHcwGUID();
                    pGUID = SessionProperties.getInstance(appCtx).getPatientGUID();
                }
                else
                    return -2;
                if (!IdentityModule.getInstance(appCtx).addTransaction(consentTxt, hGUID, pGUID, consent))
                    Log.d("Consent transaction", "Fail");
                else
                    Log.d("Consent transaction", "Pass");
            }
            if (POS != null && POS.length() > 0)
            {
                if (!IdentityModule.getInstance(appCtx).addTransaction("Proof of Service",SessionProperties.getInstance(appCtx).getHcwGUID(),SessionProperties.getInstance(appCtx).getPatientGUID(),POS))
                    Log.d("POS transaction","Fail");
                else
                    Log.d("POS transaction","Pass");
            }
        }
        else
            Log.d("IdM-AddPerson","addRet value: "+Integer.toString(addRet));

        return addRet;

    }


    /**
     *
     * API for initiating an OTP to be sent to a phone number for verifiction.
     * @param appCtx
     * @param currCtx
     * @param guid
     * @param authType
     * @param personType
     * @return
     */
    public boolean AuthenticatePerson(Context appCtx,String currCtx,String guid,String authType,String personType) {
        if (!isValidContext(currCtx))
          return false;
        String phone = "";
        String countryCode = "";
        if (personType.equals("HCW"))
        {
            phone = SessionProperties.getInstance(appCtx).getHCWPhone();
            countryCode = SessionProperties.getInstance(appCtx).getHCWCountryCode();
        } else if (personType.equals("Patient")){
            phone = SessionProperties.getInstance(appCtx).getPatientPhone();
            countryCode = SessionProperties.getInstance(appCtx).getPatientCountryCode();
        } else
            return false;

        StringBuffer rspStr  = new StringBuffer();
        if (authType == "SMS")
        {
            return mSvc.sendVerificationMsg(phone,countryCode,mSvc.getAPIKey(),"sms",rspStr);
        }
        return true;
    }


    /**
     *
     * API for verifying an OTP received on a user's mobile phone.
     * @param appCtx
     * @param currCtx
     * @param personType
     * @param verificationCode
     * @return
     */
    public boolean VerifyPerson(Context appCtx,String currCtx,String personType,String verificationCode) {
        if (!isValidContext(currCtx))
          return false;
        StringBuffer rspStr = new StringBuffer();
        String phone = "";
        String countryCode = "";
        if (personType.equals("HCW"))
        {
            phone = SessionProperties.getInstance(appCtx).getHCWPhone();
            countryCode = SessionProperties.getInstance(appCtx).getHCWCountryCode();
        } else if (personType.equals("Patient")){
            phone = SessionProperties.getInstance(appCtx).getPatientPhone();
            countryCode = SessionProperties.getInstance(appCtx).getPatientCountryCode();
        } else
            return false;
        if (mSvc.verifyCode(phone,countryCode,verificationCode,rspStr)) {
            if (rspStr.toString().equals("OK")) {
                //this means the verification code is right. user is now authenticated
                return true;
            }
        }
        return false;
    }


    /**
     *
     * API for listing all transactions
     * @param appCtx
     * @return
     */
    public List<String> ListTransactions(Context appCtx) {
        List<String> lst = new ArrayList<String>();
        IdentityModule.getInstance(appCtx).getAllTransactions(lst);
        return lst;
    }


    /**
     *
     * API to get proof of service image
     * @param appCtx
     * @param posImgStr
     * @return
     */
    public Bitmap HandleProofOfService(Context appCtx,StringBuffer posImgStr) {
        return ProofOfService.handleProofOfService(appCtx,posImgStr);
    }

    /**
     *
     * API to replicate data to a remote DB.
     * @param appCtx
     * @param dbURL
     * @param dbLogin
     * @param dbPwd
     * @return
     */
    public boolean ReplicateData(Context appCtx,String dbURL,String dbLogin,String dbPwd) {
        return (IdentityModule.getInstance(appCtx).replicateData());
    }
}
