package com.dhi.identitymodule;

import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.util.Log;

import com.cloudant.sync.documentstore.ConflictException;
import com.cloudant.sync.documentstore.DocumentBodyFactory;
import com.cloudant.sync.documentstore.DocumentException;
import com.cloudant.sync.documentstore.DocumentNotFoundException;
import com.cloudant.sync.documentstore.DocumentRevision;
import com.cloudant.sync.documentstore.DocumentStore;
import com.cloudant.sync.documentstore.DocumentStoreException;
import com.cloudant.sync.documentstore.DocumentStoreNotOpenedException;
import com.cloudant.sync.query.FieldSort;
import com.cloudant.sync.query.Query;
import com.cloudant.sync.query.QueryException;
import com.cloudant.sync.query.QueryResult;
import com.cloudant.sync.query.Tokenizer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.cloudant.sync.query.Tokenizer.*;
/**
 * This class acts like an interface to the DB and offers functions to add, query, remove data from it.
 * Date: Aug 30 2019
 * @author IPRD
 * @version 1.0
 */
public class DeviceStore {
    private static final String LOG_TAG = "DeviceStorage";

    private static final String DOCUMENT_STORE_DIR = "data";
    private static final String DOCUMENT_STORE_NAME = "idlink_store";
    private static final String DOCUMENT_LOCAL_STORE_NAME = "idlink_local_store";

    private final Context mContext;
    private DocumentStore mDocumentStore;

    /**
     *
     * @param ctx
     */
    public DeviceStore(Context ctx,String dbType) {
        mContext = ctx;
        // Set up our tasks DocumentStore within its own folder in the applications
        // data directory.
        try {
            /*File path = this.mContext.getApplicationContext().getDir(
                    DOCUMENT_STORE_DIR,
                    Context.MODE_PRIVATE
            );
            this.mDocumentStore = DocumentStore.getInstance(new File(path, DOCUMENT_STORE_NAME));
            Log.d(LOG_TAG, "Set up database at " + path.getAbsolutePath()); */
            String path = loadDBPath();
            String dbName = getDBName(dbType);
            this.mDocumentStore = DocumentStore.getInstance(new File(path, dbName));
            Log.d(LOG_TAG, "Set up database at " + path.toString());
        } catch (DocumentStoreNotOpenedException e) {
            Log.e(LOG_TAG, "Unable to open DocumentStore", e);
        }
    }

    private String getDBName(String dbType)
    {
        switch(dbType)
        {
            case "LOCAL":
                return DOCUMENT_LOCAL_STORE_NAME;
            case "USER":
            default:
                return DOCUMENT_STORE_NAME;
        }
    }

    private String loadDBPath()
    {
        return NeuroLicense.loadExternalDisk()+"/data";
    }

   DocumentStore getDocumentStore() {
        return mDocumentStore;
    }

    /**
     * This function is used to create a text index in the db.
     * Useful while trying to index db data.
     * @param fields
     * @param indexName
     * @return
     */
    boolean setQueryTextIndex(List<FieldSort> fields, String indexName)
    {
        if (!this.mDocumentStore.query().isTextSearchEnabled())
            return false;
        Tokenizer SIMPLE_TOKENIZER = new Tokenizer(null);
        try {
            this.mDocumentStore.query().createTextIndex(fields,indexName,null);
            return true;
        } catch (QueryException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This function can be used to remove an indexed string.
     * If this is done, no more indexing will be done for this string.
     * @param indexName
     * @return
     */
    boolean rmQueryIndex(String indexName)
    {
        try {
            this.mDocumentStore.query().deleteIndex(indexName);
        } catch (QueryException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     *In this method, we use individual search parameters and see if something was found in DB.
     * And then use it to populate the search parameters state values.
     * @param searchParams
     * @return
     */
    boolean getParamsState(Map<String,String> searchParams) {
        // query: { "$text" : { "$search" : "doc*" } }

        for (Map.Entry<String,String> entry : searchParams.entrySet())
        {
            Query q = this.mDocumentStore.query();
            Map<String, Object> q1 = new HashMap<String, Object>();
            q1.put(entry.getKey(),entry.getValue());
            try {
                final QueryResult indResult = q.find(q1);
                if (indResult.size() > 0) //means a match was found. maybe multiple
                    SearchParamsState.getInstance(mContext).addParamState(entry.getKey(),1);
                else
                    SearchParamsState.getInstance(mContext).addParamState(entry.getKey(),0);
                q1.remove(entry.getKey());
            } catch (QueryException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }


    /**
     *
     * This is same as the getParamsState function above. Above funcvtion howeverr searhes db while this function searches the local cache.
     * @param searchParams
     * @param outGuidList
     * @return
     */
    boolean getParamsStateFromCache(Map<String,String> searchParams,List<String> outGuidList) {
        // query: { "$text" : { "$search" : "doc*" } }
        long cacheSearchStart = System.currentTimeMillis();
        if (searchParams.size() == 0)
            return false;
        // List<Set<String>> paramsResList = new ArrayList<Set<String>>();
        Map<String,Set<String>> paramResMap = new HashMap<String,Set<String>>();
        List<Integer> sizeList = new ArrayList<Integer>();
        for (Map.Entry<String,String> entry : searchParams.entrySet())
        {
            //search the internal cache if the key and value are present.
            Set<String> list1 = UserCache.getCache().searchForUser(searchParams.get("indType"),entry.getKey(),entry.getValue());
            System.out.println("Result while searching for - "+entry.getKey()+" "+entry.getValue()+" \n");
            for (String user:list1) {
                System.out.println(user+" \n");
            }
            if (list1 != null && list1.size() > 0) {
                //we found something...co
                // uld be multiple

                paramResMap.put(entry.getKey(),list1);
                //sizeList.add(list1.size());
                SearchParamsState.getInstance(mContext).addParamState(entry.getKey(),1);
            } else {
                //nothing found. update the search params
                SearchParamsState.getInstance(mContext).addParamState(entry.getKey(),0);
            }
        }

        Set<String> uidsFromSearchRes = paramResMap.get("uid");
        Set<String> namesFromSearchRes = paramResMap.get("name");
        Set<String> agesFromSearchRes = paramResMap.get("age");

        boolean namesFound,agesFound = true;

        if (namesFromSearchRes != null && namesFromSearchRes.size() > 0)
            namesFound = true;
        else
            namesFound = false;

        if (agesFromSearchRes != null && agesFromSearchRes.size() > 0)
            agesFound = true;
        else
            agesFound = false;

        if (uidsFromSearchRes != null && uidsFromSearchRes.size() > 0) {
            //if there are some GUIDS with the uid we are searching for.
            Iterator<String> uidsSetItr = uidsFromSearchRes.iterator();

            while (uidsSetItr.hasNext()) {
                String guid = uidsSetItr.next();
                if (outGuidList.size() == 0)
                {
                    //this means biometric search yielded no results.
                    //check if this GUID is present in other search params.
                    if (namesFound)
                    {
                        if (namesFromSearchRes.contains(guid))
                            outGuidList.add(guid);
                    } else if (agesFound) {
                        if (agesFromSearchRes.contains(guid))
                            outGuidList.add(guid);
                    }
                }
                else
                {
                    //biometric search already gave some result
                    if (!outGuidList.contains(guid))
                        outGuidList.add(guid);
                }
            }
        }
        else
        {
            //since no result was found with the uid entered, search name, age results with what we got from the biometric search.
            if (namesFound)
            {
                Iterator<String> namesSetItr = namesFromSearchRes.iterator();
                while (namesSetItr.hasNext())
                {
                    String guid = namesSetItr.next();
                    if (outGuidList.size() > 0 && !outGuidList.contains(guid))
                        outGuidList.remove(guid);
                }
            }

            if (agesFound)
            {
                Iterator<String> agesSetItr = agesFromSearchRes.iterator();
                while (agesSetItr.hasNext())
                {
                    String guid = agesSetItr.next();
                    if (outGuidList.size() > 0 && !outGuidList.contains(guid))
                        outGuidList.remove(guid);
                }
            }

        }

        long cacheSearchEnd = System.currentTimeMillis();
        System.out.println("Cache search time: "+(cacheSearchEnd-cacheSearchStart));
        return true;
    }

    /**
     *
     * This method is used to do a search in DB and get the guids for the resultant users
     * This should be called only if we know that a match was found.
     * @param searchParams
     * @param docIds
     * @return
     */
    boolean runQuery(Map<String,String> searchParams, List<String> docIds) {
        // query: { "$text" : { "$search" : "doc*" } }

        Map<String, Object> q1 = new HashMap<String, Object>();

        for (Map.Entry<String,String> entry : searchParams.entrySet())
        {
            q1.put(entry.getKey(),entry.getValue());
        }

        Query q = this.mDocumentStore.query();

        try {
            final QueryResult l = q.find(q1);
            String sz = Integer.toString(l.size());
            Log.d("Size",sz);
            for (int i = 0;i<l.size();i++)
            {
                docIds.add(l.documentIds().get(i));
            }
            System.out.println(l.toString());
            System.out.println("Query " + Integer.toString(l.documentIds().size()));
            return true;
        } catch (QueryException e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * Creates a document, assigning an ID.
     * @param doc document to create
     * @return new revision of the document
     */
     SampleDoc createDocument(SampleDoc doc,String id) {
        DocumentRevision rev = new DocumentRevision(id);
        rev.setBody(DocumentBodyFactory.create(doc.asMap()));
        try {
            DocumentRevision created = this.mDocumentStore.database().create(rev);
            return doc.fromRevision(created);
        } catch (DocumentException de) {
            de.printStackTrace();
            return null;
        } catch (DocumentStoreException de) {
            de.printStackTrace();
            return null;
        }
    }

    /**
     * Updates a document within the DocumentStore.
     * @param doc document to update
     * @return the updated revision of the document
     * @throws ConflictException if the document passed in has a rev which doesn't
     *      match the current rev in the DocumentStore.
     * @throws DocumentStoreException if there was an error updating the rev for this document
     */
    SampleDoc updateDocument(SampleDoc doc,String id,String revision) throws ConflictException, DocumentStoreException {
        DocumentRevision rev = new DocumentRevision(id,revision);
        rev.setBody(DocumentBodyFactory.create(doc.asMap()));
        try {
            DocumentRevision updated = this.mDocumentStore.database().update(rev);
            return SampleDoc.fromRevision(updated);
        } catch (DocumentException de) {
            de.printStackTrace();
            return null;
        }
    }

    /**
     * Deletes a document within the DocumentStore.
     * @param doc document to delete
     * @throws ConflictException if the document passed in has a rev which doesn't
     *      match the current rev in the DocumentStore.
     * @throws DocumentNotFoundException if the rev for this document does not exist
     * @throws DocumentStoreException if there was an error deleting the rev for this document
     */
    void deleteDocument(SampleDoc doc) throws ConflictException, DocumentNotFoundException, DocumentStoreException {
        DocumentRevision delRev = this.mDocumentStore.database().delete(doc.getDocumentRevision());
        Log.d("Doc-rev deleted",delRev.toString());
    }

    void deleteDocument(String guidDel) throws ConflictException, DocumentNotFoundException, DocumentStoreException {
        List<DocumentRevision> delRevs = this.mDocumentStore.database().delete(guidDel);
        Log.d("Doc-rev deleted",Integer.toString(delRevs.size()));
    }

    void deleteDocument(SampleDoc doc,String id,String revision) throws ConflictException, DocumentNotFoundException, DocumentException,DocumentStoreException {
        DocumentRevision rev = new DocumentRevision(id,revision);
        rev.setBody(DocumentBodyFactory.create(doc.asMap()));
        DocumentRevision updated = this.mDocumentStore.database().delete(rev);
    }

    /**
     * <p>Returns all {@code Task} documents in the DocumentStore.</p>
     */
    List<SampleDoc> allDocs() throws DocumentStoreException {
        int nDocs = this.mDocumentStore.database().getDocumentCount();
        List<DocumentRevision> all = this.mDocumentStore.database().read(0, nDocs, true);
        List<SampleDoc> docs = new ArrayList<SampleDoc>();

        // Filter all documents down to those of type Task.
        for(DocumentRevision rev : all) {
            SampleDoc d = SampleDoc.fromRevision(rev);
            if (d != null) {
                docs.add(d);
            }
        }
        return docs;
    }

}
