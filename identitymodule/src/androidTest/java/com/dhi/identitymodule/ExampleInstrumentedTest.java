package com.dhi.identitymodule;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    Context testContext;
    @Before
    public void setUp() throws Exception {
        testContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.dhi.identitymodule.test", appContext.getPackageName());
    }


    @Test
    public void SegmentationAndMatchingTest() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        SegmentationAndMatching s = SegmentationAndMatching.getInstance(appContext);
        String extpath= s.loadExternalDisk(appContext);

        String fin,fin1,lefte,righte,faceIn;
        byte[] f = s.getbytesFromFile(extpath + "/Neurotechnology/fingerprint.png" );
        fin = android.util.Base64.encodeToString(f, android.util.Base64.NO_WRAP);
    //    fin = "";


        byte[] l = s.getbytesFromFile(extpath + "/Neurotechnology/leftIris.png" );
        lefte = android.util.Base64.encodeToString(f, android.util.Base64.NO_WRAP);
       // lefte = "";

        byte[] r = s.getbytesFromFile(extpath + "/Neurotechnology/rightIris.png" );
        righte = android.util.Base64.encodeToString(f, android.util.Base64.NO_WRAP);
       // righte = "";

        byte[] face = s.getbytesFromFile(extpath + "/Neurotechnology/face.png" );
        faceIn = android.util.Base64.encodeToString(face, android.util.Base64.NO_WRAP);

//        String tMessage = "{ \"eye\": { \"image\": { \"left\": \"\", \"right\": \"\" }, \"iris\": { \"left\": \"\", \"right\": \"\" } }, \"fingerPrint\": { \"right\": { \"thumb\": \"\", \"index\":" + rightIndex + ", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"left\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"template\": { \"right\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" }, \"left\": { \"thumb\": \"\", \"index\": \"\", \"middle\": \"\", \"ring\": \"\", \"pinky\": \"\" } } } }";
        JSONObject resJSON = null;
        String ret1 = "";
        StringBuffer ret=new StringBuffer();

        byte[] faceb = s.verifyPersonExist(faceIn,BiometricType.FACE,ret);
        assertEquals(0, ret.toString().length());

        byte[] fb = s.verifyPersonExist(fin,BiometricType.FINGER_L,ret);
        assertEquals(0, ret.toString().length());

        byte[] lb = s.verifyPersonExist(lefte,BiometricType.EYE_L,ret);
        assertEquals(0, ret.toString().length());

        byte[] rb = s.verifyPersonExist(righte,BiometricType.EYE_R,ret);
        assertEquals(0, ret.toString().length());

        boolean added = s.enrollPerson(lb,rb,fb,fb,faceb,"madhav");
        assertEquals(true, added);

        byte[] faceb2 = s.verifyPersonExist(faceIn,BiometricType.FACE,ret);
        assertEquals(6, ret.toString().length());

        /*
        byte[] fb2 = s.verifyPersonExist(fin,BiometricType.FINGER_L,ret);
        assertEquals(6, ret.toString().length());

        byte[] lb1 = s.verifyPersonExist(lefte,BiometricType.EYE_L,ret);
        assertEquals(12, ret.toString().length());

        byte[] rb1 = s.verifyPersonExist(righte,BiometricType.EYE_R,ret);
        assertEquals(18, ret.toString().length()); */




        StringBuffer re=new StringBuffer();
        //,boolean lefteye,boolean righteye,boolean leftfp,boolean rightfp){

        byte[] el = s.verifyPersonEyeOrFingerLAndR(righte,re,BiometricType.EYE_R);
        assertEquals(6, re.toString().length());

        byte[] er = s.verifyPersonEyeOrFingerLAndR(lefte,re,BiometricType.EYE_L);
        assertEquals(12, re.toString().length());

    }

    @Test
    public void testSessionProperties() {
        int [] age = {0,12,2147483647,-2147483647};
        for (int i=0;i<age.length;i++)
        {
            SessionProperties.getInstance(testContext).setHcwAge(age[i]);
            int outAge = Integer.parseInt(SessionProperties.getInstance(testContext).getHcwAge());
            assertEquals(age[i],outAge);
        }
        for (int i=0;i<age.length;i++)
        {
            SessionProperties.getInstance(testContext).setPatientAge(age[i]);
            int outAge = Integer.parseInt(SessionProperties.getInstance(testContext).getPatientAge());
            assertEquals(age[i],outAge);
        }

        long [] epochDOB = {1234567890L,0L,-1234567890L,9223372036854775807L};
        for (int i=0;i<epochDOB.length;i++)
        {
            SessionProperties.getInstance(testContext).setEpochDOB(true,epochDOB[i]);
            long outEpochDOB = SessionProperties.getInstance(testContext).getEpochDOB(true);
            assertEquals(epochDOB[i],outEpochDOB);
        }

        String [] epochStr = {"1234567890","","0","-1234567890","9223372036854775807"};
        for (int i=0;i<epochStr.length;i++)
        {
            SessionProperties.getInstance(testContext).setEpochDOB(true,epochStr[i]);
            long outEpochDOB = SessionProperties.getInstance(testContext).getEpochDOB(true);
            if (epochStr[i] != null && epochStr[i].length() > 0)
                assertEquals(epochStr[i],Long.toString(outEpochDOB));
            else
                assertEquals("0",Long.toString(outEpochDOB));
        }

        String [] ids = {"abc122","sas$%^&&@","","11"};
        for (int i=0;i<ids.length;i++)
        {
            SessionProperties.getInstance(testContext).setID(true,ids[i]);
            String outID = SessionProperties.getInstance(testContext).getID(true);
            assertEquals(ids[i],outID);
        }

        String [] uids = {"H_100","P_100","I_100","absbasb12","asasasa","000","adsfdf%^%^%^&%&^&^%",""};
        for (int i=0;i<uids.length;i++)
        {
            SessionProperties.getInstance(testContext).setPatientGUID(uids[i]);
            SessionProperties.getInstance(testContext).setHcwGUID(uids[i]);
            String outUID = SessionProperties.getInstance(testContext).getPatientGUID();
            assertEquals(uids[i],outUID);
            outUID = SessionProperties.getInstance(testContext).getHcwGUID();
            assertEquals(uids[i],outUID);
        }




    }
}
