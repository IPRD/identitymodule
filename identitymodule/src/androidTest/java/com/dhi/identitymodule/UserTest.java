package com.dhi.identitymodule;

import android.support.test.InstrumentationRegistry;
import android.util.Log;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class UserTest {
    DeviceStore mStore = new DeviceStore(InstrumentationRegistry.getTargetContext(),"");


    @Test
    public void testParamsStateFromCache() {
        UserCache.getCache().addDataToCache("HCW","name","Hari","H100");
        UserCache.getCache().addDataToCache("HCW","uid","25","H100");
        UserCache.getCache().addDataToCache("HCW","name","Ganesh","H102");
        UserCache.getCache().addDataToCache("HCW","uid","26","H102");
        UserCache.getCache().addDataToCache("HCW","name","Hari","H103");
        UserCache.getCache().addDataToCache("HCW","uid","27","H103");
        UserCache.getCache().addDataToCache("Patient","name","Jack","J100");
        Map<String,String> searchParams = new HashMap<String,String>();
        searchParams.put("name","Hari");
        searchParams.put("uid","25");
        searchParams.put("indType","HCW");
        List<String> outGUIDList = new ArrayList<String>();

        mStore.getParamsStateFromCache(searchParams,outGUIDList);
        if (outGUIDList.size() == 1)
            assertEquals("H100",outGUIDList.get(0));
        else
            Log.e("Failure","Wrong guid output");

        boolean cacheOperation = false;
        cacheOperation = UserCache.getCache().deleteUserFromParamsMap("HCW","name","Hari",outGUIDList.get(0));
        assertEquals(true,cacheOperation);
        cacheOperation = UserCache.getCache().deleteUserFromParamsMap("HCW","uid","25",outGUIDList.get(0));
        assertEquals(true,cacheOperation);
        cacheOperation = UserCache.getCache().deleteFromGUIDMap(outGUIDList.get(0));
        assertEquals(true,cacheOperation);

        List<String> outGUIDList1 = new ArrayList<String>();

        mStore.getParamsStateFromCache(searchParams,outGUIDList1);
        assertEquals(0,outGUIDList1.size());

    }

    @Test
    public void testSessionProperties() {
        SessionProperties.getInstance(InstrumentationRegistry.getContext()).setAppType(AppType.BIOMETRIC_ONLY);
        assertEquals(AppType.BIOMETRIC_ONLY,SessionProperties.getInstance(InstrumentationRegistry.getContext()).getAppType());

        SessionProperties.getInstance(InstrumentationRegistry.getContext()).setAppType(AppType.GENERAL_APP);
        assertEquals(AppType.GENERAL_APP,SessionProperties.getInstance(InstrumentationRegistry.getContext()).getAppType());

        SessionProperties.getInstance(InstrumentationRegistry.getContext()).setAppType(AppType.NULL);
        assertEquals(AppType.NULL,SessionProperties.getInstance(InstrumentationRegistry.getContext()).getAppType());
    }
}
